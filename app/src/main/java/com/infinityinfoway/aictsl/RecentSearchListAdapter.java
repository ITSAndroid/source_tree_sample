package com.infinityinfoway.aictsl;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.infinityinfoway.aictsl.bean.RecentSearchBean;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * Created by BhuMit on 4/1/2017.
 */

public class RecentSearchListAdapter extends BaseAdapter {

    List<RecentSearchBean> list;
    Context context;

    public RecentSearchListAdapter(List<RecentSearchBean> list, Context context) {
        super();
        this.list = list;
        this.context = context;
    }

    @Override
    public int getCount() {

        return list.size();
    }

    @Override
    public Object getItem(int position) {

        return list.get(position);
    }

    @Override
    public long getItemId(int position) {

        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        View view = convertView;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.list_recentsearch, parent, false);
            holder = new ViewHolder();
            holder.txtFromCity = (TextView) view.findViewById(R.id.tv_fromcity);
            holder.txtToCity = (TextView) view.findViewById(R.id.tv_tocity);
            holder.txtDate = (TextView) view.findViewById(R.id.tv_date_recent);
            view.setTag(holder);

        } else {
            holder = (ViewHolder) view.getTag();
        }
        final RecentSearchBean data = list.get(position);
       // Toast.makeText(context, "list==="+list.size(), Toast.LENGTH_SHORT).show();
       // Toast.makeText(context, "data.FROM_CITY==="+data.FROM_CITY, Toast.LENGTH_SHORT).show();
        if (data.FROM_CITY != null && data.FROM_CITY.length() > 0) {
            holder.txtFromCity.setText(""+data.FROM_CITY);
           // Toast.makeText(context, "11data.FROM_CITY==="+data.FROM_CITY, Toast.LENGTH_SHORT).show();
        }
        if (data.TO_CITY != null && data.TO_CITY.length() > 0) {
            holder.txtToCity.setText(""+data.TO_CITY);
        }
        if (data.DATE != null && data.DATE.length() > 0) {
            SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy", java.util.Locale.getDefault());
            try {
                Date date = format.parse(data.DATE);
                SimpleDateFormat format1 = new SimpleDateFormat("EEE MMM dd ,yyyy", java.util.Locale.getDefault());
                holder.txtDate.setText(""+format1.format(date));
            } catch (ParseException e) {

                e.printStackTrace();
            }
            // holder.txtDate.setText(data.DATE);
        }
        view.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                Intent intent = new Intent(context, ActivityCityBusOperation.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("data", data);
                context.startActivity(intent);

            }
        });
        try {
            setAnimation(view, position);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return view;
    }

    private static class ViewHolder {
        private TextView txtFromCity, txtToCity, txtDate;
    }

    private int lastPosition = -1;

    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            ScaleAnimation anim = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
            anim.setDuration(new Random().nextInt(501));//to make duration random number between [0,501)
            viewToAnimate.startAnimation(anim);
            lastPosition = position;
        }
    }
}