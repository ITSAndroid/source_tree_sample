package com.infinityinfoway.aictsl;

import android.content.Context;

public class Model_RcentSearch {

public String id,sourcecity,sourceid,tocity,toid,date;
Context context;
	
	public Model_RcentSearch(String s0, String s1, String s2, String s3, String s4, String s5)
	{
		id = s0;
		sourcecity = s1;
		sourceid=s2;
		tocity=s3;
		toid=s4;
		date=s5;
	}

	public Model_RcentSearch() {
		// TODO Auto-generated constructor stub
	}

	public Model_RcentSearch(Context c) {
		// TODO Auto-generated constructor stub
		context = c;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSourcecity() {
		return sourcecity;
	}

	public void setSourcecity(String sourcecity) {
		this.sourcecity = sourcecity;
	}

	public String getSourceid() {
		return sourceid;
	}

	public void setSourceid(String sourceid) {
		this.sourceid = sourceid;
	}

	public String getTocity() {
		return tocity;
	}

	public void setTocity(String tocity) {
		this.tocity = tocity;
	}

	public String getToid() {
		return toid;
	}

	public void setToid(String toid) {
		this.toid = toid;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}
}


