package com.infinityinfoway.aictsl;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.infinityinfoway.aictsl.api.APIsConnectivity;
import com.infinityinfoway.aictsl.api.DataConnectivity;
import com.infinityinfoway.aictsl.bean.RecentSearchBean;
import com.infinityinfoway.aictsl.database.DBConnector;
import com.infinityinfoway.aictsl.database.OnwardJourneyDataPref;
import com.infinityinfoway.aictsl.database.ReturnJourneyDataPref;
import com.infinityinfoway.aictsl.database.SharedPreference;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class AvailableRoutesActivity extends AppCompatActivity {
    String TAG = "AvailableRoutesActivity.this";
    Toolbar toolbar;
    // EasyTracker easyTracker = null;
    APIsConnectivity getAPI = new APIsConnectivity();
    DataConnectivity getData = new DataConnectivity();
    private SharedPreference getSharePref;
    private OnwardJourneyDataPref getOnwardJourneyPref;
    private ReturnJourneyDataPref getReturnJourneyPref;
    private Dialog exitDialog;
    private TextView tv_RouteName, tv_noRoute;
    private ListView li_AvailableRoots;
    private ProgressDialog progDialog;
    private AvailableAdapterCity availableRoots;
    Model_RcentSearch data;
    private Button btn_discardReturnTrip;
    DBConnector getSQLData;
    SharedPreference getPref;
    LinearLayout llAvailableBus;


    private int[] CompanyID, FromCityId, ToCityId, RouteID, RouteTimeID, BusType, ArrangementID, EmptySeats;
    int[] BusID;

    private String[] CompanyName, FromCityName, ToCityName, RouteName, RouteTime, BusNo, CityTime, ArrivalTime, BusTypeName, BookingDate,
            ArrangementName, BoardingPoints, DroppingPoints, ReferenceNumber;

    private double[] AcSeatServiceTax, AcSlpServiceTax, AcSlmbServiceTax, NonAcSeatServiceTax, NonAcSlpServiceTax, NonAcSlmbServiceTax,
            BaseAcSeat, BaseAcSlp, BaseAcSlmb, BaseNonAcSeat, BaseNonAcSlp, BaseNonAcSlmb, AcSeatRate, AcSleeperRate, AcSlumberRate,
            NonAcSeatRate, NonAcSleeperRate, NonAcSlumberRate;
    String appver;
    private int[] BusSeatType;
    private String ServiceTax[], ServiceTaxRoundUp[];
    private int[] IsIncludeTax;
    private String[] RouteAmenities;
    int i;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_available_routes);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        llAvailableBus = (LinearLayout) findViewById(R.id.llAvailableBus);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        // Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this,TAG));
        // easyTracker = EasyTracker.getInstance(AvailableRoutesActivity.this);
        getSharePref = new SharedPreference(AvailableRoutesActivity.this);
        getOnwardJourneyPref = new OnwardJourneyDataPref(AvailableRoutesActivity.this);
        getReturnJourneyPref = new ReturnJourneyDataPref(AvailableRoutesActivity.this);
        getSQLData = new DBConnector(AvailableRoutesActivity.this);
        data = new Model_RcentSearch(AvailableRoutesActivity.this);


        PackageInfo pInfo = null;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        appver = pInfo.versionCode + "(" + pInfo.versionName + ")";

        tv_noRoute = (TextView) findViewById(R.id.tv_noRoute);
        btn_discardReturnTrip = (Button) findViewById(R.id.btn_discardReturnTrip_avail);
        li_AvailableRoots = (ListView) findViewById(R.id.li_AvailableRoots);
        tv_RouteName = (TextView) findViewById(R.id.tv_RouteName_avail);
        getPref = new SharedPreference(AvailableRoutesActivity.this);

        if (getSharePref.IsReturnTrip() == 2) {
            btn_discardReturnTrip.setVisibility(View.VISIBLE);
            progDialog = new ProgressDialog(AvailableRoutesActivity.this);
            progDialog.setIndeterminate(true);
            progDialog.setMessage("Loading Available Buses...");
            progDialog.show();

            new LazyDataConnection().execute("Get_SchedulesList", getAPI.Get_SchedulesList(
                    getSharePref.getFromCityID(), getSharePref.getToCityID()));
            Log.v("getFromCityID", String.valueOf(getSharePref.getFromCityID()));
            Log.v("gettoCityID", String.valueOf(getSharePref.getToCityID()));

        } else {
            progDialog = new ProgressDialog(AvailableRoutesActivity.this);
            progDialog.setIndeterminate(true);
            progDialog.setMessage("Loading Available Buses...");
            progDialog.show();
            new LazyDataConnection().execute("Get_SchedulesList", getAPI.Get_SchedulesList(
                    getSharePref.getFromCityID(),
                    getSharePref.getToCityID()
            ));
        }

        li_AvailableRoots.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

                ConnectionCheck connectionCheck = new ConnectionCheck();
                if (connectionCheck.isNetworkConnected(AvailableRoutesActivity.this)) {
                    getReturnJourneyPref.saveReturnJourneyBusDetails(RouteName[arg2]
                            , RouteTime[arg2], BusID[arg2], BusNo[arg2]);
                    getPref.saveBusID1(BusID[arg2]);
                   // Toast.makeText(AvailableRoutesActivity.this, "BusNo.length "+BusNo.length, Toast.LENGTH_SHORT).show();
                    if(BusNo.length > 0 && BusID != null && BusNo[arg2].length()>0 ) {
                        Intent gotoSeatArrangement = new Intent(getApplicationContext(), ActivityMaps.class);
                        gotoSeatArrangement.putExtra("BusID", BusID[arg2]);
                        startActivity(gotoSeatArrangement);
                    }else {
                        Snackbar bar = Snackbar.make(llAvailableBus, "Data not Found...!!", Snackbar.LENGTH_LONG);
                        bar.show();
                       // Toast.makeText(AvailableRoutesActivity.this, "Data not Found...!!", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Snackbar bar = Snackbar.make(llAvailableBus, "Oops! No Internet Connection", Snackbar.LENGTH_LONG);
                    bar.show();
                   // Toast.makeText(AvailableRoutesActivity.this, "InternetErrorActivity", Toast.LENGTH_SHORT).show();
                    return;
                }

            }
        });

    }

    //=====================================================================================
// Source City List
//=====================================================================================
    public void Get_SchedulesList(String str_RespXML, String str_TagName) {
        Document doc = getData.XMLfromString(str_RespXML);
        NodeList nodes = doc.getElementsByTagName(str_TagName);

        RouteName = new String[nodes.getLength()];
        RouteTime = new String[nodes.getLength()];
        BusID = new int[nodes.getLength()];
        BusNo = new String[nodes.getLength()];

        if (nodes.getLength() > 0)
            for (i = 0; i <= nodes.getLength(); i++) {
                Node e1 = nodes.item(i);
                Element el = (Element) e1;
                if (el != null) {

                    try {
                        RouteName[i] = el.getElementsByTagName("RouteName").item(0).getTextContent();
                        Log.e("RouteName", "" + RouteName);

                    } catch (Exception e) {
                    }
                    try {
                        RouteTime[i] = el.getElementsByTagName("RouteTime").item(0).getTextContent();
                        Log.e("RouteTime", "" + RouteTime);

                    } catch (Exception e) {
                    }
                    try {
                        BusNo[i] = el.getElementsByTagName("BusNo").item(0).getTextContent();
                        Log.e("RouteTime", "" + RouteTime);

                    } catch (Exception e) {
                    }
                    try {
                        BusID[i] = Integer.parseInt(el.getElementsByTagName("BusID").item(0).getTextContent());
                        // Toast.makeText(AvailableRoutesActivity.this, "BusID" + BusID, Toast.LENGTH_SHORT).show();
                        Log.e("BusID", "" + BusID);

                    } catch (Exception e) {
                    }

                }
            }
    }

    //=====================================================================================
// Async Task Class
//=====================================================================================
    public class LazyDataConnection extends AsyncTask<String, Void, String> {
        String method;

        @Override
        protected String doInBackground(String... arg0) {
            method = arg0[0];
            return getData.callWebService(arg0[0], arg0[1]);
        }

        protected void onPostExecute(String xmlResponse) {

            if (xmlResponse.equals("")) {
                try {
                    progDialog.dismiss();
                    if (progDialog != null && progDialog.isShowing()) {
                        progDialog.dismiss();
                    }
                } catch (Exception ex) {
                }
                Toast.makeText(AvailableRoutesActivity.this, "InternetErrorActivity", Toast.LENGTH_SHORT).show();
                return;
            } else {
                if (method.equals("Get_SchedulesList")) {
                    Get_SchedulesList(xmlResponse, "ScheduleList");
                    try {
                        progDialog.dismiss();
                        if (progDialog != null && progDialog.isShowing()) {
                            progDialog.dismiss();
                        }
                    } catch (Exception ex) {
                    }
                    Log.e("RouteName1", "" + BusNo);
                    Log.e("BusID1", "" + BusID);
                    Log.e("RouteTime1", "+" + RouteTime);
                    Log.e("RouteName", "" + RouteName);
                    availableRoots = new AvailableAdapterCity(getApplicationContext(), RouteName, RouteTime, BusID, BusNo);


                    if (availableRoots.getCount() != 0) {
                        int available_count = availableRoots.getCount();
                        li_AvailableRoots.setAdapter(availableRoots);
                        if (getSharePref.IsReturnTrip() == 2) {
                            tv_RouteName.setText(available_count + " Trip(s) Available For " + getSharePref.getToCityName() + " To "
                                    + getSharePref.getFromCityName() + " On : " + getSharePref.getReturnJourneyDate());
                        } else {
                            tv_RouteName.setText(available_count + " Trip(s) Available For " + getSharePref.getFromCityName() + " To "
                                    + getSharePref.getToCityName() + " On : " + getSharePref.getOnwardJourneyDate());
                        }

                        DBConnector database = new DBConnector(
                                AvailableRoutesActivity.this);
                        try {
                            RecentSearchBean data = new RecentSearchBean();

                            data.FROM_ID = String.valueOf(getSharePref.getFromCityID());
                            data.TO_ID = String.valueOf(getSharePref.getToCityID());
                            data.FROM_CITY = getSharePref.getFromCityName();
                            data.TO_CITY = getSharePref.getToCityName();
                            data.DATE = getSharePref.getOnwardJourneyDate();
                            database.addRecentSearch(data);

                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                        database.close();
                    } else {
                        if (getSharePref.IsReturnTrip() == 2) {
                            tv_RouteName.setText("For : " + getSharePref.getToCityName() + " - " + getSharePref.getFromCityName() + "\nOn : " + getSharePref.getReturnJourneyDate());
                            tv_noRoute.setVisibility(View.VISIBLE);
                        } else {
                            tv_RouteName.setText("For : " + getSharePref.getFromCityName() + " - " + getSharePref.getToCityName() + "\nOn : " + getSharePref.getOnwardJourneyDate());
                            tv_noRoute.setVisibility(View.VISIBLE);
                        }

                    }
                }

            }
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
            default:
        }
        return super.onOptionsItemSelected(item);

    }

    @Override
    public void onStart() {
        super.onStart();
        //EasyTracker.getInstance(this).activityStart(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        //EasyTracker.getInstance(this).activityStop(this);
    }
}
