package com.infinityinfoway.aictsl.calendar;

import java.util.Calendar;
import java.util.List;


public interface IndicatorDate {

    Calendar getDay();

    List<Integer> getIndicatorColors();
}
