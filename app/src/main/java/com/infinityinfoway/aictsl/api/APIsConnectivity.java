package com.infinityinfoway.aictsl.api;


import com.infinityinfoway.aictsl.config.Config;

public class APIsConnectivity
{
	public String str_Key = Config.str_Key;
	private String api_url = Config.api_url;
	private String aeh_url = Config.AEH_ActURL;
	//int CompanyID =Config.COMPANY_ID;


//==============================================================================
	// XMLEnvelope
	//==============================================================================
	public String xmlEnvelope(String params) {
		return ""
				+ "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
				+ "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"
				+ "<soap:Body>" + params + "</soap:Body>" + "</soap:Envelope>";
	}
	//==============================================================================
	// Get_CityList
	//==============================================================================
	public String Get_CityList()
	{
		return xmlEnvelope(""

                + "<Get_CityList " + "xmlns=\"" + api_url + "\">"
				+ "<VerifyCall>" + str_Key + "</VerifyCall>"
				+ "</Get_CityList>");
	}


	//==============================================================================
	// Get_SchedulesList
	//==============================================================================
	public String Get_SchedulesList(int  intFromCityID,int intToCityID)
	{
		return xmlEnvelope(""
				+ "<Get_SchedulesList xmlns=\""+ api_url+"\">"
				+ "<FromCityID>" + intFromCityID + "</FromCityID>"
				+ "<ToCityID>" + intToCityID + "</ToCityID>"
				+ "<VerifyCall>" + str_Key + "</VerifyCall>"
				+ "</Get_SchedulesList>");
	}
	//==============================================================================
	// Get_SchedulesList
	//==============================================================================
	public String Get_BusLocation(int intBusID)
	{
		return xmlEnvelope(""
				+ "<Get_BusLocation xmlns=\""+ api_url+"\">"
				+ "<BusID>" + intBusID + "</BusID>"
				+ "<VerifyCall>" + str_Key + "</VerifyCall>"
				+ "</Get_BusLocation>");
	}

































	/*

	// ============================================================================
	// GetCustomerBalanceDetails
	// ============================================================================
			public String getBalance(String mobileNo) {
				return xmlEnvelope("<GetCustomerBalanceDetails xmlns=\"" + api_url
						+ "\">" + "<MobileNo>" + mobileNo + "</MobileNo>"
						+ "<VerifyCall>" + str_Key + "</VerifyCall>"
						+ "</GetCustomerBalanceDetails>");
			}
	// ============================================================================
	// FetchImageSplashScreenURL
	// ============================================================================
			public String FetchImageSplashScreenURL(int UserId, String FromDate) {
				return xmlEnvelope("" + "<FetchImageSplashScreenURL xmlns=\"" + api_url
						+ "\">" 
						+ "<ParaAppGeneralInfo>"
						+ "<AndroidId>"	+ Config.AndroidId	+ "</AndroidId>"
						+ "<UserId>"	+ UserId	+ "</UserId>"
						+ "<App_Name>"	+ Config.APP_NAME	+ "</App_Name>"
						+ "<AppVerName>"	+ Config.AppVersionName	+ "</AppVerName>"
						+ "<AppVerCode>"	+ Config.AppVersionCode	+ "</AppVerCode>"
						+ "<AppOsVer>"	+ Config.AppOsVer	+ "</AppOsVer>"
						+ "</ParaAppGeneralInfo>"
						+ "<FromDate>" + FromDate + "</FromDate>"
						+ "<VerifyCall>" + str_Key + "</VerifyCall>"
						+ "</FetchImageSplashScreenURL>");
			}
	//============================================================================
    //Fetch ContactUs Details  
//============================================================================
	public String FetchContactUs(int UserId, String modifydate)
	{
		return xmlEnvelope(""
			+ "<GetContactDetails xmlns=\""+api_url+"\">"	
			+ "<ParaAppGeneralInfo>"
			+ "<AndroidId>"	+ Config.AndroidId	+ "</AndroidId>"
			+ "<UserId>"	+ UserId	+ "</UserId>"
			+ "<App_Name>"	+ Config.APP_NAME	+ "</App_Name>"
			+ "<AppVerName>"	+ Config.AppVersionName	+ "</AppVerName>"
			+ "<AppVerCode>"	+ Config.AppVersionCode	+ "</AppVerCode>"
			+ "<AppOsVer>"	+ Config.AppOsVer	+ "</AppOsVer>"
			+ "</ParaAppGeneralInfo>"
			+ "<CompanyID>"+CompanyID+"</CompanyID>"
			+ "<LastModifyDate>" +modifydate +"</LastModifyDate>"
			+ "<VerifyCall>" +str_Key +"</VerifyCall>"
			+ "</GetContactDetails>");
	}
	//==============================================================================
	// BlockSeat
	//==============================================================================
	public String blockSeat(String referenceNumber, String seatNumber)
	{
		return xmlEnvelope(""
				+ "<BlockSeat xmlns=\""+ api_url+"\">"
				+ "<ReferenceNumber>" 	+ referenceNumber 	+ "</ReferenceNumber>"
				+ "<SeatNumber>" 		+ seatNumber 		+ "</SeatNumber>"
				+ "<VerifyCall>" 		+ str_Key 			+ "</VerifyCall>" + "</BlockSeat>");
	}

	//==============================================================================
	// getAvailableRoutes_STax
	//==============================================================================
	public String GetAvailableRoutes_STax(int fromID, int toID, String journeyDate)
	{  
		return xmlEnvelope(""
				+ "<GetAvailableRoutes_STax xmlns=\""+api_url+"\">"
				+ "<FromID>" 		+ fromID 		+ "</FromID>" 
				+ "<ToID>" 			+ toID 			+ "</ToID>" 
				+ "<JourneyDate>" 	+ journeyDate 	+ "</JourneyDate>"
				+ "<VerifyCall>" 	+ str_Key 		+ "</VerifyCall>"
				+ "</GetAvailableRoutes_STax>");
	}

	// ============================================================================
	// PhoneBookingConfrimOwnBalance
	// ============================================================================
	public String PhoneBookingConfrimOwnBalance(String PhoneOrderID,
                                                String PNRNo) {
		return xmlEnvelope("" + "<PhoneBookingConfrimOwnBalance xmlns=\""
				+ api_url + "\">" + "<PhoneOrderID>" + PhoneOrderID
				+ "</PhoneOrderID>" + "<PNRNo>" + PNRNo + "</PNRNo>"
				+ "<VerifyCall>" + str_Key + "</VerifyCall>"
				+ "</PhoneBookingConfrimOwnBalance>");
	}

	// ============================================================================
	// PhoneBookingConfrimPaymentGatway
	// ============================================================================
	public String PhoneBookingConfrimPaymentGatway(String PhoneOrderID,
                                                   String PNRNo) {
		return xmlEnvelope("" + "<PhoneBookingConfrimPaymentGatway xmlns=\""
				+ api_url + "\">" + "<PhoneOrderID>" + PhoneOrderID
				+ "</PhoneOrderID>" + "<PNRNo>" + PNRNo + "</PNRNo>"
				+ "<VerifyCall>" + str_Key + "</VerifyCall>"
				+ "</PhoneBookingConfrimPaymentGatway>");
	}

	//==============================================================================
	// GetBoardingPointDetails
	//==============================================================================
	public String GetBoardingPointDetails(String referenceNumber)
	{
		return xmlEnvelope(""
				+ "<GetBoardingPointDetails xmlns=\""+ api_url+"\">"
				+ "<ReferenceNumber>" 	+ referenceNumber 	+ "</ReferenceNumber>"
				+ "<VerifyCall>" 		+ str_Key 			+ "</VerifyCall>"
				+ "</GetBoardingPointDetails>");
	}

	//==============================================================================
	// GetPNRDetails
	//==============================================================================
	public String getPNRDetails(int pnrNo, String phoneNo, int companyID)
	{
		return xmlEnvelope(""
				+ "<GetPNRDetails xmlns=\""+ api_url+"\">"
				+ "<PNRNO>" + pnrNo + "</PNRNO>" 
				+ "<PhoneNo>" + phoneNo	+ "</PhoneNo>" 
				+ "<CompanyID>" + companyID + "</CompanyID>"
				+ "<VerifyCall>" + str_Key + "</VerifyCall>"
				+ "</GetPNRDetails>");
	}

	//==============================================================================
	// getSeatArrangementDetails_STax
	//==============================================================================
	public String getSeatArrangementDetails_STax(String referenceNumber)
	{
		return xmlEnvelope(""
				+ "<GetSeatArrangementDetails_STax xmlns=\""+api_url+"\">"
				+ "<ReferenceNumber>" + referenceNumber + "</ReferenceNumber>"
				+ "<VerifyCall>" + str_Key + "</VerifyCall>"
				+ "</GetSeatArrangementDetails_STax>");
	}
	
	public String GetSeatArrangementDetails_STax_UBLB(String referenceNumber)
	{
		return xmlEnvelope(""
				+ "<GetSeatArrangementDetails_STax_UBLB xmlns=\""+api_url+"\">"
				+ "<ReferenceNumber>" + referenceNumber + "</ReferenceNumber>"
				+ "<VerifyCall>" + str_Key + "</VerifyCall>"
				+ "</GetSeatArrangementDetails_STax_UBLB>");
	}
	   
	public String GetSeatArrangementDetails_UBLB(int companyID, String operatorID, String referenceNumber,
                                                 String requestType, String opratorFare)
	{
		return xmlEnvelope("<GetSeatArrangementDetails_UBLB xmlns=\""+api_url+"\">"
				+ "<CompanyID>"+companyID+"</CompanyID>"
				+ "<OM_OperatorId>"+operatorID+"</OM_OperatorId>"
				+ "<ReferenceNumber>" + referenceNumber + "</ReferenceNumber>"
				+ "<RequestType>" + requestType + "</RequestType>"
				+ "<opratorFare>" + opratorFare + "</opratorFare>"
				+ "<VerifyCall>" + str_Key + "</VerifyCall>"
				+ "</GetSeatArrangementDetails_UBLB>");
	}



	//==============================================================================
	// RegistrationApp
	//==============================================================================
	public String RegistrationApp(int UserId, String cusName, String cusEmail, String cusMobile,
                                  String cusBirthDate, String cusPassword, String deviceIP)
	{
		return xmlEnvelope("<RegistrationApp xmlns=\""+ api_url+"\">"
				+ "<ParaAppGeneralInfo>"
				+ "<AndroidId>"	+ Config.AndroidId	+ "</AndroidId>"
				+ "<UserId>"	+ UserId	+ "</UserId>"
				+ "<App_Name>"	+ Config.APP_NAME	+ "</App_Name>"
				+ "<AppVerName>"	+ Config.AppVersionName	+ "</AppVerName>"
				+ "<AppVerCode>"	+ Config.AppVersionCode	+ "</AppVerCode>"
				+ "<AppOsVer>"	+ Config.AppOsVer	+ "</AppOsVer>"
				+ "</ParaAppGeneralInfo>"
				+ "<CustName>"+ cusName + "</CustName>"
				+ "<CustEmail>"+ cusEmail + "</CustEmail>"
				+ "<CustMobile>" + cusMobile + "</CustMobile>"
				+ "<CustPhone>" + cusMobile + "</CustPhone>"
				+ "<CustDateOfBirth>" + cusBirthDate + "</CustDateOfBirth>"
				+ "<Password>" + cusPassword + "</Password>"
				+ "<IsActive>0</IsActive>"
				+ "<SenderIP>" + "192.168.2.1" + "</SenderIP>"
				+ "<RegistrationBy>Application</RegistrationBy>"
				+ "<VerifyCall>" + str_Key + "</VerifyCall>"
				+ "</RegistrationApp>");
	}


	//==============================================================================
	// GetVerifyVerificationCode
	//==============================================================================
	public String GetVerifyVerificationCode(int UserId, String varificationCode, String phoneNum, String EmailID)
	{
		return xmlEnvelope("<GetVerifyVerificationCode xmlns=\""+ api_url+"\">"
				+ "<ParaAppGeneralInfo>"
				+ "<AndroidId>"	+ Config.AndroidId	+ "</AndroidId>"
				+ "<UserId>"	+ UserId	+ "</UserId>"
				+ "<App_Name>"	+ Config.APP_NAME	+ "</App_Name>"
				+ "<AppVerName>"	+ Config.AppVersionName	+ "</AppVerName>"
				+ "<AppVerCode>"	+ Config.AppVersionCode	+ "</AppVerCode>"
				+ "<AppOsVer>"	+ Config.AppOsVer	+ "</AppOsVer>"
				+ "</ParaAppGeneralInfo>"
				+ "<Verification>" + varificationCode + "</Verification>"
				+ "<MobileNo>" + phoneNum + "</MobileNo>"
				+ "<EmailID>" + EmailID + "</EmailID>"
				+ "<VerifyCall>" + str_Key + "</VerifyCall>" 
				+ "</GetVerifyVerificationCode>");
	}
	//==============================================================================
		// GetDiscountCustomerbyCoupons
		//==============================================================================
		public String GetDiscountCustomerbyCoupons(String couponCode, String custEmail, String custMobile,
                                                   String CustRegMobile, String SeatDetails, String SeatDetails_Return, String RouteDetails, String RouteDetails_Return, double OnwardTotalAmount,
                                                   double ReturnTotalAmount, int OnwardSeatTotal, int ReturnSeatTotal,
                                                   int journeyType, String AndroidID, int ApplicationType, String ReferenceNumber, String ReferenceNumber_R )
		{
			return xmlEnvelope(""
					+ "<GetDiscountCustomerbyCoupons xmlns=\""+api_url+"\">"		
					+ "<CouponCode>"	+	couponCode	+	"</CouponCode>"
					+ "<CustEmail>"		+	custEmail	+	"</CustEmail>"
					+ "<CustMobile>"	+	custMobile	+	"</CustMobile>"
					+ "<CustRegMobile>"	+	CustRegMobile	+	"</CustRegMobile>"
					+ "<SeatDetails>"	+	SeatDetails	+	"</SeatDetails>"
					+ "<SeatDetails_Return>"	+	SeatDetails_Return	+	"</SeatDetails_Return>"
					+ "<RouteDetails>"	+	RouteDetails	+	"</RouteDetails>"
					+ "<RouteDetails_Return>"	+	RouteDetails_Return	+	"</RouteDetails_Return>"
					+ "<OnwardTotalAmount>"	+	OnwardTotalAmount	+	"</OnwardTotalAmount>"
					+ "<ReturnTotalAmount>"	+	ReturnTotalAmount	+	"</ReturnTotalAmount>"
					+ "<OnwardSeatTotal>"	+	OnwardSeatTotal	+	"</OnwardSeatTotal>"
					+ "<ReturnSeatTotal>"	+	ReturnSeatTotal	+	"</ReturnSeatTotal>"
					+ "<JourneyType>"	+	journeyType	+	"</JourneyType>"
					+ "<AndroidID>"	+	AndroidID	+	"</AndroidID>"
					+ "<ApplicationType>"	+	ApplicationType	+	"</ApplicationType>"
					+ "<ReferenceNumber>"	+	ReferenceNumber	+	"</ReferenceNumber>"
					+ "<ReferenceNumber_R>"	+	ReferenceNumber_R	+	"</ReferenceNumber_R>"
					+"<VerifyCall>"		+str_Key+"</VerifyCall>"
					+"</GetDiscountCustomerbyCoupons>");
		}
	
		//=============================================================================
				// GetDefaultDiscountCouponCode
				
				public String GetDefaultDiscountCouponCode(int UserId, int JourneyType, String ReferenceNumber,
                                                           String ReferenceNumber_R, String CustEmail, String CustMobile)
				{
					
					return xmlEnvelope(""
							+ "<GetDefaultDiscountCouponCode xmlns=\""+api_url+"\">"
							+ "<ParaAppGeneralInfo>"
							+ "<AndroidId>"	+ Config.AndroidId	+ "</AndroidId>"
							+ "<UserId>"	+ UserId	+ "</UserId>"
							+ "<App_Name>"	+ Config.APP_NAME	+ "</App_Name>"
							+ "<AppVerName>"	+ Config.AppVersionName	+ "</AppVerName>"
							+ "<AppVerCode>"	+ Config.AppVersionCode	+ "</AppVerCode>"
							+ "<AppOsVer>"	+ Config.AppOsVer	+ "</AppOsVer>"
							+ "</ParaAppGeneralInfo>"

							+"<ApplicationType>" + Config.ApplicationType + "</ApplicationType>"
							+"<JourneyType>" + JourneyType + "</JourneyType>"
							+"<ReferenceNumber>" + ReferenceNumber + "</ReferenceNumber>"
							+"<ReferenceNumber_R>" + ReferenceNumber_R + "</ReferenceNumber_R>"
							+"<CustEmail>" + CustEmail + "</CustEmail>"
							+"<CustMobile>" + CustMobile + "</CustMobile>"
							+"<VerifyCall>" + str_Key + "</VerifyCall>"
							+"</GetDefaultDiscountCouponCode>");
				}
		
		//=============================================================================
		//GetDiscountCoupons 
		//=============================================================================
			public String GetDiscountCouponsB2CAPP(int UserId, String CouponCode, double ServiceTaxPer,
                                                   double ServiceTaxRoundUP, double ServiceTaxPer_R, double ServiceTaxRoundUP_R,
                                                   int CompanyID, String CustEmail, String CustMobile, String CustRegMobile,
                                                   String SeatDetails, String SeatDetails_Return, String RouteDetails, String RouteDetails_Return,
                                                   double OnwardTotalAmount, double ReturnTotalAmount, int OnwardSeatTotal, int ReturnSeatTotal, int JournyType,
                                                   String AndroidID, String ReferenceNumber, String ReferenceNumber_R,
                                                   int IsIncludeTax, int IsIncludeTax_R)
			{
				return xmlEnvelope(""
						+ "<GetDiscountCouponsB2CAPP xmlns=\""+api_url+"\">"
						+ "<ParaAppGeneralInfo>"
						+ "<AndroidId>"	+ Config.AndroidId	+ "</AndroidId>"
						+ "<UserId>"	+ UserId	+ "</UserId>"
						+ "<App_Name>"	+ Config.APP_NAME	+ "</App_Name>"
						+ "<AppVerName>"	+ Config.AppVersionName	+ "</AppVerName>"
						+ "<AppVerCode>"	+ Config.AppVersionCode	+ "</AppVerCode>"
						+ "<AppOsVer>"	+ Config.AppOsVer	+ "</AppOsVer>"
						+ "</ParaAppGeneralInfo>"

						+ "<CouponCode>"	+ CouponCode	+ "</CouponCode>"
						+ "<ServiceTaxPer>"	+ ServiceTaxPer	+ "</ServiceTaxPer>"
						+ "<ServiceTaxRoundUP>"	+ ServiceTaxRoundUP	+"</ServiceTaxRoundUP>"
						+ "<ServiceTaxPer_R>"	+ ServiceTaxPer_R	+"</ServiceTaxPer_R>"
						+ "<ServiceTaxRoundUP_R>"	+ ServiceTaxRoundUP_R	+"</ServiceTaxRoundUP_R>"
						+ "<CompanyID>"	+ Config.COMPANY_ID	+ "</CompanyID>"
						+ "<CustEmail>"	+CustEmail	+"</CustEmail>"
						+ "<CustMobile>"	+CustMobile	+"</CustMobile>"
						+ "<CustRegMobile>"	+CustRegMobile	+"</CustRegMobile>"
						+ "<SeatDetails>"	+SeatDetails	+"</SeatDetails>"
						+ "<SeatDetails_Return>"	+SeatDetails_Return	+"</SeatDetails_Return>"
						+ "<RouteDetails>"	+RouteDetails	+"</RouteDetails>"
						+ "<RouteDetails_Return>"	+RouteDetails_Return	+"</RouteDetails_Return>"
						+ "<OnwardTotalAmount>"	+OnwardTotalAmount	+"</OnwardTotalAmount>"
						+ "<ReturnTotalAmount>"	+ReturnTotalAmount	+"</ReturnTotalAmount>"
						+ "<OnwardSeatTotal>"	+OnwardSeatTotal	+"</OnwardSeatTotal>"
						+ "<ReturnSeatTotal>"	+ReturnSeatTotal	+"</ReturnSeatTotal>"
						+ "<JournyType>"	+ JournyType	+"</JournyType>"
						+ "<AndroidID>"	+ AndroidID	+"</AndroidID>"
						+ "<ApplicationType>"	+  Config.ApplicationType	+"</ApplicationType>"
						+ "<ReferenceNumber>"	+  ReferenceNumber	+"</ReferenceNumber>"
						+ "<ReferenceNumber_R>"	+  ReferenceNumber_R	+"</ReferenceNumber_R>"
						+ "<IsIncludeTax>"	+  IsIncludeTax	    + "</IsIncludeTax>"
						+ "<IsIncludeTax_R>"+  IsIncludeTax_R	+ "</IsIncludeTax_R>"
						+ "<VerifyCall>"	+ str_Key	+"</VerifyCall>"
						+ "</GetDiscountCouponsB2CAPP>");
			}	
	//==============================================================================
	// Payment GateWay API - Return
	//==============================================================================
	
			public String Insert_Order_RV2(int UserId, int couponID, String couponCode, double couponRate, String couponType, String CouponRateType,
                                           double orderAmount, double orderDisount, int orderTxnType, String cusutomerName, String customerEmail,
                                           String customerMobile, String customerPhone, int versionCode, String versionName, int companyID, int fromCityID, String fromCitiyName, int toCityID,
                                           String toCityName, int routeID, int routeTimeID, int arrangementID, String arrangemenetName, String routeTime,
                                           String referenceNum, String cityTime, String journeyDate, String pickUpTime, String mainRouteName,
                                           String subRoute, int pickUpID, String pickUpName, String seatNames, String seatList, String seatGenders,
                                           String seatFares, int busType, String busTypeName, double pnrAmount, int totalPax, int totalSeaters,
                                           int totalSleepers, int totalSemiSleepers, double totalSeatersAmt, double totalSleepersAmt,
                                           double totalSemiSleeperAmt, String orderDate, double originalAmount, double discPerAmount, String companyName,
                                           double totalbasefare, double totalserviceTax,

                                           int rcompanyID, int rfromCityID, String rfromCitiyName, int rtoCityID, String rtoCityName, int rrouteID,
                                           int rrouteTimeID, int rarrangementID, String rarrangemenetName, String rrouteTime, String rreferenceNum,
                                           String rcityTime, String rjourneyDate, String rpickUpTime, String rmainRouteName, String rsubRoute, int rpickUpID,
                                           String rpickUpName, String rseatNames, String rseatList, String rseatGenders, String rseatFares, int rbusType,
                                           String rbusTypeName, double rpnrAmount, int rtotalPax, int rtotalSeaters, int rtotalSleepers, int rtotalSemiSleepers,
                                           double rtotalSeatersAmt, double rtotalSleepersAmt, double rtotalSemiSleeperAmt, String rorderDate,
                                           double roriginalAmount, double rdiscPerAmount, String rcompanyName, double rtotalbasefare, double rtotalserviceTax
					, String AndroidID, int ApplicationType, String SeatDetails_Discount, String RouteDetails_Discount,
                                           double STaxPer, String DiscountParaString,
                                           String BaseFareList, String ServiceTaxList, String RoundUpList,
                                           double STaxWithOutRoundUpAmount, double STaxRoundUpAmount, String SeatStringPara,
                                           double rSTaxPer, String rDiscountParaString,
                                           String rBaseFareList, String rServiceTaxList, String rRoundUpList,
                                           double rSTaxWithOutRoundUpAmount, double rSTaxRoundUpAmount, String rSeatStringPara,
                                           int Surcharges, int rSurcharges, int ORSurcharges)
			{
				return xmlEnvelope("<Insert_Order xmlns=\""+api_url+"\">"
						
						+ "<ParaAppGeneralInfo>"
						+ "<AndroidId>"	+ Config.AndroidId	+ "</AndroidId>"
						+ "<UserId>"	+ UserId	+ "</UserId>"
						+ "<App_Name>"	+ Config.APP_NAME	+ "</App_Name>"
						+ "<AppVerName>"	+ Config.AppVersionName	+ "</AppVerName>"
						+ "<AppVerCode>"	+ Config.AppVersionCode	+ "</AppVerCode>"
						+ "<AppOsVer>"	+ Config.AppOsVer	+ "</AppOsVer>"
						+ "</ParaAppGeneralInfo>"		
						
						+ "<Customer_x0020_Master>"
							+ "<CustId>" + UserId + "</CustId>"
							+ "<CustName>" + cusutomerName + "</CustName>"
							+ "<CustEmail>" + customerEmail + "</CustEmail>"
							+ "<CustPassword>123</CustPassword>"
							+ "<CustAdd1>" + "" + "</CustAdd1>"
							+ "<CustAdd2>" + "" + "</CustAdd2>"
							+ "<CustAdd3>" + "" + "</CustAdd3>"
							+ "<CustCity>" + "" + "</CustCity>"
							+ "<CustState>" + "" + "</CustState>"
							+ "<CustCountry>" + "" + "</CustCountry>"
							+ "<CustMobile>" + customerMobile + "</CustMobile>" 
							+ "<CustPhone>" + customerPhone + "</CustPhone>"
							+ "<CustDOB>" + "1754-01-01" + "</CustDOB>"
							+ "<CustStatus>" + 0 + "</CustStatus>"
							+ "<CustNewsletter>" + 0 + "</CustNewsletter>"
							+ "<CustSMS>" + 0 + "</CustSMS>"
							+ "<CustSourceRef>" + "" + "</CustSourceRef>"
							+ "<CustGender>" + "" + "</CustGender>"
							+ "<UpdateStatus>" + 0 + "</UpdateStatus>"
							+ "<AppVerName>" + versionName + "</AppVerName>"
							+ "<AppVerCode>" + versionCode + "</AppVerCode>"
						+ "</Customer_x0020_Master>"
						
						+ "<Order_x0020_Master>"
							+ "<OrderID>0</OrderID>"
							+ "<M_OrderID></M_OrderID>"
							+ "<OrderSenderIP>"+ "192.168.2.1" + "</OrderSenderIP>"
							+ "<CouponID>" + couponID + "</CouponID>"
							+ "<CouponCode>" + couponCode + " </CouponCode>"
							+ "<CouponRate>" + couponRate + "</CouponRate>"
							+ "<CouponType>" + couponType + "</CouponType>"
							+ "<CouponRateType>"+CouponRateType+"</CouponRateType>"
							+ "<CouponMinOrderAmount>0</CouponMinOrderAmount>"
							+ "<OrderAmount>" + orderAmount + "</OrderAmount>"
							+ "<OrderDisount>" + orderDisount + "</OrderDisount>"
							+ "<OrderTxnType>"+	orderTxnType + "</OrderTxnType>"
							+ "<OrderStatus>Pending</OrderStatus>"
							+ "<RefundStatus>0</RefundStatus>"
							+ "<RefundRef></RefundRef>"
							+ "<OrderIsB2CPhone>0</OrderIsB2CPhone>"
							+ "<Remarks></Remarks>"
							+ "<IsMobileSite>2</IsMobileSite>"
							+ "<IsPhoneConfrim>0</IsPhoneConfrim>"
							+ "<Surcharges>"+ ORSurcharges + "</Surcharges>"
						+ "</Order_x0020_Master>"
									
						+ "<paraODetails>"
							+ "<PROP_OrderDetails>"
						
							
								+ "<CustId>" + UserId + "</CustId>"
								+ "<CustName>" + cusutomerName + "</CustName>"
								+ "<CustEmail>" + customerEmail + "</CustEmail>"
								+ "<CustPassword>123</CustPassword>"
								+ "<CustAdd1>" + "" + "</CustAdd1>"
								+ "<CustAdd2>" + "" + "</CustAdd2>"
								+ "<CustAdd3>" + "" + "</CustAdd3>"
								+ "<CustCity>" + "" + "</CustCity>"
								+ "<CustState>" + "" + "</CustState>"
								+ "<CustCountry>" + "" + "</CustCountry>"
								+ "<CustMobile>" + customerMobile + "</CustMobile>" 
								+ "<CustPhone>" + customerPhone + "</CustPhone>"
								+ "<CustDOB>" + "1754-01-01" + "</CustDOB>"
								+ "<CustStatus>" + 0 + "</CustStatus>"
								+ "<CustNewsletter>" + 0 + "</CustNewsletter>"
								+ "<CustSMS>" + 0 + "</CustSMS>"
								+ "<CustSourceRef>" + "" + "</CustSourceRef>"
								+ "<CustGender>" + "" + "</CustGender>"
								+ "<UpdateStatus>" + 0 + "</UpdateStatus>"
								+ "<AppVerName>" + versionName + "</AppVerName>"
								+ "<AppVerCode>" + versionCode + "</AppVerCode>"
								
								
								+ "<OrderID>0</OrderID>"
								+ "<M_OrderID></M_OrderID>"
								+ "<OrderSenderIP>"+ "192.168.2.1" + "</OrderSenderIP>"
								+ "<CouponID>" + couponID + "</CouponID>"
								+ "<CouponCode>" + couponCode + " </CouponCode>"
								+ "<CouponRate>" + couponRate + "</CouponRate>"
								+ "<CouponType>" + couponType + "</CouponType>"
								+ "<CouponRateType>"+CouponRateType+"</CouponRateType>"
								+ "<CouponMinOrderAmount>0</CouponMinOrderAmount>"
								+ "<OrderAmount>" + orderAmount + "</OrderAmount>"
								+ "<OrderDisount>" + orderDisount + "</OrderDisount>"
								+ "<OrderTxnType>"+	orderTxnType + "</OrderTxnType>"
								+ "<OrderStatus>Pending</OrderStatus>"
								+ "<RefundStatus>0</RefundStatus>"
								+ "<RefundRef></RefundRef>"
								+ "<OrderIsB2CPhone>0</OrderIsB2CPhone>"
								+ "<Remarks></Remarks>"
								+ "<IsMobileSite>2</IsMobileSite>"
								+ "<IsPhoneConfrim>0</IsPhoneConfrim>"
								+ "<Surcharges>"+	ORSurcharges + "</Surcharges>"
								
								+ "<OrderDetailID>0</OrderDetailID>"
								+ "<PNRNo>0</PNRNo>"
								+ "<CompanyID>"+ companyID+ "</CompanyID>"
								+ "<FromCityId>"+ fromCityID+ "</FromCityId>"
								+ "<FromCityName>"+ fromCitiyName+ "</FromCityName>"
								+ "<ToCityId>"+ toCityID+ "</ToCityId>"
								+ "<ToCityName>"+ toCityName+ "</ToCityName>"
								+ "<RouteId>"+ routeID+ "</RouteId>"
								+ "<RouteTimeId>"+ routeTimeID+ "</RouteTimeId>"
								+ "<ArrangementID>"+ arrangementID+ "</ArrangementID>"
								+ "<ArrangementName>"+ arrangemenetName+ "</ArrangementName>"
								+ "<RouteTime>"+ routeTime+ "</RouteTime>"
								+ "<ReferenceNumber>"+ referenceNum+ "</ReferenceNumber>"
								+ "<CityTime>"+ cityTime+ "</CityTime>"
								+ "<JourneyDate>"+ journeyDate+ "</JourneyDate>"
								+ "<PickupTime>"+ pickUpTime+ "</PickupTime>"
								+ "<MainRouteName>"+ mainRouteName+ "</MainRouteName>"
								+ "<SubRoute>"+ subRoute+ "</SubRoute>"
								+ "<PickupID>"+ pickUpID+ "</PickupID>"
								+ "<PickupName>"+ pickUpName+ "</PickupName>"
								+ "<SeatNames>"+ seatNames+ "</SeatNames>"
								+ "<SeatList>"+ seatList + "</SeatList>"
								+ "<SeatGenders>"+ seatGenders+ "</SeatGenders>"
								+ "<SeatFares>"+ seatFares+ "</SeatFares>"
								+ "<BusType>"+ busType+ "</BusType>"
								+ "<BusTypeName>"+ busTypeName+ "</BusTypeName>"
								+ "<PNRAmount>"+ pnrAmount+ "</PNRAmount>"
								+ "<TotalPax>"+ totalPax+ "</TotalPax>"
								+ "<TotalSeaters>"+ totalSeaters+ "</TotalSeaters>"
								+ "<TotalSleepers>"+ totalSleepers+ "</TotalSleepers>"
								+ "<TotalSemiSleepers>"+ totalSemiSleepers+ "</TotalSemiSleepers>"
								+ "<TotalSeaterAmt>"+ totalSeatersAmt+ "</TotalSeaterAmt>"
								+ "<TotalSleeperAmt>"+ totalSleepersAmt+ "</TotalSleeperAmt>"
								+ "<TotalSemiSleeperAmt>"+ totalSemiSleeperAmt+ "</TotalSemiSleeperAmt>"
								+ "<DropID>0</DropID>"
								+ "<DropName></DropName>"
								+ "<DropTime></DropTime>"
								+ "<OrderDate>"+ orderDate+ "</OrderDate>"
								+ "<IsPackage>0</IsPackage>"
								+ "<PackageDays>0</PackageDays>"
								+ "<PackageDetails></PackageDetails>"
								+ "<RewardPoint>0</RewardPoint>"
								+ "<OriginalAmount>"+ originalAmount+ "</OriginalAmount>"
								+ "<DiscPerAmount>"+ discPerAmount+ "</DiscPerAmount>"
								+ "<CompanyName>"+ companyName+ "</CompanyName>"
								+ "<PhonePNRAmount>0</PhonePNRAmount>"
								+ "<BaseFare>"+ totalbasefare+ "</BaseFare>"
								+  "<ServiceTax>"+ totalserviceTax+"</ServiceTax>"
								+ "<STaxPer>"+STaxPer+"</STaxPer>"
								+ "<DiscountParaString>" + DiscountParaString +"</DiscountParaString>"
								+ "<BaseFareList>" + BaseFareList + "</BaseFareList>"
								+ "<ServiceTaxList>" + ServiceTaxList + "</ServiceTaxList>"
								+ "<RoundUpList>" + RoundUpList + "</RoundUpList>"
								+ "<STaxWithOutRoundUpAmount>" + STaxWithOutRoundUpAmount + "</STaxWithOutRoundUpAmount>"
								+ "<STaxRoundUpAmount>" + STaxRoundUpAmount + "</STaxRoundUpAmount>"
								+ "<SeatStringPara>" + SeatStringPara + "</SeatStringPara>"
								+ "<Surcharge>"+	Surcharges + "</Surcharge>"
							+ "</PROP_OrderDetails>"
							
							+ "<PROP_OrderDetails>" 
							
							
								+ "<CustId>" + UserId + "</CustId>"
								+ "<CustName>" + cusutomerName + "</CustName>"
								+ "<CustEmail>" + customerEmail + "</CustEmail>"
								+ "<CustPassword>123</CustPassword>"
								+ "<CustAdd1>" + "" + "</CustAdd1>"
								+ "<CustAdd2>" + "" + "</CustAdd2>"
								+ "<CustAdd3>" + "" + "</CustAdd3>"
								+ "<CustCity>" + "" + "</CustCity>"
								+ "<CustState>" + "" + "</CustState>"
								+ "<CustCountry>" + "" + "</CustCountry>"
								+ "<CustMobile>" + customerMobile + "</CustMobile>" 
								+ "<CustPhone>" + customerPhone + "</CustPhone>"
								+ "<CustDOB>" + "1754-01-01" + "</CustDOB>"
								+ "<CustStatus>" + 0 + "</CustStatus>"
								+ "<CustNewsletter>" + 0 + "</CustNewsletter>"
								+ "<CustSMS>" + 0 + "</CustSMS>"
								+ "<CustSourceRef>" + "" + "</CustSourceRef>"
								+ "<CustGender>" + "" + "</CustGender>"
								+ "<UpdateStatus>" + 0 + "</UpdateStatus>"
								+ "<AppVerName>" + versionName + "</AppVerName>"
								+ "<AppVerCode>" + versionCode + "</AppVerCode>"


								+ "<OrderID>0</OrderID>"
								+ "<M_OrderID></M_OrderID>"
								+ "<OrderSenderIP>"+ "192.168.2.1" + "</OrderSenderIP>"
								+ "<CouponID>" + couponID + "</CouponID>"
								+ "<CouponCode>" + couponCode + " </CouponCode>"
								+ "<CouponRate>" + couponRate + "</CouponRate>"
								+ "<CouponType>" + couponType + "</CouponType>"
								+ "<CouponRateType>"+CouponRateType+"</CouponRateType>"
								+ "<CouponMinOrderAmount>0</CouponMinOrderAmount>"
								+ "<OrderAmount>" + orderAmount + "</OrderAmount>"
								+ "<OrderDisount>" + orderDisount + "</OrderDisount>"
								+ "<OrderTxnType>"+	orderTxnType + "</OrderTxnType>"
								+ "<OrderStatus>Pending</OrderStatus>"
								+ "<RefundStatus>0</RefundStatus>"
								+ "<RefundRef></RefundRef>"
								+ "<OrderIsB2CPhone>0</OrderIsB2CPhone>"
								+ "<Remarks></Remarks>"
								+ "<IsMobileSite>2</IsMobileSite>"
								+ "<IsPhoneConfrim>0</IsPhoneConfrim>"
								+ "<Surcharges>"+	ORSurcharges + "</Surcharges>"

								+ "<OrderDetailID>0</OrderDetailID>"
								+ "<PNRNo>0</PNRNo>" 
								+ "<CompanyID>" + rcompanyID + "</CompanyID>" 
								+ "<FromCityId>" + rfromCityID +"</FromCityId>" 
								+ "<FromCityName>" + rfromCitiyName +"</FromCityName>" 
								+ "<ToCityId>" + rtoCityID + "</ToCityId>" 
								+"<ToCityName>" + rtoCityName + "</ToCityName>" 
								+ "<RouteId>" + rrouteID + "</RouteId>" 
								+ "<RouteTimeId>" + rrouteTimeID +"</RouteTimeId>" 
								+ "<ArrangementID>" + rarrangementID + "</ArrangementID>" 
								+ "<ArrangementName>" + rarrangemenetName +"</ArrangementName>" 
								+ "<RouteTime>" + rrouteTime +"</RouteTime>" 
								+ "<ReferenceNumber>" + rreferenceNum +"</ReferenceNumber>" 
								+ "<CityTime>" + rcityTime + "</CityTime>" 
								+ "<JourneyDate>" + rjourneyDate +"</JourneyDate>" 
								+ "<PickupTime>" + rpickUpTime + "</PickupTime>" 
								+ "<MainRouteName>" + rmainRouteName + "</MainRouteName>" 
								+ "<SubRoute>" + rsubRoute + "</SubRoute>"
								+ "<PickupID>" + rpickUpID + "</PickupID>" 
								+ "<PickupName>" + rpickUpName + "</PickupName>" 
								+ "<SeatNames>" + rseatNames + "</SeatNames>" 
								+ "<SeatList>" + rseatList + "</SeatList>" 
								+ "<SeatGenders>" + rseatGenders + "</SeatGenders>" 
								+ "<SeatFares>" + rseatFares + "</SeatFares>"
								+ "<BusType>" + rbusType + "</BusType>"
								+ "<BusTypeName>" + rbusTypeName + "</BusTypeName>"
								+ "<PNRAmount>"+ rpnrAmount +"</PNRAmount>"
								+"<TotalPax>" + rtotalPax + "</TotalPax>"
								+ "<TotalSeaters>" + rtotalSeaters + "</TotalSeaters>" 
								+ "<TotalSleepers>"+ rtotalSleepers + "</TotalSleepers>"
								+ "<TotalSemiSleepers>" + rtotalSemiSleepers + "</TotalSemiSleepers>"
								+ "<TotalSeaterAmt>" + rtotalSeatersAmt + "</TotalSeaterAmt>"
								+ "<TotalSleeperAmt>" + rtotalSleepersAmt + "</TotalSleeperAmt>"
								+ "<TotalSemiSleeperAmt>" + rtotalSemiSleeperAmt + "</TotalSemiSleeperAmt>"
								+ "<DropID>0</DropID>"
								+ "<DropName></DropName>"
								+ "<DropTime></DropTime>" 
								+ "<OrderDate>" + rorderDate + "</OrderDate>" 
								+ "<IsPackage>0</IsPackage>"
								+ "<PackageDays>0</PackageDays>"
								+ "<PackageDetails></PackageDetails>"
								+ "<RewardPoint>0</RewardPoint>"
								+ "<OriginalAmount>" + roriginalAmount +"</OriginalAmount>"
								+ "<DiscPerAmount>" + rdiscPerAmount + "</DiscPerAmount>"
								+ "<CompanyName>" + rcompanyName + "</CompanyName>" 
								+ "<PhonePNRAmount>0</PhonePNRAmount>" 
								+ "<BaseFare>" + rtotalbasefare + "</BaseFare>"
								+  "<ServiceTax>" + rtotalserviceTax +"</ServiceTax>"
								+  "<AndroidID>"+AndroidID+"</AndroidID>"
		                        +  "<ApplicationType>"+ApplicationType+"</ApplicationType>"
								+  "<SeatDetails_Discount>"+SeatDetails_Discount+"</SeatDetails_Discount>"
								+  " <RouteDetails_Discount>"+RouteDetails_Discount+"</RouteDetails_Discount>"
								+ "<STaxPer>"+ rSTaxPer+"</STaxPer>"
								+ "<DiscountParaString>" + rDiscountParaString +"</DiscountParaString>"
								+ "<BaseFareList>" + rBaseFareList + "</BaseFareList>"
								+ "<ServiceTaxList>" + rServiceTaxList + "</ServiceTaxList>"
								+ "<RoundUpList>" + rRoundUpList + "</RoundUpList>"
								+ "<STaxWithOutRoundUpAmount>" + rSTaxWithOutRoundUpAmount + "</STaxWithOutRoundUpAmount>"
								+ "<STaxRoundUpAmount>" + rSTaxRoundUpAmount + "</STaxRoundUpAmount>"
								+ "<SeatStringPara>" + rSeatStringPara + "</SeatStringPara>"
								+ "<Surcharge>"+	rSurcharges + "</Surcharge>"
							+ "</PROP_OrderDetails>"
						+ "</paraODetails>"
								
						+ "<VerifyCall>" + str_Key + "</VerifyCall>" 
						+ "</Insert_Order>");
				}

			//==============================================================================
			// Payment GateWay API - Single
			//==============================================================================
			public String Insert_Order(int UserId, int couponID, String couponCode, double couponRate, String couponType, String CouponRateType, double orderAmount,
                                       double orderDisount, int orderTxnType, String cusutomerName, String customerEmail, String customerMobile,
                                       String customerPhone, int versionCode, String versionName, int companyID, int fromCityID, String fromCitiyName, int toCityID, String toCityName,
                                       int routeID, int routeTimeID, int arrangementID, String arrangemenetName, String routeTime, String referenceNum,
                                       String cityTime, String journeyDate, String pickUpTime, String mainRouteName, String subRoute, int pickUpID,
                                       String pickUpName, String seatNames, String seatList, String seatGenders, String seatFares, int busType,
                                       String busTypeName, double pnrAmount, int totalPax, int totalSeaters, int totalSleepers, int totalSemiSleepers,
                                       double totalSeatersAmt, double totalSleepersAmt, double totalSemiSleeperAmt, String orderDate,
                                       double originalAmount, double discPerAmount, String companyName, double totalbasefare, double totalserviceTax
					, String AndroidID, int ApplicationType, String SeatDetails_Discount, String RouteDetails_Discount,
                                       double STaxPer, String DiscountParaString,
                                       String BaseFareList, String ServiceTaxList, String RoundUpList,
                                       double STaxWithOutRoundUpAmount, double STaxRoundUpAmount, String SeatStringPara,
                                       int Surcharges, int onwardSurcharges)
			{
				return xmlEnvelope("<Insert_Order xmlns=\""+api_url+"\">"
						
						+ "<ParaAppGeneralInfo>"
						+ "<AndroidId>"	+ Config.AndroidId	+ "</AndroidId>"
						+ "<UserId>"	+ UserId	+ "</UserId>"
						+ "<App_Name>"	+ Config.APP_NAME	+ "</App_Name>"
						+ "<AppVerName>"	+ Config.AppVersionName	+ "</AppVerName>"
						+ "<AppVerCode>"	+ Config.AppVersionCode	+ "</AppVerCode>"
						+ "<AppOsVer>"	+ Config.AppOsVer	+ "</AppOsVer>"
						+ "</ParaAppGeneralInfo>"		
						
						+ "<Customer_x0020_Master>"
							+ "<CustId>" + UserId + "</CustId>"
							+ "<CustName>" + cusutomerName + "</CustName>"
							+ "<CustEmail>" + customerEmail + "</CustEmail>"
							+ "<CustPassword>123</CustPassword>"
							+ "<CustAdd1>" + "" + "</CustAdd1>"
							+ "<CustAdd2>" + "" + "</CustAdd2>"
							+ "<CustAdd3>" + "" + "</CustAdd3>"
							+ "<CustCity>" + "" + "</CustCity>"
							+ "<CustState>" + "" + "</CustState>"
							+ "<CustCountry>" + "" + "</CustCountry>"
							+ "<CustMobile>" + customerMobile + "</CustMobile>" 
							+ "<CustPhone>" + customerPhone + "</CustPhone>"
							+ "<CustDOB>" + "1754-01-01" + "</CustDOB>"
							+ "<CustStatus>" + 0 + "</CustStatus>"
							+ "<CustNewsletter>" + 0 + "</CustNewsletter>"
							+ "<CustSMS>" + 0 + "</CustSMS>"
							+ "<CustSourceRef>" + "" + "</CustSourceRef>"
							+ "<CustGender>" + "" + "</CustGender>"
							+ "<UpdateStatus>" + 0 + "</UpdateStatus>"
							+ "<AppVerName>" + versionName + "</AppVerName>"
							+ "<AppVerCode>" + versionCode + "</AppVerCode>"
						+ "</Customer_x0020_Master>"
						
						+ "<Order_x0020_Master>"
							+ "<OrderID>0</OrderID>"
							+ "<M_OrderID></M_OrderID>"
							+ "<OrderSenderIP>"+ "192.168.2.1" + "</OrderSenderIP>"
							+ "<CouponID>" + couponID + "</CouponID>"
							+ "<CouponCode>" + couponCode + " </CouponCode>"
							+ "<CouponRate>" + couponRate + "</CouponRate>"
							+ "<CouponType>" + couponType + "</CouponType>"
							+ "<CouponRateType>"+CouponRateType+"</CouponRateType>"
							+ "<CouponMinOrderAmount>0</CouponMinOrderAmount>"
							+ "<OrderAmount>" + orderAmount + "</OrderAmount>"
							+ "<OrderDisount>" + orderDisount + "</OrderDisount>"
							+ "<OrderTxnType>"+	orderTxnType + "</OrderTxnType>"
							+ "<OrderStatus>Pending</OrderStatus>"
							+ "<RefundStatus>0</RefundStatus>"
							+ "<RefundRef></RefundRef>"
							+ "<OrderIsB2CPhone>0</OrderIsB2CPhone>"
							+ "<Remarks></Remarks>"
							+ "<IsMobileSite>2</IsMobileSite>"
							+ "<IsPhoneConfrim>0</IsPhoneConfrim>"
							+ "<Surcharges>"+	Surcharges + "</Surcharges>"
						+ "</Order_x0020_Master>"
						
						+ "<paraODetails>"
							+ "<PROP_OrderDetails>"
						
								+ "<CustId>" + UserId + "</CustId>"
								+ "<CustName>" + cusutomerName + "</CustName>"
								+ "<CustEmail>" + customerEmail + "</CustEmail>"
								+ "<CustPassword>123</CustPassword>"
								+ "<CustAdd1>" + "" + "</CustAdd1>"
								+ "<CustAdd2>" + "" + "</CustAdd2>"
								+ "<CustAdd3>" + "" + "</CustAdd3>"
								+ "<CustCity>" + "" + "</CustCity>"
								+ "<CustState>" + "" + "</CustState>"
								+ "<CustCountry>" + "" + "</CustCountry>"
								+ "<CustMobile>" + customerMobile + "</CustMobile>" 
								+ "<CustPhone>" + customerPhone + "</CustPhone>"
								+ "<CustDOB>" + "1754-01-01" + "</CustDOB>"
								+ "<CustStatus>" + 0 + "</CustStatus>"
								+ "<CustNewsletter>" + 0 + "</CustNewsletter>"
								+ "<CustSMS>" + 0 + "</CustSMS>"
								+ "<CustSourceRef>" + "" + "</CustSourceRef>"
								+ "<CustGender>" + "" + "</CustGender>"
								+ "<UpdateStatus>" + 0 + "</UpdateStatus>"
								+ "<AppVerName>" + versionName + "</AppVerName>"
								+ "<AppVerCode>" + versionCode + "</AppVerCode>"
								
								
								+ "<OrderID>0</OrderID>"
								+ "<M_OrderID></M_OrderID>"
								+ "<OrderSenderIP>"+ "192.168.2.1" + "</OrderSenderIP>"
								+ "<CouponID>" + couponID + "</CouponID>"
								+ "<CouponCode>" + couponCode + " </CouponCode>"
								+ "<CouponRate>" + couponRate + "</CouponRate>"
								+ "<CouponType>" + couponType + "</CouponType>"
								+ "<CouponRateType>"+CouponRateType+"</CouponRateType>"
								+ "<CouponMinOrderAmount>0</CouponMinOrderAmount>"
								+ "<OrderAmount>" + orderAmount + "</OrderAmount>"
								+ "<OrderDisount>" + orderDisount + "</OrderDisount>"
								+ "<OrderTxnType>"+	orderTxnType + "</OrderTxnType>"
								+ "<OrderStatus>Pending</OrderStatus>"
								+ "<RefundStatus>0</RefundStatus>"
								+ "<RefundRef></RefundRef>"
								+ "<OrderIsB2CPhone>0</OrderIsB2CPhone>"
								+ "<Remarks></Remarks>"
								+ "<IsMobileSite>2</IsMobileSite>"
								+ "<IsPhoneConfrim>0</IsPhoneConfrim>"
								+ "<Surcharges>"+	Surcharges + "</Surcharges>"
								
								
								+ "<OrderDetailID>0</OrderDetailID>"
								+ "<PNRNo>0</PNRNo>"
								+ "<CompanyID>"+ companyID+ "</CompanyID>"
								+ "<FromCityId>"+ fromCityID+ "</FromCityId>"
								+ "<FromCityName>"+ fromCitiyName+ "</FromCityName>"
								+ "<ToCityId>"+ toCityID+ "</ToCityId>"
								+ "<ToCityName>"+ toCityName+ "</ToCityName>"
								+ "<RouteId>"+ routeID+ "</RouteId>"
								+ "<RouteTimeId>"+ routeTimeID+ "</RouteTimeId>"
								+ "<ArrangementID>"+ arrangementID+ "</ArrangementID>"
								+ "<ArrangementName>"+ arrangemenetName+ "</ArrangementName>"
								+ "<RouteTime>"+ routeTime+ "</RouteTime>"
								+ "<ReferenceNumber>"+ referenceNum+ "</ReferenceNumber>"
								+ "<CityTime>"+ cityTime+ "</CityTime>"
								+ "<JourneyDate>"+ journeyDate+ "</JourneyDate>"
								+ "<PickupTime>"+ pickUpTime+ "</PickupTime>"
								+ "<MainRouteName>"+ mainRouteName+ "</MainRouteName>"
								+ "<SubRoute>"+ subRoute+ "</SubRoute>"
								+ "<PickupID>"+ pickUpID+ "</PickupID>"
								+ "<PickupName>"+ pickUpName+ "</PickupName>"
								+ "<SeatNames>"+ seatNames+ "</SeatNames>"
								+ "<SeatList>"+ seatList + "</SeatList>"
								+ "<SeatGenders>"+ seatGenders+ "</SeatGenders>"
								+ "<SeatFares>"+ seatFares+ "</SeatFares>"
								+ "<BusType>"+ busType+ "</BusType>"
								+ "<BusTypeName>"+ busTypeName+ "</BusTypeName>"
								+ "<PNRAmount>"+ pnrAmount+ "</PNRAmount>"
								+ "<TotalPax>"+ totalPax+ "</TotalPax>"
								+ "<TotalSeaters>"+ totalSeaters+ "</TotalSeaters>"
								+ "<TotalSleepers>"+ totalSleepers+ "</TotalSleepers>"
								+ "<TotalSemiSleepers>"+ totalSemiSleepers+ "</TotalSemiSleepers>"
								+ "<TotalSeaterAmt>"+ totalSeatersAmt+ "</TotalSeaterAmt>"
								+ "<TotalSleeperAmt>"+ totalSleepersAmt+ "</TotalSleeperAmt>"
								+ "<TotalSemiSleeperAmt>"+ totalSemiSleeperAmt+ "</TotalSemiSleeperAmt>"
								+ "<DropID>0</DropID>"
								+ "<DropName></DropName>"
								+ "<DropTime></DropTime>"
								+ "<OrderDate>"+ orderDate+ "</OrderDate>"
								+ "<IsPackage>0</IsPackage>"
								+ "<PackageDays>0</PackageDays>"
								+ "<PackageDetails></PackageDetails>"
								+ "<RewardPoint>0</RewardPoint>"
								+ "<OriginalAmount>"+ originalAmount+ "</OriginalAmount>"
								+ "<DiscPerAmount>"+ discPerAmount+ "</DiscPerAmount>"
								+ "<CompanyName>"+ companyName+ "</CompanyName>"
								+ "<PhonePNRAmount>0</PhonePNRAmount>"
								+ "<BaseFare>"+ totalbasefare+ "</BaseFare>"
								+  "<ServiceTax>"+ totalserviceTax+"</ServiceTax>"
								+  "<AndroidID>"+AndroidID+"</AndroidID>"
		                        +  "<ApplicationType>"+ApplicationType+"</ApplicationType>"
								+  "<SeatDetails_Discount>"+SeatDetails_Discount+"</SeatDetails_Discount>"
								+  " <RouteDetails_Discount>"+RouteDetails_Discount+"</RouteDetails_Discount>"
								+ "<STaxPer>"+STaxPer+"</STaxPer>"
								+ "<DiscountParaString>" + DiscountParaString +"</DiscountParaString>"
								+ "<BaseFareList>" + BaseFareList + "</BaseFareList>"
								+ "<ServiceTaxList>" + ServiceTaxList + "</ServiceTaxList>"
								+ "<RoundUpList>" + RoundUpList + "</RoundUpList>"
								+ "<STaxWithOutRoundUpAmount>" + STaxWithOutRoundUpAmount + "</STaxWithOutRoundUpAmount>"
								+ "<STaxRoundUpAmount>" + STaxRoundUpAmount + "</STaxRoundUpAmount>"
								+ "<SeatStringPara>" + SeatStringPara + "</SeatStringPara>"
								+ "<Surcharge>"+	onwardSurcharges + "</Surcharge>"
							+ "</PROP_OrderDetails>"
						+ "</paraODetails>"
							
						+ "<VerifyCall>" + str_Key + "</VerifyCall>" 
						+ "</Insert_Order>");
					}

	//=============================================================================
	// My Booking
	//=============================================================================
	public String getMyBooking(int UserId, String customerEmail, String customerPassword)
	{
		return xmlEnvelope(""+"<GetMyBooking xmlns=\""+ api_url+"\">"
				+ "<ParaAppGeneralInfo>"
				+ "<AndroidId>"	+ Config.AndroidId	+ "</AndroidId>"
				+ "<UserId>"	+ UserId	+ "</UserId>"
				+ "<App_Name>"	+ Config.APP_NAME	+ "</App_Name>"
				+ "<AppVerName>"	+ Config.AppVersionName	+ "</AppVerName>"
				+ "<AppVerCode>"	+ Config.AppVersionCode	+ "</AppVerCode>"
				+ "<AppOsVer>"	+ Config.AppOsVer	+ "</AppOsVer>"
				+ "</ParaAppGeneralInfo>"
				+ "<CustID>"	+ UserId	+ "</CustID>"
				+ "<EmailID>" + customerEmail + "</EmailID>"
				+ "<Password>"	+ customerPassword	+ "</Password>"
				+ "<VerifyCall>"+ str_Key + "</VerifyCall>" 
				+ "</GetMyBooking>");
	}
		
	//=============================================================================
	//Insert Feedback
	//=============================================================================	
	public String Insert_Feedback(int UserId, String type, String subject, String custName, String custEmail, String custMobile, String comment)
	{
		return xmlEnvelope(""+"<Insert_Feedback xmlns=\""+api_url+"\">"
				+ "<ParaAppGeneralInfo>"
				+ "<AndroidId>"	+ Config.AndroidId	+ "</AndroidId>"
				+ "<UserId>"	+ UserId	+ "</UserId>"
				+ "<App_Name>"	+ Config.APP_NAME	+ "</App_Name>"
				+ "<AppVerName>"	+ Config.AppVersionName	+ "</AppVerName>"
				+ "<AppVerCode>"	+ Config.AppVersionCode	+ "</AppVerCode>"
				+ "<AppOsVer>"	+ Config.AppOsVer	+ "</AppOsVer>"
				+ "</ParaAppGeneralInfo>"
				+ "<Type>" + type + "</Type>"
				+ "<Subject>" + subject + "</Subject>"
				+ "<CustName>" + custName + "</CustName>"
				+ "<CustEmail>" + custEmail + "</CustEmail>"
				+ "<CustPhone>" + custMobile + "</CustPhone>"
				+ "<Comment>" + comment + "</Comment>"
				+ "<VerifyCall>" + str_Key + "</VerifyCall>" 
				+ "</Insert_Feedback>");				    
	}	
	
	//=============================================================================
	//CheckVersionInfo 
	//=============================================================================		
	public String CheckVersionInfo(int UserId, int CompanyID, String VersionName, int VersionCode)
	{
		return xmlEnvelope(""
				+ "<CheckVersionInfo xmlns=\""+api_url+"\">"	
				+ "<ParaAppGeneralInfo>"
				+ "<AndroidId>"	+ Config.AndroidId	+ "</AndroidId>"
				+ "<UserId>"	+ UserId	+ "</UserId>"
				+ "<App_Name>"	+ Config.APP_NAME	+ "</App_Name>"
				+ "<AppVerName>"	+ Config.AppVersionName	+ "</AppVerName>"
				+ "<AppVerCode>"	+ Config.AppVersionCode	+ "</AppVerCode>"
				+ "<AppOsVer>"	+ Config.AppOsVer	+ "</AppOsVer>"
				+ "</ParaAppGeneralInfo>"
				+ "<CompanyID>" + CompanyID +"</CompanyID>"
				+ "<VersionName>" + VersionName +"</VersionName>"
				+ "<VersionCode>" + VersionCode + "</VersionCode>"
				+ "<VerifyCall>" + str_Key + "</VerifyCall>"
				+ "</CheckVersionInfo>");
		
	}
	
	//=============================================================================
	//GetDiscount 
	//=============================================================================								
	public String GetDiscount(int companyID, String couponCode, String custEmail, double orderAmount)
	{
		return xmlEnvelope(""
				+ "<GetDiscount xmlns=\""+api_url+"\">"		
				+ "<CompanyID>"+companyID+"</CompanyID>"
				+ "<CouponCode>"+couponCode+"</CouponCode>"
				+"<CustEmail>"+custEmail+"</CustEmail>"
				+"<OrderAmount>"+orderAmount+"</OrderAmount>"
				+"<VerifyCall>"+str_Key+"</VerifyCall>"
				+"</GetDiscount>");
	}
				
	//=============================================================================
	//Get_CompanyDiscountCoupon 
	//=============================================================================
	public String Get_CompanyDiscountCoupon(int companyID)
	{
		return xmlEnvelope(""
				+ "<Get_CompanyDiscountCoupon xmlns=\""+api_url+"\">"		
				+ "<CompanyID>"+companyID+"</CompanyID>"
				+"<VerifyCall>"+str_Key+"</VerifyCall>"
				+"</Get_CompanyDiscountCoupon>");
	}
	//==============================================================================
	// ConfirmCancellation
	//==============================================================================
	public String TicketConfirmCancellation_B2C(int UserId, String PNRNo, String OrderId, String OrderDetailsID, String RefundAmount, String Checksum)
	{
		return xmlEnvelope(""
				+ "<TicketConfirmCancellation_B2C xmlns=\""+ api_url+"\">"
				+ "<ParaAppGeneralInfo>"
				+ "<AndroidId>"	+ Config.AndroidId	+ "</AndroidId>"
				+ "<UserId>"	+ UserId	+ "</UserId>"
				+ "<App_Name>"	+ Config.APP_NAME	+ "</App_Name>"
				+ "<AppVerName>"	+ Config.AppVersionName	+ "</AppVerName>"
				+ "<AppVerCode>"	+ Config.AppVersionCode	+ "</AppVerCode>"
				+ "<AppOsVer>"	+ Config.AppOsVer	+ "</AppOsVer>"
				+ "</ParaAppGeneralInfo>"
				+ "<PNRNo>" + PNRNo	+ "</PNRNo>" 
				+ "<OrderId>" + OrderId	+ "</OrderId>" 
				+ "<OrderDetailsID>" + OrderDetailsID	+ "</OrderDetailsID>" 
				+ "<RefundAmount>" + RefundAmount	+ "</RefundAmount>" 
				+ "<Checksum>" + Checksum	+ "</Checksum>" 
				+ "<VerifyCall>" + str_Key + "</VerifyCall>"
				+ "</TicketConfirmCancellation_B2C>");
	}
	//==============================================================================
	// ConfirmCancellation
	//==============================================================================
	public String TicketCancellationDetails_B2C(int UserId, String PNRNo, String JourneyDate, String Email, int FromCityID, int ToCityID)
	{
		return xmlEnvelope(""
				+ "<TicketCancellationDetails_B2C xmlns=\""+ api_url+"\">"
				+ "<ParaAppGeneralInfo>"
				+ "<AndroidId>"	+ Config.AndroidId	+ "</AndroidId>"
				+ "<UserId>"	+ UserId	+ "</UserId>"
				+ "<App_Name>"	+ Config.APP_NAME	+ "</App_Name>"
				+ "<AppVerName>"	+ Config.AppVersionName	+ "</AppVerName>"
				+ "<AppVerCode>"	+ Config.AppVersionCode	+ "</AppVerCode>"
				+ "<AppOsVer>"	+ Config.AppOsVer	+ "</AppOsVer>"
				+ "</ParaAppGeneralInfo>"
				+ "<PNRNo>" + PNRNo	+ "</PNRNo>" 
				+ "<JourneyDate>" + JourneyDate	+ "</JourneyDate>" 
				+ "<Email>" + Email	+ "</Email>" 
				+ "<FromCityID>" + FromCityID	+ "</FromCityID>" 
				+ "<ToCityID>" + ToCityID	+ "</ToCityID>" 
				+ "<VerifyCall>" + str_Key + "</VerifyCall>"
				+ "</TicketCancellationDetails_B2C>");
	}
	
//==============================================================================
		// ConfirmCancellation
//==============================================================================
		public String GetCancellationPolicyAndTermsAndConditions_B2C(int UserId)
		{
			return xmlEnvelope(""
					+ "<GetCancellationPolicyAndTermsAndConditions_B2C xmlns=\""+ api_url+"\">"
					+ "<ParaAppGeneralInfo>"
					+ "<AndroidId>"	+ Config.AndroidId	+ "</AndroidId>"
					+ "<UserId>"	+ UserId	+ "</UserId>"
					+ "<App_Name>"	+ Config.APP_NAME	+ "</App_Name>"
					+ "<AppVerName>"	+ Config.AppVersionName	+ "</AppVerName>"
					+ "<AppVerCode>"	+ Config.AppVersionCode	+ "</AppVerCode>"
					+ "<AppOsVer>"	+ Config.AppOsVer	+ "</AppOsVer>"
					+ "</ParaAppGeneralInfo>"
					+ "<CompanyID>" + Config.COMPANY_ID	+ "</CompanyID>" 
					+ "<VerifyCall>" + str_Key + "</VerifyCall>"
					+ "</GetCancellationPolicyAndTermsAndConditions_B2C>");
		}
	
	//==============================================================================
	// insertException
	//==============================================================================
	public String insertException(String app_name, String app_version, String exception_datetime, String public_ip, String android_id,
                                  String activity_name, String username, String exception_stack, String device_info)
	{
		return xmlEnvelope(""
				+ "<insertException xmlns=\""+aeh_url+"\">"		
				+ "<app_name>" 		+ 	app_name		+"</app_name>"
				+ "<app_version>" 		+ 	app_version		+"</app_version>"
				+ "<exception_datetime>" + exception_datetime + "</exception_datetime>"
				+ "<public_ip>" + public_ip + "</public_ip>"
				+ "<android_id>" 		+ 	android_id		+"</android_id>"
				+ "<activity_name>" + activity_name + "</activity_name>"
				+ "<username>" + username + "</username>"
				+ "<exception_stack>" 		+ 	exception_stack		+"</exception_stack>"
				+ "<device_info>" + device_info + "</device_info>"
				+"</insertException>");
	}
	
	//==============================================================================
			// ForgetPassword
	//==============================================================================
			public String ForgetPassword(int UserId, String Email)
			{
				return xmlEnvelope(""
						+ "<ForgetPassword xmlns=\""+ api_url+"\">"
						+ "<ParaAppGeneralInfo>"
						+ "<AndroidId>"	+ Config.AndroidId	+ "</AndroidId>"
						+ "<UserId>"	+ UserId	+ "</UserId>"
						+ "<App_Name>"	+ Config.APP_NAME	+ "</App_Name>"
						+ "<AppVerName>"	+ Config.AppVersionName	+ "</AppVerName>"
						+ "<AppVerCode>"	+ Config.AppVersionCode	+ "</AppVerCode>"
						+ "<AppOsVer>"	+ Config.AppOsVer	+ "</AppOsVer>"
						+ "</ParaAppGeneralInfo>"
						+ "<Email>" + Email	+ "</Email>" 
						+ "<VerifyCall>" + str_Key + "</VerifyCall>"
						+ "</ForgetPassword>");
			}
			
//==============================================================================
			// Verfiy_CustomerLogin
//==============================================================================
			public String Verfiy_CustomerLogin(int UserId, String Email, String Password)
			{
				return xmlEnvelope(""
						+ "<Verfiy_CustomerLogin xmlns=\""+ api_url+"\">"
						+ "<ParaAppGeneralInfo>"
						+ "<AndroidId>"	+ Config.AndroidId	+ "</AndroidId>"
						+ "<UserId>"	+ UserId	+ "</UserId>"
						+ "<App_Name>"	+ Config.APP_NAME	+ "</App_Name>"
						+ "<AppVerName>"	+ Config.AppVersionName	+ "</AppVerName>"
						+ "<AppVerCode>"	+ Config.AppVersionCode	+ "</AppVerCode>"
						+ "<AppOsVer>"	+ Config.AppOsVer	+ "</AppOsVer>"
						+ "</ParaAppGeneralInfo>"
						+ "<Email>" + Email	+ "</Email>" 
						+ "<Password>" + Password	+ "</Password>" 
						+ "<VerifyCall>" + str_Key + "</VerifyCall>"
						+ "</Verfiy_CustomerLogin>");
			}
	//==============================================================================
// Payment GateWay API - BlockSeatV2
//==============================================================================
	public String BlockSeatV2(int UserId, int couponID, String couponCode, double couponRate, String couponType, String CouponRateType, double orderAmount,
                              double orderDisount, int orderTxnType, String cusutomerName, String customerEmail, String customerMobile,
                              String customerPhone, int versionCode, String versionName, int companyID, int fromCityID, String fromCitiyName, int toCityID, String toCityName,
                              int routeID, int routeTimeID, int arrangementID, String arrangemenetName, String routeTime, String referenceNum,
                              String cityTime, String journeyDate, String pickUpTime, String mainRouteName, String subRoute, int pickUpID,
                              String pickUpName, String seatNames, String seatList, String seatGenders, String seatFares, int busType,
                              String busTypeName, double pnrAmount, int totalPax, int totalSeaters, int totalSleepers, int totalSemiSleepers,
                              double totalSeatersAmt, double totalSleepersAmt, double totalSemiSleeperAmt, String orderDate,
                              double originalAmount, double discPerAmount, String companyName, double totalbasefare, double totalserviceTax
			, String AndroidID, int ApplicationType, String SeatDetails_Discount, String RouteDetails_Discount,
                              double STaxPer, String DiscountParaString,
                              String BaseFareList, String ServiceTaxList, String RoundUpList,
                              double STaxWithOutRoundUpAmount, double STaxRoundUpAmount, String SeatStringPara,
                              int Surcharges, int ORSurcharges)
	{
		return xmlEnvelope("<BlockSeatV2 xmlns=\""+api_url+"\">"

				+ "<ParaAppGeneralInfo>"
				+ "<AndroidId>"	+ Config.AndroidId	+ "</AndroidId>"
				+ "<UserId>"	+ UserId	+ "</UserId>"
				+ "<App_Name>"	+ Config.APP_NAME	+ "</App_Name>"
				+ "<AppVerName>"	+ Config.AppVersionName	+ "</AppVerName>"
				+ "<AppVerCode>"	+ Config.AppVersionCode	+ "</AppVerCode>"
				+ "<AppOsVer>"	+ Config.AppOsVer	+ "</AppOsVer>"
				+ "</ParaAppGeneralInfo>"

				+ "<Order_x0020_Master>"
				+ "<CustId>" + UserId + "</CustId>"
				+ "<CustName>" + cusutomerName + "</CustName>"
				+ "<CustEmail>" + customerEmail + "</CustEmail>"
				+ "<CustPassword>123</CustPassword>"
				+ "<CustAdd1>" + "" + "</CustAdd1>"
				+ "<CustAdd2>" + "" + "</CustAdd2>"
				+ "<CustAdd3>" + "" + "</CustAdd3>"
				+ "<CustCity>" + "" + "</CustCity>"
				+ "<CustState>" + "" + "</CustState>"
				+ "<CustCountry>" + "" + "</CustCountry>"
				+ "<CustMobile>" + customerMobile + "</CustMobile>"
				+ "<CustPhone>" + customerPhone + "</CustPhone>"
				+ "<CustDOB>" + "1754-01-01" + "</CustDOB>"
				+ "<CustStatus>" + 0 + "</CustStatus>"
				+ "<CustNewsletter>" + 0 + "</CustNewsletter>"
				+ "<CustSMS>" + 0 + "</CustSMS>"
				+ "<CustSourceRef>" + "" + "</CustSourceRef>"
				+ "<CustGender>" + "" + "</CustGender>"
				+ "<UpdateStatus>" + 0 + "</UpdateStatus>"
				+ "<AppVerName>" + versionName + "</AppVerName>"
				+ "<AppVerCode>" + versionCode + "</AppVerCode>"
				+ "<OrderID>0</OrderID>"
				+ "<M_OrderID></M_OrderID>"
				+ "<OrderSenderIP>"+ "192.168.2.1" + "</OrderSenderIP>"
				+ "<CouponID>" + couponID + "</CouponID>"
				+ "<CouponCode>" + couponCode + " </CouponCode>"
				+ "<CouponRate>" + couponRate + "</CouponRate>"
				+ "<CouponType>" + couponType + "</CouponType>"
				+ "<CouponRateType>"+CouponRateType+"</CouponRateType>"
				+ "<CouponMinOrderAmount>0</CouponMinOrderAmount>"
				+ "<OrderAmount>" + orderAmount + "</OrderAmount>"
				+ "<OrderDisount>" + orderDisount + "</OrderDisount>"
				+ "<OrderTxnType>"+	orderTxnType + "</OrderTxnType>"
				+ "<OrderStatus>Pending</OrderStatus>"
				+ "<RefundStatus>0</RefundStatus>"
				+ "<RefundRef></RefundRef>"
				+ "<OrderIsB2CPhone>0</OrderIsB2CPhone>"
				+ "<Remarks></Remarks>"
				+ "<IsMobileSite>2</IsMobileSite>"
				+ "<IsPhoneConfrim>0</IsPhoneConfrim>"
				+ "<Surcharges>"+	ORSurcharges + "</Surcharges>"
				+ "</Order_x0020_Master>"

				+ "<Customer_x0020_Master>"
				+ "<CustId>" + UserId + "</CustId>"
				+ "<CustName>" + cusutomerName + "</CustName>"
				+ "<CustEmail>" + customerEmail + "</CustEmail>"
				+ "<CustPassword>123</CustPassword>"
				+ "<CustAdd1>" + "" + "</CustAdd1>"
				+ "<CustAdd2>" + "" + "</CustAdd2>"
				+ "<CustAdd3>" + "" + "</CustAdd3>"
				+ "<CustCity>" + "" + "</CustCity>"
				+ "<CustState>" + "" + "</CustState>"
				+ "<CustCountry>" + "" + "</CustCountry>"
				+ "<CustMobile>" + customerMobile + "</CustMobile>"
				+ "<CustPhone>" + customerPhone + "</CustPhone>"
				+ "<CustDOB>" + "1754-01-01" + "</CustDOB>"
				+ "<CustStatus>" + 0 + "</CustStatus>"
				+ "<CustNewsletter>" + 0 + "</CustNewsletter>"
				+ "<CustSMS>" + 0 + "</CustSMS>"
				+ "<CustSourceRef>" + "" + "</CustSourceRef>"
				+ "<CustGender>" + "" + "</CustGender>"
				+ "<UpdateStatus>" + 0 + "</UpdateStatus>"
				+ "<AppVerName>" + versionName + "</AppVerName>"
				+ "<AppVerCode>" + versionCode + "</AppVerCode>"
				+ "</Customer_x0020_Master>"


				+ "<paraODetails>"
				+ "<PROP_OrderDetails>"

				+ "<CustId>" + UserId + "</CustId>"
				+ "<CustName>" + cusutomerName + "</CustName>"
				+ "<CustEmail>" + customerEmail + "</CustEmail>"
				+ "<CustPassword>123</CustPassword>"
				+ "<CustAdd1>" + "" + "</CustAdd1>"
				+ "<CustAdd2>" + "" + "</CustAdd2>"
				+ "<CustAdd3>" + "" + "</CustAdd3>"
				+ "<CustCity>" + "" + "</CustCity>"
				+ "<CustState>" + "" + "</CustState>"
				+ "<CustCountry>" + "" + "</CustCountry>"
				+ "<CustMobile>" + customerMobile + "</CustMobile>"
				+ "<CustPhone>" + customerPhone + "</CustPhone>"
				+ "<CustDOB>" + "1754-01-01" + "</CustDOB>"
				+ "<CustStatus>" + 0 + "</CustStatus>"
				+ "<CustNewsletter>" + 0 + "</CustNewsletter>"
				+ "<CustSMS>" + 0 + "</CustSMS>"
				+ "<CustSourceRef>" + "" + "</CustSourceRef>"
				+ "<CustGender>" + "" + "</CustGender>"
				+ "<UpdateStatus>" + 0 + "</UpdateStatus>"
				+ "<AppVerName>" + versionName + "</AppVerName>"
				+ "<AppVerCode>" + versionCode + "</AppVerCode>"


				+ "<OrderID>0</OrderID>"
				+ "<M_OrderID></M_OrderID>"
				+ "<OrderSenderIP>"+ "192.168.2.1" + "</OrderSenderIP>"
				+ "<CouponID>" + couponID + "</CouponID>"
				+ "<CouponCode>" + couponCode + " </CouponCode>"
				+ "<CouponRate>" + couponRate + "</CouponRate>"
				+ "<CouponType>" + couponType + "</CouponType>"
				+ "<CouponRateType>"+CouponRateType+"</CouponRateType>"
				+ "<CouponMinOrderAmount>0</CouponMinOrderAmount>"
				+ "<OrderAmount>" + orderAmount + "</OrderAmount>"
				+ "<OrderDisount>" + orderDisount + "</OrderDisount>"
				+ "<OrderTxnType>"+	orderTxnType + "</OrderTxnType>"
				+ "<OrderStatus>Pending</OrderStatus>"
				+ "<RefundStatus>0</RefundStatus>"
				+ "<RefundRef></RefundRef>"
				+ "<OrderIsB2CPhone>0</OrderIsB2CPhone>"
				+ "<Remarks></Remarks>"
				+ "<IsMobileSite>2</IsMobileSite>"
				+ "<IsPhoneConfrim>0</IsPhoneConfrim>"
				+ "<Surcharges>"+	ORSurcharges + "</Surcharges>"

				+ "<OrderDetailID>0</OrderDetailID>"
				+ "<PNRNo>0</PNRNo>"
				+ "<CompanyID>"+ companyID+ "</CompanyID>"
				+ "<FromCityId>"+ fromCityID+ "</FromCityId>"
				+ "<FromCityName>"+ fromCitiyName+ "</FromCityName>"
				+ "<ToCityId>"+ toCityID+ "</ToCityId>"
				+ "<ToCityName>"+ toCityName+ "</ToCityName>"
				+ "<RouteId>"+ routeID+ "</RouteId>"
				+ "<RouteTimeId>"+ routeTimeID+ "</RouteTimeId>"
				+ "<ArrangementID>"+ arrangementID+ "</ArrangementID>"
				+ "<ArrangementName>"+ arrangemenetName+ "</ArrangementName>"
				+ "<RouteTime>"+ routeTime+ "</RouteTime>"
				+ "<ReferenceNumber>"+ referenceNum+ "</ReferenceNumber>"
				+ "<CityTime>"+ cityTime+ "</CityTime>"
				+ "<JourneyDate>"+ journeyDate+ "</JourneyDate>"
				+ "<PickupTime>"+ pickUpTime+ "</PickupTime>"
				+ "<MainRouteName>"+ mainRouteName+ "</MainRouteName>"
				+ "<SubRoute>"+ subRoute+ "</SubRoute>"
				+ "<PickupID>"+ pickUpID+ "</PickupID>"
				+ "<PickupName>"+ pickUpName+ "</PickupName>"
				+ "<SeatNames>"+ seatNames+ "</SeatNames>"
				+ "<SeatList>"+ seatList + "</SeatList>"
				+ "<SeatGenders>"+ seatGenders+ "</SeatGenders>"
				+ "<SeatFares>"+ seatFares+ "</SeatFares>"
				+ "<BusType>"+ busType+ "</BusType>"
				+ "<BusTypeName>"+ busTypeName+ "</BusTypeName>"
				+ "<PNRAmount>"+ pnrAmount+ "</PNRAmount>"
				+ "<TotalPax>"+ totalPax+ "</TotalPax>"
				+ "<TotalSeaters>"+ totalSeaters+ "</TotalSeaters>"
				+ "<TotalSleepers>"+ totalSleepers+ "</TotalSleepers>"
				+ "<TotalSemiSleepers>"+ totalSemiSleepers+ "</TotalSemiSleepers>"
				+ "<TotalSeaterAmt>"+ totalSeatersAmt+ "</TotalSeaterAmt>"
				+ "<TotalSleeperAmt>"+ totalSleepersAmt+ "</TotalSleeperAmt>"
				+ "<TotalSemiSleeperAmt>"+ totalSemiSleeperAmt+ "</TotalSemiSleeperAmt>"
				+ "<DropID>0</DropID>"
				+ "<DropName></DropName>"
				+ "<DropTime></DropTime>"
				+ "<OrderDate>"+ orderDate+ "</OrderDate>"
				+ "<IsPackage>0</IsPackage>"
				+ "<PackageDays>0</PackageDays>"
				+ "<PackageDetails></PackageDetails>"
				+ "<RewardPoint>0</RewardPoint>"
				+ "<OriginalAmount>"+ originalAmount+ "</OriginalAmount>"
				+ "<DiscPerAmount>"+ discPerAmount+ "</DiscPerAmount>"
				+ "<CompanyName>"+ companyName+ "</CompanyName>"
				+ "<PhonePNRAmount>0</PhonePNRAmount>"
				+ "<BaseFare>"+ totalbasefare+ "</BaseFare>"
				+  "<ServiceTax>"+ totalserviceTax+"</ServiceTax>"
				+  "<AndroidID>"+AndroidID+"</AndroidID>"
				+  "<ApplicationType>"+ApplicationType+"</ApplicationType>"
				+  "<SeatDetails_Discount>"+SeatDetails_Discount+"</SeatDetails_Discount>"
				+  " <RouteDetails_Discount>"+RouteDetails_Discount+"</RouteDetails_Discount>"
				+ "<STaxPer>"+STaxPer+"</STaxPer>"
				+ "<DiscountParaString>" + DiscountParaString +"</DiscountParaString>"
				+ "<BaseFareList>" + BaseFareList + "</BaseFareList>"
				+ "<ServiceTaxList>" + ServiceTaxList + "</ServiceTaxList>"
				+ "<RoundUpList>" + RoundUpList + "</RoundUpList>"
				+ "<STaxWithOutRoundUpAmount>" + STaxWithOutRoundUpAmount + "</STaxWithOutRoundUpAmount>"
				+ "<STaxRoundUpAmount>" + STaxRoundUpAmount + "</STaxRoundUpAmount>"
				+ "<SeatStringPara>" + SeatStringPara + "</SeatStringPara>"
				+ "<Surcharge>"+	Surcharges + "</Surcharge>"
				+ "</PROP_OrderDetails>"
				+ "</paraODetails>"

				+ "<VerifyCall>" + str_Key + "</VerifyCall>"
				+ "</BlockSeatV2>");
	}
	//==============================================================================
//Payment GateWay API - BlockSeatV2
//==============================================================================
	public String BlockSeatV2_R(int UserId, int couponID, String couponCode, double couponRate, String couponType, String CouponRateType,
                                double orderAmount, double orderDisount, int orderTxnType, String cusutomerName, String customerEmail,
                                String customerMobile, String customerPhone, int versionCode, String versionName, int companyID, int fromCityID, String fromCitiyName, int toCityID,
                                String toCityName, int routeID, int routeTimeID, int arrangementID, String arrangemenetName, String routeTime,
                                String referenceNum, String cityTime, String journeyDate, String pickUpTime, String mainRouteName,
                                String subRoute, int pickUpID, String pickUpName, String seatNames, String seatList, String seatGenders,
                                String seatFares, int busType, String busTypeName, double pnrAmount, int totalPax, int totalSeaters,
                                int totalSleepers, int totalSemiSleepers, double totalSeatersAmt, double totalSleepersAmt,
                                double totalSemiSleeperAmt, String orderDate, double originalAmount, double discPerAmount, String companyName,
                                double totalbasefare, double totalserviceTax,

                                int rcompanyID, int rfromCityID, String rfromCitiyName, int rtoCityID, String rtoCityName, int rrouteID,
                                int rrouteTimeID, int rarrangementID, String rarrangemenetName, String rrouteTime, String rreferenceNum,
                                String rcityTime, String rjourneyDate, String rpickUpTime, String rmainRouteName, String rsubRoute, int rpickUpID,
                                String rpickUpName, String rseatNames, String rseatList, String rseatGenders, String rseatFares, int rbusType,
                                String rbusTypeName, double rpnrAmount, int rtotalPax, int rtotalSeaters, int rtotalSleepers, int rtotalSemiSleepers,
                                double rtotalSeatersAmt, double rtotalSleepersAmt, double rtotalSemiSleeperAmt, String rorderDate,
                                double roriginalAmount, double rdiscPerAmount, String rcompanyName, double rtotalbasefare, double rtotalserviceTax
			, String AndroidID, int ApplicationType, String SeatDetails_Discount, String RouteDetails_Discount,
                                double STaxPer, String DiscountParaString,
                                String BaseFareList, String ServiceTaxList, String RoundUpList,
                                double STaxWithOutRoundUpAmount, double STaxRoundUpAmount, String SeatStringPara,
                                double rSTaxPer, String rDiscountParaString,
                                String rBaseFareList, String rServiceTaxList, String rRoundUpList,
                                double rSTaxWithOutRoundUpAmount, double rSTaxRoundUpAmount, String rSeatStringPara
			, int Surcharges, int rSurcharges, int ORSurcharges)
	{
		return xmlEnvelope("<BlockSeatV2 xmlns=\""+api_url+"\">"
				+ "<ParaAppGeneralInfo>"
				+ "<AndroidId>"	+ Config.AndroidId	+ "</AndroidId>"
				+ "<UserId>"	+ UserId	+ "</UserId>"
				+ "<App_Name>"	+ Config.APP_NAME	+ "</App_Name>"
				+ "<AppVerName>"	+ Config.AppVersionName	+ "</AppVerName>"
				+ "<AppVerCode>"	+ Config.AppVersionCode	+ "</AppVerCode>"
				+ "<AppOsVer>"	+ Config.AppOsVer	+ "</AppOsVer>"
				+ "</ParaAppGeneralInfo>"

				+ "<Order_x0020_Master>"
				+ "<CustId>" + UserId + "</CustId>"
				+ "<CustName>" + cusutomerName + "</CustName>"
				+ "<CustEmail>" + customerEmail + "</CustEmail>"
				+ "<CustPassword>123</CustPassword>"
				+ "<CustAdd1>" + "" + "</CustAdd1>"
				+ "<CustAdd2>" + "" + "</CustAdd2>"
				+ "<CustAdd3>" + "" + "</CustAdd3>"
				+ "<CustCity>" + "" + "</CustCity>"
				+ "<CustState>" + "" + "</CustState>"
				+ "<CustCountry>" + "" + "</CustCountry>"
				+ "<CustMobile>" + customerMobile + "</CustMobile>"
				+ "<CustPhone>" + customerPhone + "</CustPhone>"
				+ "<CustDOB>" + "1754-01-01" + "</CustDOB>"
				+ "<CustStatus>" + 0 + "</CustStatus>"
				+ "<CustNewsletter>" + 0 + "</CustNewsletter>"
				+ "<CustSMS>" + 0 + "</CustSMS>"
				+ "<CustSourceRef>" + "" + "</CustSourceRef>"
				+ "<CustGender>" + "" + "</CustGender>"
				+ "<UpdateStatus>" + 0 + "</UpdateStatus>"
				+ "<AppVerName>" + versionName + "</AppVerName>"
				+ "<AppVerCode>" + versionCode + "</AppVerCode>"
				+ "<OrderID>0</OrderID>"
				+ "<M_OrderID></M_OrderID>"
				+ "<OrderSenderIP>"+ "192.168.2.1" + "</OrderSenderIP>"
				+ "<CouponID>" + couponID + "</CouponID>"
				+ "<CouponCode>" + couponCode + " </CouponCode>"
				+ "<CouponRate>" + couponRate + "</CouponRate>"
				+ "<CouponType>" + couponType + "</CouponType>"
				+ "<CouponRateType>"+CouponRateType+"</CouponRateType>"
				+ "<CouponMinOrderAmount>0</CouponMinOrderAmount>"
				+ "<OrderAmount>" + orderAmount + "</OrderAmount>"
				+ "<OrderDisount>" + orderDisount + "</OrderDisount>"
				+ "<OrderTxnType>"+	orderTxnType + "</OrderTxnType>"
				+ "<OrderStatus>Pending</OrderStatus>"
				+ "<RefundStatus>0</RefundStatus>"
				+ "<RefundRef></RefundRef>"
				+ "<OrderIsB2CPhone>0</OrderIsB2CPhone>"
				+ "<Remarks></Remarks>"
				+ "<IsMobileSite>2</IsMobileSite>"
				+ "<IsPhoneConfrim>0</IsPhoneConfrim>"
				+ "<Surcharges>"+	ORSurcharges + "</Surcharges>"
				+ "</Order_x0020_Master>"

				+ "<Customer_x0020_Master>"
				+ "<CustId>" + UserId + "</CustId>"
				+ "<CustName>" + cusutomerName + "</CustName>"
				+ "<CustEmail>" + customerEmail + "</CustEmail>"
				+ "<CustPassword>123</CustPassword>"
				+ "<CustAdd1>" + "" + "</CustAdd1>"
				+ "<CustAdd2>" + "" + "</CustAdd2>"
				+ "<CustAdd3>" + "" + "</CustAdd3>"
				+ "<CustCity>" + "" + "</CustCity>"
				+ "<CustState>" + "" + "</CustState>"
				+ "<CustCountry>" + "" + "</CustCountry>"
				+ "<CustMobile>" + customerMobile + "</CustMobile>"
				+ "<CustPhone>" + customerPhone + "</CustPhone>"
				+ "<CustDOB>" + "1754-01-01" + "</CustDOB>"
				+ "<CustStatus>" + 0 + "</CustStatus>"
				+ "<CustNewsletter>" + 0 + "</CustNewsletter>"
				+ "<CustSMS>" + 0 + "</CustSMS>"
				+ "<CustSourceRef>" + "" + "</CustSourceRef>"
				+ "<CustGender>" + "" + "</CustGender>"
				+ "<UpdateStatus>" + 0 + "</UpdateStatus>"
				+ "<AppVerName>" + versionName + "</AppVerName>"
				+ "<AppVerCode>" + versionCode + "</AppVerCode>"
				+ "</Customer_x0020_Master>"


				+ "<paraODetails>"
				+ "<PROP_OrderDetails>"

				+ "<CustId>" + UserId + "</CustId>"
				+ "<CustName>" + cusutomerName + "</CustName>"
				+ "<CustEmail>" + customerEmail + "</CustEmail>"
				+ "<CustPassword>123</CustPassword>"
				+ "<CustAdd1>" + "" + "</CustAdd1>"
				+ "<CustAdd2>" + "" + "</CustAdd2>"
				+ "<CustAdd3>" + "" + "</CustAdd3>"
				+ "<CustCity>" + "" + "</CustCity>"
				+ "<CustState>" + "" + "</CustState>"
				+ "<CustCountry>" + "" + "</CustCountry>"
				+ "<CustMobile>" + customerMobile + "</CustMobile>"
				+ "<CustPhone>" + customerPhone + "</CustPhone>"
				+ "<CustDOB>" + "1754-01-01" + "</CustDOB>"
				+ "<CustStatus>" + 0 + "</CustStatus>"
				+ "<CustNewsletter>" + 0 + "</CustNewsletter>"
				+ "<CustSMS>" + 0 + "</CustSMS>"
				+ "<CustSourceRef>" + "" + "</CustSourceRef>"
				+ "<CustGender>" + "" + "</CustGender>"
				+ "<UpdateStatus>" + 0 + "</UpdateStatus>"
				+ "<AppVerName>" + versionName + "</AppVerName>"
				+ "<AppVerCode>" + versionCode + "</AppVerCode>"


				+ "<OrderID>0</OrderID>"
				+ "<M_OrderID></M_OrderID>"
				+ "<OrderSenderIP>"+ "192.168.2.1" + "</OrderSenderIP>"
				+ "<CouponID>" + couponID + "</CouponID>"
				+ "<CouponCode>" + couponCode + " </CouponCode>"
				+ "<CouponRate>" + couponRate + "</CouponRate>"
				+ "<CouponType>" + couponType + "</CouponType>"
				+ "<CouponRateType>"+CouponRateType+"</CouponRateType>"
				+ "<CouponMinOrderAmount>0</CouponMinOrderAmount>"
				+ "<OrderAmount>" + orderAmount + "</OrderAmount>"
				+ "<OrderDisount>" + orderDisount + "</OrderDisount>"
				+ "<OrderTxnType>"+	orderTxnType + "</OrderTxnType>"
				+ "<OrderStatus>Pending</OrderStatus>"
				+ "<RefundStatus>0</RefundStatus>"
				+ "<RefundRef></RefundRef>"
				+ "<OrderIsB2CPhone>0</OrderIsB2CPhone>"
				+ "<Remarks></Remarks>"
				+ "<IsMobileSite>2</IsMobileSite>"
				+ "<IsPhoneConfrim>0</IsPhoneConfrim>"
				+ "<Surcharges>"+	ORSurcharges + "</Surcharges>"

				+ "<OrderDetailID>0</OrderDetailID>"
				+ "<PNRNo>0</PNRNo>"
				+ "<CompanyID>"+ companyID+ "</CompanyID>"
				+ "<FromCityId>"+ fromCityID+ "</FromCityId>"
				+ "<FromCityName>"+ fromCitiyName+ "</FromCityName>"
				+ "<ToCityId>"+ toCityID+ "</ToCityId>"
				+ "<ToCityName>"+ toCityName+ "</ToCityName>"
				+ "<RouteId>"+ routeID+ "</RouteId>"
				+ "<RouteTimeId>"+ routeTimeID+ "</RouteTimeId>"
				+ "<ArrangementID>"+ arrangementID+ "</ArrangementID>"
				+ "<ArrangementName>"+ arrangemenetName+ "</ArrangementName>"
				+ "<RouteTime>"+ routeTime+ "</RouteTime>"
				+ "<ReferenceNumber>"+ referenceNum+ "</ReferenceNumber>"
				+ "<CityTime>"+ cityTime+ "</CityTime>"
				+ "<JourneyDate>"+ journeyDate+ "</JourneyDate>"
				+ "<PickupTime>"+ pickUpTime+ "</PickupTime>"
				+ "<MainRouteName>"+ mainRouteName+ "</MainRouteName>"
				+ "<SubRoute>"+ subRoute+ "</SubRoute>"
				+ "<PickupID>"+ pickUpID+ "</PickupID>"
				+ "<PickupName>"+ pickUpName+ "</PickupName>"
				+ "<SeatNames>"+ seatNames+ "</SeatNames>"
				+ "<SeatList>"+ seatList + "</SeatList>"
				+ "<SeatGenders>"+ seatGenders+ "</SeatGenders>"
				+ "<SeatFares>"+ seatFares+ "</SeatFares>"
				+ "<BusType>"+ busType+ "</BusType>"
				+ "<BusTypeName>"+ busTypeName+ "</BusTypeName>"
				+ "<PNRAmount>"+ pnrAmount+ "</PNRAmount>"
				+ "<TotalPax>"+ totalPax+ "</TotalPax>"
				+ "<TotalSeaters>"+ totalSeaters+ "</TotalSeaters>"
				+ "<TotalSleepers>"+ totalSleepers+ "</TotalSleepers>"
				+ "<TotalSemiSleepers>"+ totalSemiSleepers+ "</TotalSemiSleepers>"
				+ "<TotalSeaterAmt>"+ totalSeatersAmt+ "</TotalSeaterAmt>"
				+ "<TotalSleeperAmt>"+ totalSleepersAmt+ "</TotalSleeperAmt>"
				+ "<TotalSemiSleeperAmt>"+ totalSemiSleeperAmt+ "</TotalSemiSleeperAmt>"
				+ "<DropID>0</DropID>"
				+ "<DropName></DropName>"
				+ "<DropTime></DropTime>"
				+ "<OrderDate>"+ orderDate+ "</OrderDate>"
				+ "<IsPackage>0</IsPackage>"
				+ "<PackageDays>0</PackageDays>"
				+ "<PackageDetails></PackageDetails>"
				+ "<RewardPoint>0</RewardPoint>"
				+ "<OriginalAmount>"+ originalAmount+ "</OriginalAmount>"
				+ "<DiscPerAmount>"+ discPerAmount+ "</DiscPerAmount>"
				+ "<CompanyName>"+ companyName+ "</CompanyName>"
				+ "<PhonePNRAmount>0</PhonePNRAmount>"
				+ "<BaseFare>"+ totalbasefare+ "</BaseFare>"
				+  "<ServiceTax>"+ totalserviceTax+"</ServiceTax>"
				+  "<AndroidID>"+AndroidID+"</AndroidID>"
				+  "<ApplicationType>"+ApplicationType+"</ApplicationType>"
				+  "<SeatDetails_Discount>"+SeatDetails_Discount+"</SeatDetails_Discount>"
				+  " <RouteDetails_Discount>"+RouteDetails_Discount+"</RouteDetails_Discount>"
				+ "<STaxPer>"+STaxPer+"</STaxPer>"
				+ "<DiscountParaString>" + DiscountParaString +"</DiscountParaString>"
				+ "<BaseFareList>" + BaseFareList + "</BaseFareList>"
				+ "<ServiceTaxList>" + ServiceTaxList + "</ServiceTaxList>"
				+ "<RoundUpList>" + RoundUpList + "</RoundUpList>"
				+ "<STaxWithOutRoundUpAmount>" + STaxWithOutRoundUpAmount + "</STaxWithOutRoundUpAmount>"
				+ "<STaxRoundUpAmount>" + STaxRoundUpAmount + "</STaxRoundUpAmount>"
				+ "<SeatStringPara>" + SeatStringPara + "</SeatStringPara>"
				+ "<Surcharge>"+	Surcharges + "</Surcharge>"
				+ "</PROP_OrderDetails>"

				+ "<PROP_OrderDetails>"

				+ "<CustId>" + UserId + "</CustId>"
				+ "<CustName>" + cusutomerName + "</CustName>"
				+ "<CustEmail>" + customerEmail + "</CustEmail>"
				+ "<CustPassword>123</CustPassword>"
				+ "<CustAdd1>" + "" + "</CustAdd1>"
				+ "<CustAdd2>" + "" + "</CustAdd2>"
				+ "<CustAdd3>" + "" + "</CustAdd3>"
				+ "<CustCity>" + "" + "</CustCity>"
				+ "<CustState>" + "" + "</CustState>"
				+ "<CustCountry>" + "" + "</CustCountry>"
				+ "<CustMobile>" + customerMobile + "</CustMobile>"
				+ "<CustPhone>" + customerPhone + "</CustPhone>"
				+ "<CustDOB>" + "1754-01-01" + "</CustDOB>"
				+ "<CustStatus>" + 0 + "</CustStatus>"
				+ "<CustNewsletter>" + 0 + "</CustNewsletter>"
				+ "<CustSMS>" + 0 + "</CustSMS>"
				+ "<CustSourceRef>" + "" + "</CustSourceRef>"
				+ "<CustGender>" + "" + "</CustGender>"
				+ "<UpdateStatus>" + 0 + "</UpdateStatus>"
				+ "<AppVerName>" + versionName + "</AppVerName>"
				+ "<AppVerCode>" + versionCode + "</AppVerCode>"


				+ "<OrderID>0</OrderID>"
				+ "<M_OrderID></M_OrderID>"
				+ "<OrderSenderIP>"+ "192.168.2.1" + "</OrderSenderIP>"
				+ "<CouponID>" + couponID + "</CouponID>"
				+ "<CouponCode>" + couponCode + " </CouponCode>"
				+ "<CouponRate>" + couponRate + "</CouponRate>"
				+ "<CouponType>" + couponType + "</CouponType>"
				+ "<CouponRateType>"+CouponRateType+"</CouponRateType>"
				+ "<CouponMinOrderAmount>0</CouponMinOrderAmount>"
				+ "<OrderAmount>" + orderAmount + "</OrderAmount>"
				+ "<OrderDisount>" + orderDisount + "</OrderDisount>"
				+ "<OrderTxnType>"+	orderTxnType + "</OrderTxnType>"
				+ "<OrderStatus>Pending</OrderStatus>"
				+ "<RefundStatus>0</RefundStatus>"
				+ "<RefundRef></RefundRef>"
				+ "<OrderIsB2CPhone>0</OrderIsB2CPhone>"
				+ "<Remarks></Remarks>"
				+ "<IsMobileSite>2</IsMobileSite>"
				+ "<IsPhoneConfrim>0</IsPhoneConfrim>"
				+ "<Surcharges>"+ ORSurcharges + "</Surcharges>"

				+ "<OrderDetailID>0</OrderDetailID>"
				+ "<PNRNo>0</PNRNo>"
				+ "<CompanyID>"+ rcompanyID+ "</CompanyID>"
				+ "<FromCityId>"+ rfromCityID+ "</FromCityId>"
				+ "<FromCityName>"+ rfromCitiyName+ "</FromCityName>"
				+ "<ToCityId>"+ rtoCityID+ "</ToCityId>"
				+ "<ToCityName>"+ rtoCityName+ "</ToCityName>"
				+ "<RouteId>"+ rrouteID+ "</RouteId>"
				+ "<RouteTimeId>"+ rrouteTimeID+ "</RouteTimeId>"
				+ "<ArrangementID>"+ rarrangementID+ "</ArrangementID>"
				+ "<ArrangementName>"+ rarrangemenetName+ "</ArrangementName>"
				+ "<RouteTime>"+ rrouteTime+ "</RouteTime>"
				+ "<ReferenceNumber>"+ rreferenceNum+ "</ReferenceNumber>"
				+ "<CityTime>"+ rcityTime+ "</CityTime>"
				+ "<JourneyDate>"+ rjourneyDate+ "</JourneyDate>"
				+ "<PickupTime>"+ rpickUpTime+ "</PickupTime>"
				+ "<MainRouteName>"+ rmainRouteName+ "</MainRouteName>"
				+ "<SubRoute>"+ rsubRoute+ "</SubRoute>"
				+ "<PickupID>"+ rpickUpID+ "</PickupID>"
				+ "<PickupName>"+ rpickUpName+ "</PickupName>"
				+ "<SeatNames>"+ rseatNames+ "</SeatNames>"
				+ "<SeatList>"+ rseatList + "</SeatList>"
				+ "<SeatGenders>"+ rseatGenders+ "</SeatGenders>"
				+ "<SeatFares>"+ rseatFares+ "</SeatFares>"
				+ "<BusType>"+ rbusType+ "</BusType>"
				+ "<BusTypeName>"+ rbusTypeName+ "</BusTypeName>"
				+ "<PNRAmount>"+ rpnrAmount+ "</PNRAmount>"
				+ "<TotalPax>"+ rtotalPax+ "</TotalPax>"
				+ "<TotalSeaters>"+ rtotalSeaters+ "</TotalSeaters>"
				+ "<TotalSleepers>"+ rtotalSleepers+ "</TotalSleepers>"
				+ "<TotalSemiSleepers>"+ rtotalSemiSleepers+ "</TotalSemiSleepers>"
				+ "<TotalSeaterAmt>"+ rtotalSeatersAmt+ "</TotalSeaterAmt>"
				+ "<TotalSleeperAmt>"+ rtotalSleepersAmt+ "</TotalSleeperAmt>"
				+ "<TotalSemiSleeperAmt>"+ rtotalSemiSleeperAmt+ "</TotalSemiSleeperAmt>"
				+ "<DropID>0</DropID>"
				+ "<DropName></DropName>"
				+ "<DropTime></DropTime>"
				+ "<OrderDate>"+ rorderDate+ "</OrderDate>"
				+ "<IsPackage>0</IsPackage>"
				+ "<PackageDays>0</PackageDays>"
				+ "<PackageDetails></PackageDetails>"
				+ "<RewardPoint>0</RewardPoint>"
				+ "<OriginalAmount>"+ roriginalAmount+ "</OriginalAmount>"
				+ "<DiscPerAmount>"+ rdiscPerAmount+ "</DiscPerAmount>"
				+ "<CompanyName>"+ rcompanyName+ "</CompanyName>"
				+ "<PhonePNRAmount>0</PhonePNRAmount>"
				+ "<BaseFare>"+ rtotalbasefare+ "</BaseFare>"
				+  "<ServiceTax>"+ rtotalserviceTax+"</ServiceTax>"
				+  "<AndroidID>"+AndroidID+"</AndroidID>"
				+  "<ApplicationType>"+ApplicationType+"</ApplicationType>"
				+  "<SeatDetails_Discount>"+SeatDetails_Discount+"</SeatDetails_Discount>"
				+  " <RouteDetails_Discount>"+RouteDetails_Discount+"</RouteDetails_Discount>"
				+ "<STaxPer>"+rSTaxPer+"</STaxPer>"
				+ "<DiscountParaString>" + rDiscountParaString +"</DiscountParaString>"
				+ "<BaseFareList>" + rBaseFareList + "</BaseFareList>"
				+ "<ServiceTaxList>" + rServiceTaxList + "</ServiceTaxList>"
				+ "<RoundUpList>" + rRoundUpList + "</RoundUpList>"
				+ "<STaxWithOutRoundUpAmount>" + rSTaxWithOutRoundUpAmount + "</STaxWithOutRoundUpAmount>"
				+ "<STaxRoundUpAmount>" + rSTaxRoundUpAmount + "</STaxRoundUpAmount>"
				+ "<SeatStringPara>" + rSeatStringPara + "</SeatStringPara>"
				+ "<Surcharge>"+ rSurcharges + "</Surcharge>"
				+ "</PROP_OrderDetails>"

				+ "</paraODetails>"

				+ "<VerifyCall>" + str_Key + "</VerifyCall>"
				+ "</BlockSeatV2>");
	}*/
}