package com.infinityinfoway.aictsl.bean;

import java.io.Serializable;

public class BannerInfoBean implements Serializable
{
	private static final long serialVersionUID = 1L;
	public String IM_ID, IM_Type, IM_Message, IM_FromDate, IM_ToDate,
			IM_Duration;

	public void release() 
	{
		IM_ID = null;
		IM_Type = null;
		IM_Message = null;
		IM_FromDate = null;
		IM_ToDate = null;
		IM_Duration = null;

	}
}
