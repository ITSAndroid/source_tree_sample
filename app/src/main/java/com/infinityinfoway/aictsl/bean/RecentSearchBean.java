package com.infinityinfoway.aictsl.bean;

import java.io.Serializable;

public class RecentSearchBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String ID, FROM_ID, TO_ID, FROM_CITY, TO_CITY, DATE;

	public void release() {
		ID = null;
		FROM_ID = null;
		TO_ID = null;
		FROM_CITY = null;
		TO_CITY = null;
		DATE = null;

		System.gc();
	}

}
