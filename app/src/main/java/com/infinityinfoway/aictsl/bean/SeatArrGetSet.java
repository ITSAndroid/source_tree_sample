package com.infinityinfoway.aictsl.bean;

import android.os.Parcel;
import android.os.Parcelable;

public class SeatArrGetSet implements Parcelable {
	
	private String seatNo;
	private String isLadiesSeat;
	private int seatType;
	private int seatCategory;
	private double seatRate;
	private int row;
	private int column;
	private int rowSpan;
	private int columnSpan;
	private int blockType;
	private double baseFare;
	private double serviceTax;
	private String upLowBerth;
	private String available;
	private int isLowPrice,Surcharges;
	private String gender;
	private double totalRate;
	
	public void setSeatNo(String seatNo) {
		this.seatNo = seatNo;
	}
	public void setTotalRate (double totalRate){
		this.totalRate = totalRate;
	}
	public void setSeatIDs(int seatIDs) {
		//int seatIDs1 = seatIDs;
	}
	public String getSeatNo() {
		return seatNo;
	}
	public double getTotalRate (){
		return totalRate;
	}
	
	public void setIsLadiesSeat(String isLadiesSeat){
		this.isLadiesSeat = isLadiesSeat;
	}
	
	public String getIsLadiesSeat(){
		return isLadiesSeat;
	}
	
	
	public void setSeatType (int seatType){
		this.seatType = seatType;
	}
	
	public int getSeatType(){
		return seatType;
	}
	
	
	public void setSeatCategory (int seatCategory){
		this.seatCategory = seatCategory;
	}
	
	public int getSeatCategory(){
		return seatCategory;
	}
	
	
	public void setSeatRate (double seatRate){
		this.seatRate = seatRate;
	}
	
	public double getSeatRate (){
		return seatRate;
	}
	public void setBaseFare (double baseFare){
		this.baseFare = baseFare;
	}
	
	public double getBaseFare (){
		return baseFare;
	}
	public void setServiceTax (double serviceTax){
		this.serviceTax = serviceTax;
	}
	
	public double getServiceTax (){
		return serviceTax;
	}
	
	public void setRow (int row){
		this.row = row;
	}
	
	public int getRow(){
		return row;
	}
	
	
	public void setColumn (int column){
		this.column = column;
	}
	
	public int getColumn(){
		return column;
	}
	
	
	public void setRowSpan(int rowSpan){
		this.rowSpan = rowSpan;
	}
	
	public int getRowSpan(){
		return rowSpan;
	}
	
	
	public void setColumnSpan (int columnSpan){
		this.columnSpan = columnSpan;
	}
	
	public int getColumnSpan (){
		return columnSpan;
	}
	
	
	public void setBlockType (int blockType){
		this.blockType = blockType;
	}
	
	public int getBlockType(){
		return blockType;
	}
	
	
	public void setUpLowBerth (String upLowBerth){
		this.upLowBerth = upLowBerth;
	}
	
	public String getUpLowBerth()
	{
		return upLowBerth;
	}
	
	
	public void setAvailable(String available){
		this.available = available;
	}
	
	public String getAvailable(){
		return available;
	}
	
	
	public void setIsLowPrice(int isLowPrice){
		this.isLowPrice = isLowPrice;
	}
	
	public int getIsLowPrice(){
		return isLowPrice;
	}
	public void setSurcharges(int Surcharges){
		this.Surcharges = Surcharges;
	}
	
	public int getSurcharges(){
		return Surcharges;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	
	public String getGender() {
		return gender;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
	}
}