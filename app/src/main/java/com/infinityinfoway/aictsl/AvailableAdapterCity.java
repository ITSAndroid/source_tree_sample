package com.infinityinfoway.aictsl;


import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.StringTokenizer;

@SuppressLint("ViewHolder")
public class AvailableAdapterCity extends ArrayAdapter<String> {

    Context context;
    String[] routeName, routeTime,BusNo;

    double acSeatRate[], acSlumberRate[], acSleeperRate[], nonAcSeatRate[], nonAcSlumberRate[], nonAcSleeperRate[];
    int[]  BusID, i;
    String[] RouteAmenities;

    StringTokenizer st;
    String[] Aminities_Id, Aminities_Name, Aminities_Desc;
    String data = "";
    String s = "", s1[] = null;
    String[] items = null;
    TextView txtName, txtDesc;
    View v1;
    TextView btnOk;
    String Am_Name = "", Am_desc = "";
AvailableRoutesActivity AvailableRoutesActivity;

    public AvailableAdapterCity(Context context, String[] routeName, String[] routeTime, int[] BusID, String[] BusNo) {
        super(context, R.layout.list_availableroutes_items, routeName);
        this.context = context;
        this.routeName = routeName;
        this.routeTime = routeTime;
        this.BusID = BusID;
        this.BusNo = BusNo;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View rowView = inflater.inflate(R.layout.list_availableroutes_items, parent, false);

        TextView tv_RouteName = (TextView) rowView.findViewById(R.id.tv_RouteName_list);
        TextView tv_RouteTime = (TextView) rowView.findViewById(R.id.tv_BusTime);
        TextView tv_BusId = (TextView) rowView.findViewById(R.id.tv_BusId);
        TextView tv_BusNo = (TextView) rowView.findViewById(R.id.tv_BusNo);
        LinearLayout llMainList = (LinearLayout) rowView.findViewById(R.id.llMainList);


        if (routeName.toString().trim().length() > 0) {
            tv_RouteName.setText("Route Name :" + " " + routeName[position]);
        }
        if (routeTime.toString().trim().length() > 0) {
            tv_RouteTime.setText( " " + routeTime[position]);
        }
        // Log.v("BusIDlength ",""+BusID.length );
        if (BusID.length > 0) {
            tv_BusId.setText(" " + BusID[position]);
        }
        if (BusNo.length > 0) {
            tv_BusNo.setText( " " + BusNo[position]);
        }

        return rowView;
    }

}
