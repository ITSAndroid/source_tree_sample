package com.infinityinfoway.aictsl;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.infinityinfoway.aictsl.api.APIsConnectivity;
import com.infinityinfoway.aictsl.api.DataConnectivity;
import com.infinityinfoway.aictsl.bean.CityBean;
import com.infinityinfoway.aictsl.bean.RecentSearchBean;
import com.infinityinfoway.aictsl.calendar.CalendarPickerView;
import com.infinityinfoway.aictsl.database.DBConnector;
import com.infinityinfoway.aictsl.database.SharedPreference;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import static com.infinityinfoway.aictsl.R.id.llAvailableBus;

public class ActivityCityBusOperation extends AppCompatActivity {
    private Toolbar toolbar;
    // EasyTracker easyTracker = null;
    APIsConnectivity getAPI = new APIsConnectivity();
    DataConnectivity getData = new DataConnectivity();
    SharedPreference getSharePref;
    private Dialog exitDialog;
    private int sel_SourceCityID, sel_DestinationCityID;
    private String sel_SourceCityName, sel_DestinationCityName, sel_ReturnDate, str_day_r, str_date_r, str_month_r, str_year_r,
            activities[], SCM_CityName[], str_date, str_day, str_month, str_year, sel_JourneyDate;
    private ProgressDialog progDialog;
    private ArrayAdapter<String> sourceArrayAdapter;
    private AutoCompleteTextView actv_SourceCity, actv_DestinationCity;
    private DBConnector getSQLData;
    private TextView tv_day, tv_date, tv_month, tv_year, tv_day_r, tv_date_r, tv_month_r, tv_year_r;
    private Date today, getDate, r_getDate;
    private SimpleDateFormat sdf_day, sdf_date, sdf_month, sdf_year, sdf_full;
    private ImageButton ib_Calender, ib_Calender_r;
    private ArrayList<Date> Hdates;
    private CalendarPickerView dialogView;
    private AlertDialog theDialog;
    TextView recent_search, tv_returnTrip;
    List<CityBean> listCity;
    int temp;
    Button btn_FindBuses;
    LinearLayout llCitybusOper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city_bus_operation);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        llCitybusOper = (LinearLayout) findViewById(R.id.llCitybusOper);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        // Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this,"SelectJourney Activity"));
        // easyTracker = EasyTracker.getInstance(SelectJourneyActivity.this);
        recent_search = (TextView) findViewById(R.id.txt_recent);
        recent_search.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent i1 = new Intent(ActivityCityBusOperation.this, ActivityRecentSearchList.class);
                startActivity(i1);
                try {
                    // overridePendingTransition(R.anim.slide_right,R.anim.slide_left);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }
        });

        tv_returnTrip = (TextView) findViewById(R.id.tv_returnTrip);
        listCity = new ArrayList<CityBean>();
        getSharePref = new SharedPreference(ActivityCityBusOperation.this);
        getSQLData = new DBConnector(ActivityCityBusOperation.this);
        activities = getSharePref.getActivitiesPref().split(",");
        getSharePref.setIsReturnTrip(0);

        //list=new ArrayList<ExceptionDetailBean>();
        //list=getSQLData.getExceptionDetail();
        LinearLayout rl_calender_r = (LinearLayout) findViewById(R.id.rl_calender_r);
        LinearLayout rl_calender = (LinearLayout) findViewById(R.id.rl_calender);
        tv_day = (TextView) findViewById(R.id.tv_day);
        tv_date = (TextView) findViewById(R.id.tv_date);
        tv_month = (TextView) findViewById(R.id.tv_month);
        tv_year = (TextView) findViewById(R.id.tv_year);
        ib_Calender = (ImageButton) findViewById(R.id.ib_Calender);
        tv_day_r = (TextView) findViewById(R.id.tv_day_r);
        tv_date_r = (TextView) findViewById(R.id.tv_date_r);
        tv_month_r = (TextView) findViewById(R.id.tv_month_r);
        tv_year_r = (TextView) findViewById(R.id.tv_year_r);
        ib_Calender_r = (ImageButton) findViewById(R.id.ib_Calender_r);
        actv_SourceCity = (AutoCompleteTextView) findViewById(R.id.actv_SourceCity);
        actv_DestinationCity = (AutoCompleteTextView) findViewById(R.id.actv_DestiCity);
        btn_FindBuses = (Button) findViewById(R.id.btn_FindBuses);

        actv_SourceCity.setSelection(actv_SourceCity.getText().length());
        actv_DestinationCity.setSelection(actv_DestinationCity.getText().length());
        if (activities[10].equals("0")) {
            rl_calender_r.setVisibility(View.GONE);
            tv_returnTrip.setVisibility(View.GONE);
        } else {
            rl_calender_r.setVisibility(View.GONE);
            tv_returnTrip.setVisibility(View.GONE);
        }
        if (getIntent().hasExtra("data")) {
            if (getIntent().getExtras().getSerializable("data") != null) {
                RecentSearchBean data = (RecentSearchBean) getIntent()
                        .getExtras().getSerializable("data");
                if (data.FROM_CITY != null && data.FROM_CITY.length() > 0) {

                    actv_SourceCity.setText(data.FROM_CITY);
                    actv_SourceCity.setSelection(data.FROM_CITY.length());
                    sel_SourceCityName = data.FROM_CITY;
                    sel_SourceCityID = Integer.parseInt(data.FROM_ID);

                }
                if (data.TO_CITY != null && data.TO_CITY.length() > 0) {

                    actv_DestinationCity.setText(data.TO_CITY);
                    actv_DestinationCity.setSelection(data.TO_CITY.length());
                    sel_DestinationCityName = data.TO_CITY;
                    sel_DestinationCityID = Integer.parseInt(data.TO_ID);
                }
            }
        }
        Calendar calender = Calendar.getInstance();
        today = new Date();
        getDate = today;
        r_getDate = today;

        sdf_day = new SimpleDateFormat("EEEE", java.util.Locale.getDefault());
        sdf_date = new SimpleDateFormat("dd", java.util.Locale.getDefault());
        sdf_month = new SimpleDateFormat("MMMM", java.util.Locale.getDefault());
        sdf_year = new SimpleDateFormat("yyyy", java.util.Locale.getDefault());
        sdf_full = new SimpleDateFormat("dd-MM-yyyy", java.util.Locale.getDefault());

        str_day = sdf_day.format(calender.getTime());
        str_date = sdf_date.format(calender.getTime());
        str_month = sdf_month.format(calender.getTime());
        str_year = sdf_year.format(calender.getTime());
        sel_JourneyDate = sdf_full.format(calender.getTime());
        sel_ReturnDate = "";

        tv_day.setText(str_day);
        tv_date.setText(str_date);
        tv_month.setText(str_month);
        tv_year.setText(str_year);
        tv_day_r.setHint(str_day);
        tv_date_r.setHint(str_date);
        tv_month_r.setHint(str_month);
        tv_year_r.setHint(str_year);

        Hdates = new ArrayList<Date>();
        int year = Calendar.getInstance().get(Calendar.YEAR);
        int month = Calendar.getInstance().get(Calendar.MONTH);

        Calendar cal = new GregorianCalendar(year, month, 1);
        do {
            int day = cal.get(Calendar.DAY_OF_WEEK);
            if (day == Calendar.SATURDAY || day == Calendar.SUNDAY) {
                Hdates.add(cal.getTime());
            }
            cal.add(Calendar.DAY_OF_YEAR, 1);
        } while (cal.get(Calendar.MONTH) == month);

        int month2 = month + 1;
        Calendar cal2 = new GregorianCalendar(year, month2, 1);
        do {
            int day = cal2.get(Calendar.DAY_OF_WEEK);
            if (day == Calendar.SATURDAY || day == Calendar.SUNDAY) {
                Hdates.add(cal2.getTime());
            }
            cal2.add(Calendar.DAY_OF_YEAR, 1);
        } while (cal2.get(Calendar.MONTH) == month2);

        int month3 = month + 2;
        Calendar cal3 = new GregorianCalendar(year, month3, 1);
        do {
            int day = cal3.get(Calendar.DAY_OF_WEEK);
            if (day == Calendar.SATURDAY || day == Calendar.SUNDAY) {
                Hdates.add(cal3.getTime());
            }
            cal3.add(Calendar.DAY_OF_YEAR, 1);
        } while (cal3.get(Calendar.MONTH) == month3);
        String tempOnwardDate = sdf_full.format(today);
        String tempReturnDate = sdf_full.format(today);

        try {
            getDate = sdf_full.parse(tempOnwardDate);
            r_getDate = sdf_full.parse(tempReturnDate);
        } catch (ParseException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);


//=====================================================================================
// Source City Loading
//=====================================================================================
        SCM_CityName = getSQLData.getAllSourceCity();
        if (SCM_CityName != null && SCM_CityName.length <= 0) {
            try {
                progDialog = new ProgressDialog(ActivityCityBusOperation.this);
                progDialog.setIndeterminate(true);
                progDialog.setMessage("Loading City...");
                progDialog.setCancelable(false);
                progDialog.show();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                LazyDataConnection Get_CityList = new LazyDataConnection("Get_CityList");
                Get_CityList.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "Get_CityList", getAPI.Get_CityList());
            } else {
                new LazyDataConnection("Get_CityList").execute("Get_CityList", getAPI.Get_CityList());
            }
        } else {
            InputMethodManager imm = (InputMethodManager) getSystemService(
                    Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(actv_SourceCity, InputMethodManager.SHOW_FORCED);

            sourceArrayAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.autocomplete_items_citybus_operation, SCM_CityName);
            actv_SourceCity.setAdapter(sourceArrayAdapter);
            actv_DestinationCity.setAdapter(sourceArrayAdapter);
            actv_SourceCity.setSelection(actv_SourceCity.getText().length());
        }

//=====================================================================================
// Source TextField on ItemClick Listener
//=====================================================================================
        actv_SourceCity.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                for (int i = 0; i < SCM_CityName.length; i++) {
                    if (SCM_CityName[i].equals(actv_SourceCity.getText().toString())) {
                        if (actv_DestinationCity.getText().toString().equals(actv_SourceCity.getText().toString())) {
                            actv_DestinationCity.setText("");
                            sel_DestinationCityName = "";
                            sel_DestinationCityID = 0;
                        }
                        sel_SourceCityName = SCM_CityName[i];
                        sel_SourceCityID = getSQLData.GetSourceCityID(sel_SourceCityName);
                    }
                }
                actv_DestinationCity.setAdapter(sourceArrayAdapter);
                actv_DestinationCity.setSelection(actv_DestinationCity.getText().length());
                InputMethodManager imm = (InputMethodManager) getSystemService(
                        Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(actv_SourceCity.getWindowToken(), 0);
                actv_DestinationCity.requestFocus();
                imm.showSoftInput(actv_DestinationCity, InputMethodManager.SHOW_FORCED);


            }
        });
        actv_SourceCity.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
                String tempSourceCity = arg0.toString().trim();
                String str = tempSourceCity.toString();
                //actv_DestinationCity.setSelection(tempDestinationCity.length());
                for (int i = 0; i < SCM_CityName.length; i++) {
                    if (SCM_CityName[i].compareToIgnoreCase(str) == 0) {
                        sel_SourceCityName = SCM_CityName[i];
                        sel_SourceCityID = getSQLData
                                .GetSourceCityID(sel_SourceCityName);

                        return;
                    } else {
                        sel_SourceCityName = "";
                        sel_SourceCityID = 0;
                    }
                }
            }
        });

//=====================================================================================
// Destination TextField on Focus Event
//=====================================================================================
        actv_DestinationCity.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                for (int j = 0; j < SCM_CityName.length; j++) {
                    if (SCM_CityName[j].equals(actv_DestinationCity.getText().toString())) {
                        if (actv_DestinationCity.getText().toString().equals(actv_SourceCity.getText().toString())) {
                            //CustomToast.showToastMessage(SelectJourneyActivity.this,"From city & To city cannot be same !!!");
                            Toast.makeText(ActivityCityBusOperation.this, "From city & To city cannot be same !!!", Toast.LENGTH_SHORT).show();
                            actv_DestinationCity.setText("");
                        } else {
                            sel_DestinationCityName = SCM_CityName[j];
                            sel_DestinationCityID = getSQLData.GetSourceCityID(sel_DestinationCityName);
                        }
                        break;
                    }
                }
                InputMethodManager imm = (InputMethodManager) getSystemService(
                        Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(actv_SourceCity.getWindowToken(), 0);
                actv_DestinationCity.setSelection(actv_DestinationCity
                        .getText().length());
//                ib_Calender.performClick();
            }
        });
        actv_DestinationCity.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
                String tempDestinationCity = arg0.toString().trim();
                String str = tempDestinationCity.toString();
                //actv_DestinationCity.setSelection(tempDestinationCity.length());
                for (int i = 0; i < SCM_CityName.length; i++) {
                    if (SCM_CityName[i].compareToIgnoreCase(str) == 0) {
                        sel_DestinationCityName = SCM_CityName[i];
                        sel_DestinationCityID = getSQLData
                                .GetSourceCityID(sel_DestinationCityName);

                        return;
                    } else {
                        sel_DestinationCityName = "";
                        sel_DestinationCityID = 0;
                    }
                }
            }
        });
//=====================================================================================
// Departure Date button Click Event
//=====================================================================================
        rl_calender.setEnabled(false);
        ib_Calender.setEnabled(false);
        rl_calender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                ib_Calender.performClick();
            }
        });
        ib_Calender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogView = (CalendarPickerView) getLayoutInflater().inflate(R.layout.calender_dialog_customized, null, false);
                final Calendar nextDay = Calendar.getInstance();
                nextDay.add(Calendar.DAY_OF_MONTH, 60);

                dialogView.init(today, nextDay.getTime()).withSelectedDate(today).withHighlightedDates(Hdates);

                theDialog = new AlertDialog.Builder(ActivityCityBusOperation.this)
                        .setTitle(Html.fromHtml("<font color='#000000'>Select Departure Date</font>"))
                        .setView(dialogView)
                        .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                                sel_JourneyDate = sdf_full.format(dialogView.getSelectedDate().getTime());
                                str_day = sdf_day.format(dialogView.getSelectedDate().getTime());
                                str_date = sdf_date.format(dialogView.getSelectedDate().getTime());
                                str_month = sdf_month.format(dialogView.getSelectedDate().getTime());
                                str_year = sdf_year.format(dialogView.getSelectedDate().getTime());
                                tv_day.setText(str_day);
                                tv_date.setText(str_date);
                                tv_month.setText(str_month);
                                tv_year.setText(str_year);
                                try {
                                    getDate = sdf_full.parse(sel_JourneyDate);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                if (temp == 1) {
                                    if (r_getDate.equals(getDate)) {
                                        getSharePref.setIsReturnTrip(1);
                                    }
                                } else if (r_getDate.equals(getDate)) {
                                    getSharePref.setIsReturnTrip(0);
                                } else if (r_getDate.after(getDate)) {
                                    getSharePref.setIsReturnTrip(1);
                                }
                                if (r_getDate.before(getDate)) {
                                    if (tv_date_r.getText().length() > 0) {
                                        // CustomToast.showToastMessage(SelectJourneyActivity.this,"Onward Journey Date should be less than Return Journey Date !!!");
                                        Toast.makeText(ActivityCityBusOperation.this, "Onward Journey Date should be less than Return Journey Date !!!", Toast.LENGTH_SHORT).show();
                                        tv_day_r.setText("");
                                        tv_date_r.setText("");
                                        tv_month_r.setText("");
                                        tv_year_r.setText("");
                                        sel_ReturnDate = "";
                                        tv_day_r.setHint(sdf_day.format(today));
                                        tv_date_r.setHint(sdf_date.format(today));
                                        tv_month_r.setHint(sdf_month.format(today));
                                        tv_year_r.setHint(sdf_year.format(today));
                                        getSharePref.setIsReturnTrip(0);
                                    }
                                }
                            }
                        })
                        .create();
                theDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface dialogInterface) {
                        dialogView.fixDialogDimens();
                    }
                });
                theDialog.show();
                ((Button) theDialog.findViewById(android.R.id.button3)).setBackgroundResource(R.drawable.btn_main);
                ((Button) theDialog.findViewById(android.R.id.button3)).setTextColor(Color.WHITE);
                ((Button) theDialog.findViewById(android.R.id.button3)).setTypeface(null, Typeface.BOLD);
            }
        });
//=====================================================================================
// Return Date button Click Event
//=====================================================================================		
        rl_calender_r.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                ib_Calender_r.performClick();
            }
        });
        ib_Calender_r.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogView = (CalendarPickerView) getLayoutInflater().inflate(R.layout.calender_dialog_customized, null, false);
                final Calendar nextDay = Calendar.getInstance();
                nextDay.add(Calendar.DAY_OF_MONTH, 60);

                dialogView.init(today, nextDay.getTime()).withSelectedDate(today).withHighlightedDates(Hdates);

                theDialog = new AlertDialog.Builder(ActivityCityBusOperation.this)
                        .setTitle(Html.fromHtml("<font color='#000000'>Select Departure Date</font>"))
                        .setView(dialogView)
                        .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                                sel_ReturnDate = sdf_full.format(dialogView.getSelectedDate().getTime());
                                str_day_r = sdf_day.format(dialogView.getSelectedDate().getTime());
                                str_date_r = sdf_date.format(dialogView.getSelectedDate().getTime());
                                str_month_r = sdf_month.format(dialogView.getSelectedDate().getTime());
                                str_year_r = sdf_year.format(dialogView.getSelectedDate().getTime());
                                tv_day_r.setText(str_day_r);
                                tv_date_r.setText(str_date_r);
                                tv_month_r.setText(str_month_r);
                                tv_year_r.setText(str_year_r);
                                try {
                                    r_getDate = sdf_full.parse(sel_ReturnDate);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                temp = 1;
                                if (r_getDate.equals(getDate)) {
                                    getSharePref.setIsReturnTrip(1);
                                } else if (r_getDate.after(getDate)) {
                                    getSharePref.setIsReturnTrip(1);
                                }
                                if (r_getDate.before(getDate)) {
                                    if (tv_date_r.getText().length() > 0) {
                                        //  CustomToast.showToastMessage(SelectJourneyActivity.this,"Onward Journey Date should be less than Return Journey Date !!!");
                                        Toast.makeText(ActivityCityBusOperation.this, "Onward Journey Date should be less than Return Journey Date !!!", Toast.LENGTH_SHORT).show();
                                        tv_day_r.setText("");
                                        tv_date_r.setText("");
                                        tv_month_r.setText("");
                                        tv_year_r.setText("");
                                        sel_ReturnDate = "";
                                        tv_day_r.setHint(sdf_day.format(today));
                                        tv_date_r.setHint(sdf_date.format(today));
                                        tv_month_r.setHint(sdf_month.format(today));
                                        tv_year_r.setHint(sdf_year.format(today));
                                        getSharePref.setIsReturnTrip(0);
                                    }
                                }
                            /*else if(r_getDate.after(getDate))
                            {
			    				getSharePref.setIsReturnTrip(1);
							}*/
                            }
                        })
                        .create();
                theDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface dialogInterface) {
                        dialogView.fixDialogDimens();
                    }
                });
                theDialog.show();
                ((Button) theDialog.findViewById(android.R.id.button3)).setBackgroundResource(R.drawable.btn_main);
                ((Button) theDialog.findViewById(android.R.id.button3)).setTextColor(Color.WHITE);
                ((Button) theDialog.findViewById(android.R.id.button3)).setTypeface(null, Typeface.BOLD);
            }
        });

//=====================================================================================
// Find Bus Button ClickListener
//=====================================================================================
        findViewById(R.id.btn_FindBuses).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                ConnectionCheck connectionCheck = new ConnectionCheck();
                if (connectionCheck.isNetworkConnected(ActivityCityBusOperation.this)) {

                    if (activities[4].equals("0")) {
                        Intent gotoDisabled = new Intent(getApplicationContext(), ActivityDisabled.class);
                        startActivity(gotoDisabled);
                    } else if (actv_DestinationCity.getText().toString().length() > 0 && actv_DestinationCity.getText().toString().equals(actv_SourceCity.getText().toString())) {
                        //CustomToast.showToastMessage(SelectJourneyActivity.this, "From City & To City Cannot Be Same !!!");
                        Toast.makeText(ActivityCityBusOperation.this, "From City & To City Cannot Be Same !!!", Toast.LENGTH_SHORT).show();
                        actv_DestinationCity.setText("");
                        sel_DestinationCityName = "";
                        sel_DestinationCityID = 0;
                    } else if (sel_SourceCityID <= 0 || sel_DestinationCityID <= 0) {
                        for (int i = 0; i < SCM_CityName.length; i++) {
                            if (SCM_CityName[i].equalsIgnoreCase(actv_SourceCity.getText().toString())) {
                                sel_SourceCityName = SCM_CityName[i];
                                sel_SourceCityID = getSQLData.GetSourceCityID(sel_SourceCityName);
                            } else if (SCM_CityName[i].equalsIgnoreCase(actv_DestinationCity.getText().toString())) {
                                sel_DestinationCityName = SCM_CityName[i];
                                sel_DestinationCityID = getSQLData.GetSourceCityID(sel_DestinationCityName);
                            }
                        }

                        if (actv_SourceCity.getText().toString().trim().length() <= 0 || sel_SourceCityID <= 0) {
                            //CustomToast.showToastMessage(SelectJourneyActivity.this, "Please Type A Valid Source City !!!");
                            Toast.makeText(ActivityCityBusOperation.this, "Please Type A Valid Source City !!!", Toast.LENGTH_SHORT).show();
                        } else if (actv_DestinationCity.getText().toString().trim().length() <= 0 || sel_DestinationCityID <= 0) {
                            //CustomToast.showToastMessage(SelectJourneyActivity.this, "Please Type A Valid Destination City !!!");

                            Toast.makeText(ActivityCityBusOperation.this, "Please Type A Valid Destination City !!!", Toast.LENGTH_SHORT).show();
                        } else {
                            getSharePref.saveFromCityIDName(sel_SourceCityID, sel_SourceCityName);
                            getSharePref.saveToCityIDName(sel_DestinationCityID, sel_DestinationCityName);
                            getSharePref.saveOnwardJourneyDate(sel_JourneyDate, str_day);
                            getSharePref.saveReturnJourneyDate(sel_ReturnDate, str_day_r);
                            Intent gotoAvailableRoots = new Intent(getApplicationContext(), AvailableRoutesActivity.class);
                            startActivity(gotoAvailableRoots);
                            try {
                                //  overridePendingTransition(R.anim.slide_right,R.anim.slide_left);
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        }
                    } else if (actv_SourceCity.getText().toString().trim().length() <= 0) {
                        //CustomToast.showToastMessage(SelectJourneyActivity.this, "Please Type A Valid Source City !!!");
                        Toast.makeText(ActivityCityBusOperation.this, "Please Type A Valid Source City !!!", Toast.LENGTH_SHORT).show();
                    } else if (actv_DestinationCity.getText().toString().trim().length() <= 0) {
                        //CustomToast.showToastMessage(SelectJourneyActivity.this, "Please Type A Valid Destination City !!!");
                        Toast.makeText(ActivityCityBusOperation.this, "Please Type A Valid Destination City !!!", Toast.LENGTH_SHORT).show();
                    } else {
                        getSharePref.saveFromCityIDName(sel_SourceCityID, sel_SourceCityName);
                        getSharePref.saveToCityIDName(sel_DestinationCityID, sel_DestinationCityName);
                        getSharePref.saveOnwardJourneyDate(sel_JourneyDate, str_day);
                        getSharePref.saveReturnJourneyDate(sel_ReturnDate, str_day_r);
                        Intent gotoAvailableRoots = new Intent(getApplicationContext(), AvailableRoutesActivity.class);
                        startActivity(gotoAvailableRoots);
                        try {
                            //  overridePendingTransition(R.anim.slide_right,R.anim.slide_left);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                } else {
                    Snackbar bar = Snackbar.make(llCitybusOper, "Oops! No Internet Connection", Snackbar.LENGTH_LONG);
                    bar.show();

                    // Intent i = new Intent(ActivityCityBusOperation.this, InternetErrorActivity.class);
                    // Toast.makeText(ActivityCityBusOperation.this, "Opps No internet Connection...", Toast.LENGTH_SHORT).show();
                    // startActivity(i);

                }
            }
        });
    }

    //=====================================================================================
// GetAllSources
//=====================================================================================
    public void GetAllSources(String str_RespXML, String str_TagName) {
        Document doc = getData.XMLfromString(str_RespXML);
        NodeList nodes = doc.getElementsByTagName(str_TagName);

        if (nodes.getLength() > 0)
            for (int i = 0; i <= nodes.getLength(); i++) {
                Node e1 = nodes.item(i);
                Element el = (Element) e1;
                CityBean data = new CityBean();
                if (el != null) {
                    try {
                        data.SCM_CITY_ID = Integer.parseInt(el.getElementsByTagName("CityID").item(0).getTextContent());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        data.SCM_CITY_NAME = el.getElementsByTagName("CityName").item(0).getTextContent();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        if (data.SCM_CITY_ID > 0 && data.SCM_CITY_NAME != null && data.SCM_CITY_NAME.length() > 0) {
                            listCity.add(data);
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }
    }

    //=====================================================================================
// Async Task Class
//=====================================================================================
    public class LazyDataConnection extends AsyncTask<String, Void, String> {
        String method;

        public LazyDataConnection(String method) {
            this.method = method;
        }

        @Override
        protected String doInBackground(String... arg0) {
            method = arg0[0];
            return getData.callWebService(arg0[0], arg0[1]);
        }

        protected void onPostExecute(String xmlResponse) {
            if (xmlResponse.equals("")) {
                try {
                    progDialog.dismiss();
                    if (progDialog != null && progDialog.isShowing()) {
                        progDialog.dismiss();
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
				/*Intent i = new Intent(SelectJourneyActivity.this, InternetErrorActivity.class);
				i.putExtra("caller","SelectJourneyActivity");
				startActivity(i);
			*/
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ActivityCityBusOperation.this);
                alertDialogBuilder
                        .setMessage("You are not connected to Active Network Connection.")
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                finish();
                                System.exit(0);
                            }
                        });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();


            } else if (method.equals("Get_CityList")) {
                try {
                    GetAllSources(xmlResponse, "CityList");
                    try {
                        progDialog.dismiss();
                        if (progDialog != null && progDialog.isShowing()) {
                            progDialog.dismiss();
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                    getSQLData.addAllSourceCity(listCity);
                    SCM_CityName = getSQLData.getAllSourceCity();
                    sourceArrayAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.autocomplete_items_citybus_operation, SCM_CityName);
                    actv_SourceCity.setAdapter(sourceArrayAdapter);
                    actv_DestinationCity.setAdapter(sourceArrayAdapter);

                    actv_SourceCity.setSelection(actv_SourceCity.getText()
                            .length());
                    actv_DestinationCity.setSelection(actv_DestinationCity
                            .getText().length());
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

        }
    }


    /* public void	onBackPressed()
     {
         exitDialog=new Dialog(SelectJourneyActivity.this);
         exitDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
         exitDialog.setContentView(R.layout.exitdialog);
         exitDialog.setCancelable(false);
         exitDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
         Button btnNegative=(Button)exitDialog.findViewById(R.id.btnNegative);
         btnNegative.setOnClickListener(new View.OnClickListener() {

             @Override
             public void onClick(View v) {
                 exitDialog.cancel();

             }
         });
         Button btnPositive=(Button)exitDialog.findViewById(R.id.btnPositive);
         btnPositive.setOnClickListener(new View.OnClickListener() {

             @Override
             public void onClick(View arg0) {
                 exitDialog.cancel();
                 finish();
                 System.exit(0);
             }
         });
         exitDialog.show();
     }*/
    @Override
    public void onStart() {
        super.onStart();
        // EasyTracker.getInstance(this).activityStart(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        //EasyTracker.getInstance(this).activityStop(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
            default:
        }
        return super.onOptionsItemSelected(item);

    }
}
