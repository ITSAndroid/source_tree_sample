package com.infinityinfoway.aictsl.database;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;

public class OnwardJourneyDataPref 
{
	

	Context context;
	SharedPreferences getPayData;
	Editor editPref;
	
	public static String COMPANY_ID = "COMPANY_ID";
	public static String COMPANY_NAME = "COMPANY_NAME";
	public static String FROM_CITY_ID = "FROM_CITY_ID";
	public static String FROM_CITY_NAME = "FROM_CITY_NAME";
	public static String TO_CITY_ID = "TO_CITY_ID";
	public static String TO_CITY_NAME = "TO_CITY_NAME";
	public static String ROUTE_ID = "ROUTE_ID";
	public static String ROUTE_TIME_ID = "ROUTE_TIME_ID";
	public static String ROUTE_NAME = "ROUTE_NAME";
	public static String ROUTE_TIME = "ROUTE_TIME";
	public static String CITY_TIME = "CITY_TIME";
	public static String ARRAIVAL_TIME = "ARRAIVAL_TIME";
	public static String BUS_TYPE = "BUS_TYPE";
	public static String BUS_TYPE_NAME = "BUS_TYPE_NAME";
	public static String BOOKING_DATE = "BOOKING_DATE";
	public static String ARRANGEMENT_ID = "ARRANGEMENT_ID";
	public static String ARRANGEMENT_NAME = "ARRANGEMENT_NAME";
	public static String AC_SEAT_RATE = "AC_SEAT_RATE";
	public static String AC_SLEEPER_RATE = "AC_SLEEPER_RATE";
	public static String AC_SLUMBER_RATE = "AC_SLUMBER_RATE";
	public static String NONAC_SEAT_RATE = "NONAC_SEAT_RATE";
	public static String NONAC_SLEEPER_RATE = "NONAC_SLEEPER_RATE";
	public static String NONAC_SLUMBER_RATE = "NONAC_SLUMBER_RATE";
	public static String BOARDING_POINTS = "BOARDING_POINTS";
	public static String DROPPING_POINTS = "DROPPING_POINTS";
	public static String EMPTY_SEATS = "EMPTY_SEATS";
	public static String REFERENCE_NUMBER = "REFERENCE_NUMBER";
	public static String AC_SEAT_SERVICE_TAX = "AC_SEAT_SERVICE_TAX";
	public static String AC_SLP_SERVICE_TAX = "AC_SLP_SERVICE_TAX";
	public static String AC_SLMB_SERVICE_TAX = "AC_SLMB_SERVICE_TAX";
	public static String NONAC_SEAT_SERVICE_TAX = "NONAC_SEAT_SERVICE_TAX";
	public static String NONAC_SLP_SERVICE_TAX = "NONAC_SLP_SERVICE_TAX";
	public static String NONAC_SLMB_SERVICE_TAX = "NONAC_SLMB_SERVICE_TAX";
	public static String BASE_AC_SEAT = "BASE_AC_SEAT";
	public static String BASE_AC_SLP = "BASE_AC_SLP";
	public static String BASE_AC_SLMB = "BASE_AC_SLMB";
	public static String BASE_NONAC_SEAT = "BASE_NONAC_SEAT";
	public static String BASE_NONAC_SLP = "BASE_NONAC_SLP";
	public static String BASE_NONAC_SLMB = "BASE_NONAC_SLMB";
	public static String ISINCLUDETAX = "ISINCLUDETAX";
	public static String SERVICETAX = "SERVICETAX";
	public static String SERVICETAXROUNDUP = "SERVICETAXROUNDUP";
	
	
	private static String TOTAL_SEAT = "TOTAL_SEAT";
	private static String SEATS = "SEATS";
	private static String TOTAL_PAY = "TOTAL_PAY";
	private static String TOTAL_SERVICE_TAX = "TOTAL_SERVICE_TAX";
	private static String TOTAL_BASE_FARE = "TOTAL_BASE_FARE";
	private static String PICKUP_POINT = "PICKUP_POINT";
	private static String PICKUP_ID = "PICKUP_ID";
	private static String PICKUP_NAME = "PICKUP_NAME";
	private static String PICKUP_TIME = "PICKUP_TIME";
	private static String SEAT_NAMES = "SEAT_NAMES";
	private static String TOTAL_SEATERS = "TOTAL_SEATERS";
	private static String TOTAL_SLEEPERS = "TOTAL_SLEEPERS";
	private static String TOTAL_SEMISLEEPERS = "TOTAL_SEMISLEEPERS";
	private static String TOTAL_SEATER_AMT = "TOTAL_SEATER_AMT";
	private static String TOTAL_SLEEPER_AMT = "TOTAL_SLEEPER_AMT";
	private static String TOTAL_SEMISLEEPER_AMT = "TOTAL_SEMISLEEPER_AMT";
	private static String SEAT_FARES = "SEAT_FARES";
	private static String SEAT_GENDERS = "SEAT_GENDERS";
	private static String SEAT_SERVICETAX = "SEAT_SERVICETAX";
	private static String SEAT_BASEFARE = "SEAT_BASEFARE";
	
	private static String SEATDETAILS = "SEATDETAILS";
	private static String SEAT_DETAILS = "SEAT_DETAILS";
	private static String SEAT_SURCHARGES = "SEAT_SURCHARGES";
	
	public OnwardJourneyDataPref(Activity mContext)
	{
		context = mContext;
		getPayData = PreferenceManager.getDefaultSharedPreferences(context);
		editPref = getPayData.edit();
	}
		
//-----------------------------------------------------------------------------------------------
	
	public void saveOnwardJourneyBusDetails(int CompanyID, String CompanyName, int FromCityId, String FromCityName,
                                            int ToCityId, String ToCityName, int RouteID, int RouteTimeID, String RouteName, String RouteTime,
                                            String CityTime, String ArrivalTime, int BusType, String BusTypeName, String BookingDate, int ArrangementID,
                                            String ArrangementName, String AcSeatRate, String AcSleeperRate, String AcSlumberRate, String NonAcSeatRate,
                                            String NonAcSleeperRate, String NonAcSlumberRate, String BoardingPoints, String DroppingPoints, int EmptySeats,
                                            String ReferenceNumber, String AcSeatServiceTax, String AcSlpServiceTax, String AcSlmbServiceTax, String NonAcSeatServiceTax,
                                            String NonAcSlpServiceTax, String NonAcSlmbServiceTax, String BaseAcSeat, String BaseAcSlp, String BaseAcSlmb, String BaseNonAcSeat,
                                            String BaseNonAcSlp, String BaseNonAcSlmb, int BusSeatType, String ServiceTax, String ServiceTaxRoundUp, int IsIncludeTax)
	{
		editPref.putInt(COMPANY_ID, CompanyID);
		editPref.putString(COMPANY_NAME, CompanyName);
		editPref.putInt(FROM_CITY_ID, FromCityId);
		editPref.putString(FROM_CITY_NAME, FromCityName);
		editPref.putInt(TO_CITY_ID, ToCityId);
		editPref.putString(TO_CITY_NAME, ToCityName);
		editPref.putInt(ROUTE_ID, RouteID);
		editPref.putInt(ROUTE_TIME_ID, RouteTimeID);
		editPref.putString(ROUTE_NAME, RouteName);
		editPref.putString(ROUTE_TIME, RouteTime);
		editPref.putString(CITY_TIME, CityTime);
		editPref.putString(ARRAIVAL_TIME, ArrivalTime);
		editPref.putInt(BUS_TYPE, BusType);
		editPref.putString(BUS_TYPE_NAME, BusTypeName);
		editPref.putString(BOOKING_DATE, BookingDate);
		editPref.putInt(ARRANGEMENT_ID, ArrangementID);
		editPref.putString(ARRANGEMENT_NAME, ArrangementName);
		editPref.putString(AC_SEAT_RATE, AcSeatRate);
		editPref.putString(AC_SLEEPER_RATE, AcSleeperRate);
		editPref.putString(AC_SLUMBER_RATE, AcSlumberRate);
		editPref.putString(NONAC_SEAT_RATE, NonAcSeatRate);
		editPref.putString(NONAC_SLEEPER_RATE, NonAcSleeperRate);
		editPref.putString(NONAC_SLUMBER_RATE, NonAcSlumberRate);
		editPref.putString(BOARDING_POINTS, BoardingPoints);
		editPref.putString(DROPPING_POINTS, DroppingPoints);
		editPref.putInt(EMPTY_SEATS, EmptySeats);
		editPref.putString(REFERENCE_NUMBER, ReferenceNumber);
		editPref.putString(AC_SEAT_SERVICE_TAX, AcSeatServiceTax);
		editPref.putString(AC_SLP_SERVICE_TAX, AcSlpServiceTax);
		editPref.putString(AC_SLMB_SERVICE_TAX, AcSlmbServiceTax);
		editPref.putString(NONAC_SEAT_SERVICE_TAX, NonAcSeatServiceTax);
		editPref.putString(NONAC_SLP_SERVICE_TAX, NonAcSlpServiceTax);
		editPref.putString(NONAC_SLMB_SERVICE_TAX, NonAcSlmbServiceTax);
		editPref.putString(BASE_AC_SEAT, BaseAcSeat);
		editPref.putString(BASE_AC_SLP, BaseAcSlp);
		editPref.putString(BASE_AC_SLMB, BaseAcSlmb);
		editPref.putString(BASE_NONAC_SEAT, BaseNonAcSeat);
		editPref.putString(BASE_NONAC_SLP, BaseNonAcSlp);
		editPref.putString(BASE_NONAC_SLMB, BaseNonAcSlmb);
		editPref.putInt("BUSSEATTYPE", BusSeatType);
		editPref.putString(SERVICETAX, ServiceTax);
		editPref.putString(SERVICETAXROUNDUP, ServiceTaxRoundUp);
		editPref.putInt(ISINCLUDETAX, IsIncludeTax);
		editPref.commit();
	}
	public int getBusSeatType(){
		return getPayData.getInt("BUSSEATTYPE", 0);
	}
	public String getServiceTax()
	{
		return getPayData.getString(SERVICETAX, "");
	}
	public String getServiceTaxRoundUp()
	{
		return getPayData.getString(SERVICETAXROUNDUP, "");
	}
	public int getCompanyID() 
	{
		return getPayData.getInt(COMPANY_ID, 0);
	}
	public String getCompanyName()
	{
		return getPayData.getString(COMPANY_NAME, "");
	}
	public int getFromCityId() 
	{
		return getPayData.getInt(FROM_CITY_ID, 0);
	}
	public String getFromCityName()
	{
		return getPayData.getString(FROM_CITY_NAME, "");
	}
	public int getToCityID() 
	{
		return getPayData.getInt(TO_CITY_ID, 0);
	}
	public String getToCityName()
	{
		return getPayData.getString(TO_CITY_NAME, "");
	}
	public int getRouteID() 
	{
		return getPayData.getInt(ROUTE_ID, 0);
	}
	public int getRouteTimeID() 
	{
		return getPayData.getInt(ROUTE_TIME_ID, 0);
	}
	public String getRouteName()
	{
		return getPayData.getString(ROUTE_NAME, "");
	}
	public String getRouteTime()
	{
		return getPayData.getString(ROUTE_TIME, "");
	}
	public String getCityTime()
	{
		return getPayData.getString(CITY_TIME, "");
	}
	public String getArrivalTime()
	{
		return getPayData.getString(ARRAIVAL_TIME, "");
	}
	public int getBusType() 
	{
		return getPayData.getInt(BUS_TYPE, 0);
	}
	public String getBusTypeName()
	{
		return getPayData.getString(BUS_TYPE_NAME, "");
	}
	public String getBookingDate()
	{
		return getPayData.getString(BOOKING_DATE, "");
	}
	public int getArrangementID() 
	{
		return getPayData.getInt(ARRANGEMENT_ID, 0);
	}
	public String getArrangementName()
	{
		return getPayData.getString(ARRANGEMENT_NAME, "");
	}
	public String getAcSeatRate()
	{
		return getPayData.getString(AC_SEAT_RATE, "");
	}
	public String getAcSleeperRate()
	{
		return getPayData.getString(AC_SLEEPER_RATE, "");
	}
	public String getAcSlumberRate()
	{
		return getPayData.getString(AC_SLUMBER_RATE, "");
	}
	public String getNonAcSeatRate()
	{
		return getPayData.getString(NONAC_SEAT_RATE, "");
	}
	public String getNonAcSleeperRate()
	{
		return getPayData.getString(NONAC_SLEEPER_RATE, "");
	}
	public String getNonAcSlumberRate()
	{
		return getPayData.getString(NONAC_SLUMBER_RATE, "");
	}
	public String getBoardingPoints()
	{
		return getPayData.getString(BOARDING_POINTS, "");
	}
	public String getDroppingPoints()
	{
		return getPayData.getString(DROPPING_POINTS, "");
	}
	public int getEmptySeats() 
	{
		return getPayData.getInt(EMPTY_SEATS, 0);
	}
	public String getReferenceNumber()
	{
		return getPayData.getString(REFERENCE_NUMBER, "");
	}
	public String getAcSeatServiceTax()
	{
		return getPayData.getString(AC_SEAT_SERVICE_TAX, "");
	}
	public String getAcSlpServiceTax()
	{
		return getPayData.getString(AC_SLP_SERVICE_TAX, "");
	}
	public String getAcSlmbServiceTax()
	{
		return getPayData.getString(AC_SLMB_SERVICE_TAX, "");
	}
	public String getNonAcSeatServiceTax()
	{
		return getPayData.getString(NONAC_SEAT_SERVICE_TAX, "");
	}
	public String getNonAcSlpServiceTax()
	{
		return getPayData.getString(NONAC_SLP_SERVICE_TAX, "");
	}
	public String getNonAcSlmbServiceTax()
	{
		return getPayData.getString(NONAC_SLMB_SERVICE_TAX, "");
	}
	public String getBaseAcSeat()
	{
		return getPayData.getString(BASE_AC_SEAT, "");
	}
	public String getBaseAcSlp()
	{
		return getPayData.getString(BASE_AC_SLP, "");
	}
	public String getBaseAcSlmb()
	{
		return getPayData.getString(BASE_AC_SLMB, "");
	}
	public String getBaseNonAcSeat()
	{
		return getPayData.getString(BASE_NONAC_SEAT, "");
	}
	public String getBaseNonAcSlp()
	{
		return getPayData.getString(BASE_NONAC_SLP, "");
	}
	public String getBaseNonAcSlmb()
	{
		return getPayData.getString(BASE_NONAC_SLMB, "");
	}
	public int getIsServiceTaxInclude() 
	{
		return getPayData.getInt(ISINCLUDETAX, 0);
	}
	
	
//-----------------------------------------------------------------------------------------------	
	
	public void saveOnwardJourneySeatDetails(int totalSeat, String seats, String totalPay, String totalServiceTax,
                                             String totalBaseFare, String PickUpPoint, int PickUpID, String PickUpName, String PickUpTime, String seatNames,
                                             int totalSeaters, int totalSleepers, int totalSemiSleepers, String totalSeatersAmt, String totalSleepersAmt,
                                             String totalSemiSleeperAmt, String seatFares, String seatGenders, String seatServiceTax, String seatBaseFare
			, String seatDetails, String seat_details, int sel_Surcharges)
	{
		editPref.putInt(TOTAL_SEAT, totalSeat);
		editPref.putString(SEATS, seats);
		editPref.putString(TOTAL_PAY, totalPay);
		editPref.putString(TOTAL_SERVICE_TAX, totalServiceTax);
		editPref.putString(TOTAL_BASE_FARE, totalBaseFare);
		editPref.putString(PICKUP_POINT, PickUpPoint);
		editPref.putInt(PICKUP_ID, PickUpID);
		editPref.putString(PICKUP_NAME, PickUpName);
		editPref.putString(PICKUP_TIME, PickUpTime);
		editPref.putString(SEAT_NAMES, seatNames);
		editPref.putInt(TOTAL_SEATERS, totalSeaters);
		editPref.putInt(TOTAL_SLEEPERS, totalSleepers);
		editPref.putInt(TOTAL_SEMISLEEPERS, totalSemiSleepers);
		editPref.putString(TOTAL_SEATER_AMT, totalSeatersAmt);
		editPref.putString(TOTAL_SLEEPER_AMT, totalSleepersAmt);
		editPref.putString(TOTAL_SEMISLEEPER_AMT, totalSemiSleeperAmt);
		editPref.putString(SEAT_FARES, seatFares);
		editPref.putString(SEAT_GENDERS, seatGenders);
		editPref.putString(SEAT_SERVICETAX, seatServiceTax);
		editPref.putString(SEAT_BASEFARE, seatBaseFare);
		editPref.putString(SEATDETAILS, seatDetails);
		editPref.putString(SEAT_DETAILS, seat_details);
		editPref.putInt(SEAT_SURCHARGES, sel_Surcharges);
		editPref.commit();
	}
	
	public int gettotalSeat() 
	{
		return getPayData.getInt(TOTAL_SEAT, 0);
	}
	public String getseatDetails()
	{
		return getPayData.getString(SEATDETAILS, "");
	}
	public String getseat_details()
	{
		return getPayData.getString(SEAT_DETAILS, "");
	}
	public String getseats()
	{
		return getPayData.getString(SEATS, "");
	}
	public String gettotalPay()
	{
		return getPayData.getString(TOTAL_PAY, "");
	}
	public String gettotalServiceTax()
	{
		return getPayData.getString(TOTAL_SERVICE_TAX, "");
	}
	public String gettotalBaseFare()
	{
		return getPayData.getString(TOTAL_BASE_FARE, "");
	}
	public String getPickUpPoint()
	{
		return getPayData.getString(PICKUP_POINT, "");
	}
	public int getPickUpID() 
	{
		return getPayData.getInt(PICKUP_ID, 0);
	}
	public String getPickUpName()
	{
		return getPayData.getString(PICKUP_NAME, "");
	}
	public String getPickUpTime()
	{
		return getPayData.getString(PICKUP_TIME, "");
	}
	public String getseatNames()
	{
		return getPayData.getString(SEAT_NAMES, "");
	}
	public int gettotalSeaters() 
	{
		return getPayData.getInt(TOTAL_SEATERS, 0);
	}
	public int gettotalSleepers() 
	{
		return getPayData.getInt(TOTAL_SLEEPERS, 0);
	}
	public int gettotalSemiSleepers() 
	{
		return getPayData.getInt(TOTAL_SEMISLEEPERS, 0);
	}
	public String gettotalSeatersAmt()
	{
		return getPayData.getString(TOTAL_SEATER_AMT, "");
	}
	public String gettotalSleepersAmt()
	{
		return getPayData.getString(TOTAL_SLEEPER_AMT, "");
	}
	public String gettotalSemiSleeperAmt()
	{
		return getPayData.getString(TOTAL_SEMISLEEPER_AMT, "");
	}
	public String getseatFares()
	{
		return getPayData.getString(SEAT_FARES, "");
	}
	public String getseatGenders()
	{
		return getPayData.getString(SEAT_GENDERS, "");
	}
	public String getSeatServiceTax() {
		return getPayData.getString(SEAT_SERVICETAX, "");
	}
	
	public String getSeatBaseFare() {
		return getPayData.getString(SEAT_BASEFARE, "");
	}
	public int getSurcharges() 
	{
		return getPayData.getInt(SEAT_SURCHARGES, 0);
	}
}