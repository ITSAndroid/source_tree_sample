package com.infinityinfoway.aictsl.database;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;

public class ReturnJourneyDataPref 
{
	Context context;
	SharedPreferences getPayData;
	Editor editPref;
	

	public static String RETURN_COMPANY_ID = "RETURN_COMPANY_ID";
	public static String RETURN_COMPANY_NAME = "RETURN_COMPANY_NAME";
	public static String RETURN_FROM_CITY_ID = "RETURN_FROM_CITY_ID";
	public static String RETURN_FROM_CITY_NAME = "RETURN_FROM_CITY_NAME";
	public static String RETURN_TO_CITY_ID = "RETURN_TO_CITY_ID";
	public static String RETURN_TO_CITY_NAME = "RETURN_TO_CITY_NAME";
	public static String RETURN_ROUTE_ID = "RETURN_ROUTE_ID";
	public static String RETURN_ROUTE_TIME_ID = "RETURN_ROUTE_TIME_ID";
	public static String RETURN_CITY_TIME = "RETURN_CITY_TIME";
	public static String RETURN_ARRAIVAL_TIME = "RETURN_ARRAIVAL_TIME";
	public static String RETURN_BUS_TYPE = "RETURN_BUS_TYPE";
	public static String RETURN_BUS_TYPE_NAME = "RETURN_BUS_TYPE_NAME";
	public static String RETURN_BOOKING_DATE = "RETURN_BOOKING_DATE";
	public static String RETURN_ARRANGEMENT_ID = "RETURN_ARRANGEMENT_ID";
	public static String RETURN_ARRANGEMENT_NAME = "RETURN_ARRANGEMENT_NAME";
	public static String RETURN_AC_SEAT_RATE = "RETURN_AC_SEAT_RATE";
	public static String RETURN_AC_SLEEPER_RATE = "RETURN_AC_SLEEPER_RATE";
	public static String RETURN_AC_SLUMBER_RATE = "RETURN_AC_SLUMBER_RATE";
	public static String RETURN_NONAC_SEAT_RATE = "RETURN_NONAC_SEAT_RATE";
	public static String RETURN_NONAC_SLEEPER_RATE = "RETURN_NONAC_SLEEPER_RATE";
	public static String RETURN_NONAC_SLUMBER_RATE = "RETURN_NONAC_SLUMBER_RATE";
	public static String RETURN_BOARDING_POINTS = "RETURN_BOARDING_POINTS";
	public static String RETURN_DROPPING_POINTS = "RETURN_DROPPING_POINTS";
	public static String RETURN_EMPTY_SEATS = "RETURN_EMPTY_SEATS";
	public static String RETURN_REFERENCE_NUMBER = "RETURN_REFERENCE_NUMBER";
	public static String RETURN_AC_SEAT_SERVICE_TAX = "RETURN_AC_SEAT_SERVICE_TAX";
	public static String RETURN_AC_SLP_SERVICE_TAX = "RETURN_AC_SLP_SERVICE_TAX";
	public static String RETURN_AC_SLMB_SERVICE_TAX = "RETURN_AC_SLMB_SERVICE_TAX";
	public static String RETURN_NONAC_SEAT_SERVICE_TAX = "RETURN_NONAC_SEAT_SERVICE_TAX";
	public static String RETURN_NONAC_SLP_SERVICE_TAX = "RETURN_NONAC_SLP_SERVICE_TAX";
	public static String RETURN_NONAC_SLMB_SERVICE_TAX = "RETURN_NONAC_SLMB_SERVICE_TAX";
	public static String RETURN_BASE_AC_SEAT = "RETURN_BASE_AC_SEAT";
	public static String RETURN_BASE_AC_SLP = "RETURN_BASE_AC_SLP";
	public static String RETURN_BASE_AC_SLMB = "RETURN_BASE_AC_SLMB";
	public static String RETURN_BASE_NONAC_SEAT = "RETURN_BASE_NONAC_SEAT";
	public static String RETURN_BASE_NONAC_SLP = "RETURN_BASE_NONAC_SLP";
	public static String RETURN_BASE_NONAC_SLMB = "RETURN_BASE_NONAC_SLMB";
	public static String RETURN_BUSSEATTYPE = "RETURN_BUSSEATTYPE";
	public static String RETURN_SERVICETAX = "RETURN_SERVICETAX";
	public static String RETURN_SERVICETAXROUNDUP = "RETURN_SERVICETAXROUNDUP";
	public static String RETURN_ISINCLUDETAX = "RETURN_ISINCLUDETAX";
	
	private static String RETURN_TOTAL_SEAT = "RETURN_TOTAL_SEAT";
	private static String RETURN_SEATS = "RETURN_SEATS";
	private static String RETURN_TOTAL_PAY = "RETURN_TOTAL_PAY";
	private static String RETURN_TOTAL_SERVICE_TAX = "RETURN_TOTAL_SERVICE_TAX";
	private static String RETURN_TOTAL_BASE_FARE = "RETURN_TOTAL_BASE_FARE";
	private static String RETURN_PICKUP_POINT = "RETURN_PICKUP_POINT";
	private static String RETURN_PICKUP_ID = "RETURN_PICKUP_ID";
	private static String RETURN_PICKUP_NAME = "RETURN_PICKUP_NAME";
	private static String RETURN_PICKUP_TIME = "RETURN_PICKUP_TIME";
	private static String RETURN_SEAT_NAMES = "RETURN_SEAT_NAMES";
	private static String RETURN_TOTAL_SEATERS = "RETURN_TOTAL_SEATERS";
	private static String RETURN_TOTAL_SLEEPERS = "RETURN_TOTAL_SLEEPERS";
	private static String RETURN_TOTAL_SEMISLEEPERS = "RETURN_TOTAL_SEMISLEEPERS";
	private static String RETURN_TOTAL_SEATER_AMT = "RETURN_TOTAL_SEATER_AMT";
	private static String RETURN_TOTAL_SLEEPER_AMT = "RETURN_TOTAL_SLEEPER_AMT";
	private static String RETURN_TOTAL_SEMISLEEPER_AMT = "RETURN_TOTAL_SEMISLEEPER_AMT";
	private static String RETURN_SEAT_FARES = "RETURN_SEAT_FARES";
	private static String RETURN_SEAT_GENDERS = "RETURN_SEAT_GENDERS";
	private static String RETURN_SEAT_SERVICETAX = "RETURN_SEAT_SERVICETAX";

	private static String RETURN_SEAT_BASEFARE = "RETURN_SEAT_BASEFARE";
	private static String RETURN_SEATDETAILS = "RETURN_SEATDETAILS";
	private static String RETURN_SEAT_DETAILS = "RETURN_SEAT_DETAILS";
	private static String RETURN_SEAT_SURCHARGES = "RETURN_SEAT_SURCHARGES";


	private static String BUSID = "BUSID";
	private static String ROUTENAME = "ROUTENAME";
	private static String ROUTETIME = "ROUTETIME";
	private static String BUSNO = "BUSNO";
public ReturnJourneyDataPref(Activity mContext)
	{
		context = mContext;
		getPayData = PreferenceManager.getDefaultSharedPreferences(context);
		editPref = getPayData.edit();
	}
		
//-----------------------------------------------------------------------------------------------
	
	public void saveReturnJourneyBusDetails(String RouteName
			, String RouteTime,int BusID,String BusNo)
	{
		editPref.putInt(BUSID, BusID);
		editPref.putString(ROUTENAME, RouteName);
		editPref.putString(ROUTETIME, RouteTime);
		editPref.putString(BUSNO, BusNo);
		editPref.commit();
	}
	public String getRouteName()
	{
		return getPayData.getString(ROUTENAME, "");
	}
	public String getRouteTime()
	{
		return getPayData.getString(ROUTETIME, "");
	}
	public String getBusNo()
	{
		return getPayData.getString(BUSNO, "");
	}

	public int getBusID(){
		return getPayData.getInt(BUSID, 0);
	}



	public int getBusSeatType(){
		return getPayData.getInt(RETURN_BUSSEATTYPE, 0);
	}
	public String getServiceTax()
	{
		return getPayData.getString(RETURN_SERVICETAX, "");
	}
	public String getServiceTaxRoundUp()
	{
		return getPayData.getString(RETURN_SERVICETAXROUNDUP, "");
	}
	public int getCompanyID() 
	{
		return getPayData.getInt(RETURN_COMPANY_ID, 0);
	}
	public String getCompanyName()
	{
		return getPayData.getString(RETURN_COMPANY_NAME, "");
	}
	public int getFromCityId() 
	{
		return getPayData.getInt(RETURN_FROM_CITY_ID, 0);
	}
	public String getFromCityName()
	{
		return getPayData.getString(RETURN_FROM_CITY_NAME, "");
	}
	public int getToCityID() 
	{
		return getPayData.getInt(RETURN_TO_CITY_ID, 0);
	}
	public String getToCityName()
	{
		return getPayData.getString(RETURN_TO_CITY_NAME, "");
	}
	public int getRouteID() 
	{
		return getPayData.getInt(RETURN_ROUTE_ID, 0);
	}
	public int getRouteTimeID() 
	{
		return getPayData.getInt(RETURN_ROUTE_TIME_ID, 0);
	}
	public String getCityTime()
	{
		return getPayData.getString(RETURN_CITY_TIME, "");
	}
	public String getArrivalTime()
	{
		return getPayData.getString(RETURN_ARRAIVAL_TIME, "");
	}
	public int getBusType() 
	{
		return getPayData.getInt(RETURN_BUS_TYPE, 0);
	}
	public String getBusTypeName()
	{
		return getPayData.getString(RETURN_BUS_TYPE_NAME, "");
	}
	public String getBookingDate()
	{
		return getPayData.getString(RETURN_BOOKING_DATE, "");
	}
	public int getArrangementID() 
	{
		return getPayData.getInt(RETURN_ARRANGEMENT_ID, 0);
	}
	public String getArrangementName()
	{
		return getPayData.getString(RETURN_ARRANGEMENT_NAME, "");
	}
	public String getAcSeatRate()
	{
		return getPayData.getString(RETURN_AC_SEAT_RATE, "");
	}
	public String getAcSleeperRate()
	{
		return getPayData.getString(RETURN_AC_SLEEPER_RATE, "");
	}
	public String getAcSlumberRate()
	{
		return getPayData.getString(RETURN_AC_SLUMBER_RATE, "");
	}
	public String getNonAcSeatRate()
	{
		return getPayData.getString(RETURN_NONAC_SEAT_RATE, "");
	}
	public String getNonAcSleeperRate()
	{
		return getPayData.getString(RETURN_NONAC_SLEEPER_RATE, "");
	}
	public String getNonAcSlumberRate()
	{
		return getPayData.getString(RETURN_NONAC_SLUMBER_RATE, "");
	}
	public String getBoardingPoints()
	{
		return getPayData.getString(RETURN_BOARDING_POINTS, "");
	}
	public String getDroppingPoints()
	{
		return getPayData.getString(RETURN_DROPPING_POINTS, "");
	}
	public int getEmptySeats() 
	{
		return getPayData.getInt(RETURN_EMPTY_SEATS, 0);
	}
	public int getOM_ACFareIncludeTax() 
	{
		return getPayData.getInt("OM_ACFareIncludeTax", 0);
	}
	public String getReferenceNumber()
	{
		return getPayData.getString(RETURN_REFERENCE_NUMBER, "");
	}
	public String getAcSeatServiceTax()
	{
		return getPayData.getString(RETURN_AC_SEAT_SERVICE_TAX, "");
	}
	public String getAcSlpServiceTax()
	{
		return getPayData.getString(RETURN_AC_SLP_SERVICE_TAX, "");
	}
	public String getAcSlmbServiceTax()
	{
		return getPayData.getString(RETURN_AC_SLMB_SERVICE_TAX, "");
	}
	public String getNonAcSeatServiceTax()
	{
		return getPayData.getString(RETURN_NONAC_SEAT_SERVICE_TAX, "");
	}
	public String getNonAcSlpServiceTax()
	{
		return getPayData.getString(RETURN_NONAC_SLP_SERVICE_TAX, "");
	}
	public String getNonAcSlmbServiceTax()
	{
		return getPayData.getString(RETURN_NONAC_SLMB_SERVICE_TAX, "");
	}
	public String getBaseAcSeat()
	{
		return getPayData.getString(RETURN_BASE_AC_SEAT, "");
	}
	public String getBaseAcSlp()
	{
		return getPayData.getString(RETURN_BASE_AC_SLP, "");
	}
	public String getBaseAcSlmb()
	{
		return getPayData.getString(RETURN_BASE_AC_SLMB, "");
	}
	public String getBaseNonAcSeat()
	{
		return getPayData.getString(RETURN_BASE_NONAC_SEAT, "");
	}
	public String getBaseNonAcSlp()
	{
		return getPayData.getString(RETURN_BASE_NONAC_SLP, "");
	}
	public String getBaseNonAcSlmb()
	{
		return getPayData.getString(RETURN_BASE_NONAC_SLMB, "");
	}
	public int getIsServiceTaxInclude() 
	{
		return getPayData.getInt(RETURN_ISINCLUDETAX, 0);
	}
	
//-----------------------------------------------------------------------------------------------	
	
	public void saveReturnJourneySeatDetails(int totalSeat, String seats, String totalPay, String totalServiceTax,
                                             String totalBaseFare, String PickUpPoint, int PickUpID, String PickUpName, String PickUpTime, String seatNames,
                                             int totalSeaters, int totalSleepers, int totalSemiSleepers, String totalSeatersAmt, String totalSleepersAmt,
                                             String totalSemiSleeperAmt, String seatFares, String seatGenders, String seatServiceTax, String seatBaseFare
			, String seatDetails, String seat_details, int sel_Surcharges)
	{
		editPref.putInt(RETURN_TOTAL_SEAT, totalSeat);
		editPref.putString(RETURN_SEATS, seats);
		editPref.putString(RETURN_TOTAL_PAY, totalPay);
		editPref.putString(RETURN_TOTAL_SERVICE_TAX, totalServiceTax);
		editPref.putString(RETURN_TOTAL_BASE_FARE, totalBaseFare);
		editPref.putString(RETURN_PICKUP_POINT, PickUpPoint);
		editPref.putInt(RETURN_PICKUP_ID, PickUpID);
		editPref.putString(RETURN_PICKUP_NAME, PickUpName);
		editPref.putString(RETURN_PICKUP_TIME, PickUpTime);
		editPref.putString(RETURN_SEAT_NAMES, seatNames);
		editPref.putInt(RETURN_TOTAL_SEATERS, totalSeaters);
		editPref.putInt(RETURN_TOTAL_SLEEPERS, totalSleepers);
		editPref.putInt(RETURN_TOTAL_SEMISLEEPERS, totalSemiSleepers);
		editPref.putString(RETURN_TOTAL_SEATER_AMT, totalSeatersAmt);
		editPref.putString(RETURN_TOTAL_SLEEPER_AMT, totalSleepersAmt);
		editPref.putString(RETURN_TOTAL_SEMISLEEPER_AMT, totalSemiSleeperAmt);
		editPref.putString(RETURN_SEAT_FARES, seatFares);
		editPref.putString(RETURN_SEAT_GENDERS, seatGenders);
		editPref.putString(RETURN_SEAT_SERVICETAX, seatServiceTax);
		editPref.putString(RETURN_SEAT_BASEFARE, seatBaseFare);
		editPref.putString(RETURN_SEATDETAILS, seatDetails);
		editPref.putString(RETURN_SEAT_DETAILS, seat_details);
		editPref.putInt(RETURN_SEAT_SURCHARGES, sel_Surcharges);
		editPref.commit();
	}
	
	public String getseatDetails()
	{
		return getPayData.getString(RETURN_SEATDETAILS, "");
	}
	public String getseat_details()
	{
		return getPayData.getString(RETURN_SEAT_DETAILS, "");
	}
	public int gettotalSeat() 
	{
		return getPayData.getInt(RETURN_TOTAL_SEAT, 0);
	}
	public String getseats()
	{
		return getPayData.getString(RETURN_SEATS, "");
	}
	public String gettotalPay()
	{
		return getPayData.getString(RETURN_TOTAL_PAY, "");
	}
	public String gettotalServiceTax()
	{
		return getPayData.getString(RETURN_TOTAL_SERVICE_TAX, "");
	}
	public String gettotalBaseFare()
	{
		return getPayData.getString(RETURN_TOTAL_BASE_FARE, "");
	}
	public String getPickUpPoint()
	{
		return getPayData.getString(RETURN_PICKUP_POINT, "");
	}
	public int getPickUpID() 
	{
		return getPayData.getInt(RETURN_PICKUP_ID, 0);
	}
	public String getPickUpName()
	{
		return getPayData.getString(RETURN_PICKUP_NAME, "");
	}
	public String getPickUpTime()
	{
		return getPayData.getString(RETURN_PICKUP_TIME, "");
	}
	public String getseatNames()
	{
		return getPayData.getString(RETURN_SEAT_NAMES, "");
	}
	public int gettotalSeaters() 
	{
		return getPayData.getInt(RETURN_TOTAL_SEATERS, 0);
	}
	public int gettotalSleepers() 
	{
		return getPayData.getInt(RETURN_TOTAL_SLEEPERS, 0);
	}
	public int gettotalSemiSleepers() 
	{
		return getPayData.getInt(RETURN_TOTAL_SEMISLEEPERS, 0);
	}
	public String gettotalSeatersAmt()
	{
		return getPayData.getString(RETURN_TOTAL_SEATER_AMT, "");
	}
	public String gettotalSleepersAmt()
	{
		return getPayData.getString(RETURN_TOTAL_SLEEPER_AMT, "");
	}
	public String gettotalSemiSleeperAmt()
	{
		return getPayData.getString(RETURN_TOTAL_SEMISLEEPER_AMT, "");
	}
	public String getseatFares()
	{
		return getPayData.getString(RETURN_SEAT_FARES, "");
	}
	public String getseatGenders()
	{
		return getPayData.getString(RETURN_SEAT_GENDERS, "");
	}
	public String getSeatServiceTax() {
		return getPayData.getString(RETURN_SEAT_SERVICETAX, "");
	}
	
	public String getSeatBaseFare() {
		return getPayData.getString(RETURN_SEAT_BASEFARE, "");
	}
	public int getSurcharges() 
	{
		return getPayData.getInt(RETURN_SEAT_SURCHARGES, 0);
	}
}