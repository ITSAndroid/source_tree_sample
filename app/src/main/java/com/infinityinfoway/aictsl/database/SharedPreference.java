package com.infinityinfoway.aictsl.database;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;

public class SharedPreference {
    Context context;
    Boolean isGetCode;


    public SharedPreference(Activity mContext) {
        context = mContext;
    }

    public void setRegistrationCode(Boolean isGetCode) {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editPref = getReg.edit();
        editPref.putBoolean("IS_CODED", isGetCode);
        editPref.commit();
    }


    //------------------------------------------------------------------------------------------------
    public void saveApCouponCode(String code) {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editPref = getReg.edit();
        editPref.putString("APCOUPENCODE", code);
        editPref.commit();
    }

    public String getApCouponCode() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getString("APCOUPENCODE", "");
    }

    public void setWalletBalance(String walletBalance) {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editPref = getReg.edit();
        editPref.putString("WALLET_BALANCE", walletBalance);
        editPref.commit();
    }

    public boolean loadCoded() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getBoolean("IS_CODED", false);
    }

    public void savemodifydate(String modifydt) {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editPref = getReg.edit();
        editPref.putString("Modify_DATE", modifydt);
        editPref.commit();
    }

    public String getmodifydate() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getString("Modify_DATE", null);
    }

    public void setIsLogin(Boolean isLogin) {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editPref = getReg.edit();
        editPref.putBoolean("IS_LOGIN", isLogin);
        editPref.commit();
    }

    public boolean IsLogin() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getBoolean("IS_LOGIN", false);
    }

    public void setUserId(int UserId) {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editPref = getReg.edit();
        editPref.putInt("UserId", UserId);
        editPref.commit();
    }

    public int getUserId() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getInt("UserId", 0);
    }

    public void setUserPassword(String UserPassword) {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editPref = getReg.edit();
        editPref.putString("UserPassword", UserPassword);
        editPref.commit();
    }

    public String getUserPassword() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getString("UserPassword", "");
    }

    public void setIsReturnTrip(int isReturnTrip) {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editPref = getReg.edit();
        editPref.putInt("IS_RETURNTRIP", isReturnTrip);
        editPref.commit();
    }

    public int IsReturnTrip() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getInt("IS_RETURNTRIP", 0);
    }

    public void saveFromCityIDName(int fromCityID, String fromCityName) {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editPref = getReg.edit();
        editPref.putInt("FROM_CITY_ID", fromCityID);
        editPref.putString("FROM_CITY_NAME", fromCityName);
        editPref.commit();
    }

    public int getFromCityID() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getInt("FROM_CITY_ID", 0);
    }

    public String getFromCityName() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getString("FROM_CITY_NAME", "");
    }

    public void saveCompanyIDName(int companyID, String companyName) {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editPref = getReg.edit();
        editPref.putInt("COMPANY_ID", companyID);
        editPref.putString("COMPANY_NAME", companyName);
        editPref.commit();
    }

    //-----------------------------------------------------------------------------------------------
    public void saveToCityIDName(int toCityID, String toCityName) {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editPref = getReg.edit();
        editPref.putInt("TO_CITY_ID", toCityID);
        editPref.putString("TO_CITY_NAME", toCityName);
        editPref.commit();
    }

    public int getToCityID() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getInt("TO_CITY_ID", 0);
    }

    public String getToCityName() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getString("TO_CITY_NAME", "");
    }
//-----------------------------------------------------------------------------------------------

    public void saveOnwardJourneyDate(String onwardJourneyDate, String onwardJourneyDay) {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editPref = getReg.edit();
        editPref.putString("ONWARD_JOURNEY_DATE", onwardJourneyDate);
        editPref.putString("ONWARD_JOURNEY_DAY", onwardJourneyDay);
        editPref.commit();
    }

    public String getOnwardJourneyDate() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getString("ONWARD_JOURNEY_DATE", "");
    }

    public String getOnwardJourneyDay() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getString("ONWARD_JOURNEY_DAY", "");
    }

    //-----------------------------------------------------------------------------------------------
    public void saveReturnJourneyDate(String returnJourneyDate, String returnJourneyDay) {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editPref = getReg.edit();
        editPref.putString("RETURN_JOURNEY_DATE", returnJourneyDate);
        editPref.putString("RETURN_JOURNEY_DAY", returnJourneyDay);
        editPref.commit();
    }

    public String getReturnJourneyDate() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getString("RETURN_JOURNEY_DATE", "");
    }

    public String getReturnJourneyDay() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getString("RETURN_JOURNEY_DAY", "");
    }

    //-----------------------------------------------------------------------------------------------
    public int getOM_OperatorID() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getInt("OM_OperatorID", 0);
    }

    public String getRTX_ReferenceNumber() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getString("RTX_ReferanceNumber", "");
    }

    public String getOperaterfare() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getString("OperatorFare", "");
    }

    public void setVersionPref(int versionCode, int downloadType, int IsSurcharge) {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editPref = getReg.edit();
        editPref.putInt("PREF_versionCode", versionCode);
        editPref.putInt("PREF_downloadType", downloadType);
        editPref.putInt("IsSurcharge", IsSurcharge);
        editPref.commit();
    }

    public int getIsSurcharge() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getInt("IsSurcharge", 0);
    }

    public int getversionCode() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getInt("PREF_versionCode", 0);
    }

    public int getdownloadType() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getInt("PREF_downloadType", 0);
    }

    public boolean loadSavedPref() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getBoolean("IS_REG", false);
    }

    public String loadUserName() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getString("REG_NAME", "");
    }

    public String loadUserPhone() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getString("REG_PHONE", "");
    }

    public String loadUserEmail() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getString("REG_EMAIL", "");
    }

    public void savePref(boolean isRegistered, String name, String phoneNo, String email) {
        SharedPreferences setReg = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editPref = setReg.edit();
        editPref.putBoolean("IS_REG", isRegistered);
        editPref.putString("REG_NAME", name);
        editPref.putString("REG_PHONE", phoneNo);
        editPref.putString("REG_EMAIL", email);
        editPref.commit();
    }

    public void deletePref(boolean isRegistered, String name, String phoneNo, String email) {
        SharedPreferences setReg = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editPref = setReg.edit();
        editPref.putBoolean("IS_REG", isRegistered);
        editPref.putString("REG_NAME", name);
        editPref.putString("REG_PHONE", phoneNo);
        editPref.putString("REG_EMAIL", email);
        setReg.edit().clear().commit();
    }

    public void saveRecentSearch(int fromCityId, String fromCity, int toCityId, String toCity) {
        SharedPreferences setReg = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editPref = setReg.edit();
        editPref.putInt("FROM_CITY_ID", fromCityId);
        editPref.putString("FROM_CITY", fromCity);
        editPref.putInt("TO_CITY_ID", toCityId);
        editPref.putString("TO_CITY", toCity);
        editPref.commit();
    }

    public String loadRecentSearch() {
        SharedPreferences getRecent = PreferenceManager.getDefaultSharedPreferences(context);
        int int_fromCityId = getRecent.getInt("FROM_CITY_ID", 0);
        String str_fromCity = getRecent.getString("FROM_CITY", "---");
        int int_toCityId = getRecent.getInt("TO_CITY_ID", 0);
        String str_toCity = getRecent.getString("TO_CITY", "---");

        return str_fromCity + "," + str_toCity + "," + int_fromCityId + "," + int_toCityId;
    }

    public void setActivitiesPref(String HomeActivityPref, String BookingActivityPref, String MyBookingActivityPref,
                                  String CancellationActivityPref, String AvailableRoutesActivityPref, String PaymentGatewayActivityPref,
                                  String PayForPhoneBookingActivityPref, String PhoneRechargeActivityPref, String PhoneBookingActivityPref,
                                  String PayFromWalletActivity, String ReturnBooking) {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editPref = getReg.edit();
        editPref.putString("HomeActivity_PREF", HomeActivityPref);
        editPref.putString("BookingActivity_PREF", BookingActivityPref);
        editPref.putString("MyBookingActivity_PREF", MyBookingActivityPref);
        editPref.putString("CancellationActivity_PREF", CancellationActivityPref);
        editPref.putString("AvailableRoutesActivity_PREF", AvailableRoutesActivityPref);
        editPref.putString("PaymentGatewayActivity_PREF", PaymentGatewayActivityPref);
        editPref.putString("PayForPhoneBookingActivity_PREF", PayForPhoneBookingActivityPref);
        editPref.putString("PhoneRechargeActivity_PREF", PhoneRechargeActivityPref);
        editPref.putString("PhoneBookingActivity_PREF", PhoneBookingActivityPref);
        editPref.putString("PayFromWalletActivity_PREF", PayFromWalletActivity);
        editPref.putString("ReturnBooking_PREF", ReturnBooking);
        editPref.commit();
    }

    public String getActivitiesPref() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        String[] str_HomeActivity = getReg.getString("HomeActivity_PREF", "1:1").split(":");
        String[] str_BookingActivity = getReg.getString("BookingActivity_PREF", "2:1").split(":");
        String[] str_MyBookingActivity = getReg.getString("MyBookingActivity_PREF", "3:1").split(":");
        String[] str_CancellationActivity = getReg.getString("CancellationActivity_PREF", "4:1").split(":");
        String[] str_AvailableRoutesActivity = getReg.getString("AvailableRoutesActivity_PREF", "5:1").split(":");
        String[] str_PaymentGatewayActivity = getReg.getString("PaymentGatewayActivity_PREF", "6:1").split(":");
        String[] str_PayForPhoneBookingActivity = getReg.getString("PayForPhoneBookingActivity_PREF", "7:1").split(":");
        String[] str_PhoneRechargeActivity = getReg.getString("PhoneRechargeActivity_PREF", "8:1").split(":");
        String[] str_PhoneBookingActivity = getReg.getString("PhoneBookingActivity_PREF", "9:1").split(":");
        String[] str_PayFromWalletActivity = getReg.getString("PayFromWalletActivity_PREF", "10:1").split(":");
        String[] str_ReturnBooking = getReg.getString("ReturnBooking_PREF", "11:1").split(":");

        return str_HomeActivity[1] + "," + str_BookingActivity[1] + "," + str_MyBookingActivity[1] + "," + str_CancellationActivity[1]
                + "," + str_AvailableRoutesActivity[1] + "," + str_PaymentGatewayActivity[1] + "," +
                str_PayForPhoneBookingActivity[1] + "," + str_PhoneRechargeActivity[1] + "," + str_PhoneBookingActivity[1] +
                "," + str_PayFromWalletActivity[1] + "," + str_ReturnBooking[1];
    }

    public void savePrefdata(boolean b, String name, String phoneNo,
                             String email) {
        // TODO Auto-generated method stub
        SharedPreferences setReg = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editPref = setReg.edit();
        editPref.putBoolean("IS_PAS", b);
        editPref.putString("PAS_NAME", name);
        editPref.putString("PAS_PHONE", phoneNo);
        editPref.putString("PAS_EMAIL", email);
        editPref.commit();
    }

    public String passengerName() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getString("PAS_NAME", "");
    }

    public String passengerPhone() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getString("PAS_PHONE", "");
    }

    public String passengerEmail() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getString("PAS_EMAIL", "");
    }

    public boolean is_Passanger() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getBoolean("IS_PAS", false);

    }

    public void saveVersionInfo(int versionCode, String versionName) {
        // TODO Auto-generated method stub
        SharedPreferences setReg = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editPref = setReg.edit();
        editPref.putInt("VERSIONCODE", versionCode);
        editPref.putString("VERSIONNAME", versionName);
        editPref.commit();
    }

    public String getVersionName() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getString("VERSIONNAME", "");

    }

    public int getVersionCode() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getInt("VERSIONCODE", 0);

    }

    public void saveBusID(int busID) {
        SharedPreferences setReg = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editPref = setReg.edit();
        editPref.putInt("busID", busID);
        editPref.commit();
    }
    public int getbusID() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getInt("busID", 0);

    }

    public void saveBusID1(int busID) {
        SharedPreferences setReg = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editPref = setReg.edit();
        editPref.putInt("busID1", busID);
        editPref.commit();
    }
    public int getbusID1() {
        SharedPreferences getReg = PreferenceManager.getDefaultSharedPreferences(context);
        return getReg.getInt("busID1", 0);

    }

}