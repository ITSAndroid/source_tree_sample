package com.infinityinfoway.aictsl.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;

import com.infinityinfoway.aictsl.bean.BannerInfoBean;
import com.infinityinfoway.aictsl.bean.CityBean;
import com.infinityinfoway.aictsl.bean.ContactCityName;
import com.infinityinfoway.aictsl.bean.ContactInfo;
import com.infinityinfoway.aictsl.bean.RecentSearchBean;
import com.infinityinfoway.aictsl.config.Config;

import java.util.ArrayList;
import java.util.List;


public class DBConnector extends SQLiteOpenHelper {
	private static final int DATABASE_VERSION = Config.db_version;
	private static final String DATABASE_NAME = Config.db_name;
	private static final String TBL_SOURCE_CITIES = "SourceCityMaster";

	//private static final String SCM_ID = "scm_ID";
	private static final String SCM_CITY_NAME = "scm_CityName";
	private static final String SCM_CITY_ID = "scm_CityID";
	
	private static final String TBL_BANNER_INFO = "BannerInfo";
	
	private static final String IM_ID = "IM_ID";
	private static final String IM_Type = "IM_Type";
	private static final String IM_Message = "IM_Message";
	private static final String IM_FromDate = "IM_FromDate";
	private static final String IM_ToDate = "IM_ToDate";
	private static final String IM_Duration = "IM_Duration";
	
	public static final String createBannerInfo = "CREATE TABLE "
			+ TBL_BANNER_INFO + " ( " + IM_ID + " INTEGER PRIMARY KEY,"
			+ IM_Type + " TEXT NOT NULL, " + IM_Message + " text not null , "
			+ IM_FromDate + " text not null , " + IM_ToDate + " text not null,"
			+ IM_Duration + " text not null" + ");";
	


	private static final String TBL_RECENT_SEARCHBUS = "RecentSearchBus";
	private static final String ID = "ID";
	private static final String FROM_ID = "FROM_ID";
	private static final String TO_ID = "TO_ID";
	private static final String FROM_CITY = "FROM_CITY";
	private static final String TO_CITY = "TO_CITY";
	private static final String DATE = "DATE";
	
	private static final String TBL_CONTACT_INFO = "ContactUsInfo";
	private static final String C_ORDERID = "OrderBy";
	private static final String C_ID = "ID";
	private static final String C_CITYNAME = "CityName";
	private static final String C_CITYAREA = "CityAreaName";
	private static final String C_ADDRESS = "Address";
	private static final String C_PHONE = "ContactNumber";
	private static final String C_EMAIL = "EmailID";
	private static final String C_ISACTIVE = "IsActive";
	private static final String C_CREATE_DATE = "CreateDate";
	private static final String C_MODIFY_DATE = "ModifyDate";
	private static final String C_STATUS = "Status";
	private static final String C_STATE_NAME = "StateName";
	
	private static final String TBL_CONTACT_CITY_INFO = "ContactUscity";
	private static final String CITY_ID = "City_ID";
	private static final String CITY_NAME = "City_Name";
	
	

	String createCityMaster = "CREATE TABLE  "
			+ TBL_SOURCE_CITIES + "("
			+ SCM_CITY_ID + " INTEGER PRIMARY KEY ,"
			+ SCM_CITY_NAME + " TEXT)";
	
	 String createRecentSearchMaster = "CREATE TABLE IF NOT EXISTS "
				+ TBL_RECENT_SEARCHBUS + " ( " + ID + " INTEGER PRIMARY KEY,"
				+ FROM_ID + " TEXT NOT NULL, " + TO_ID + " text not null , "
				+ FROM_CITY + " text not null , " + TO_CITY + " text not null,"
				+ DATE + " text not null" + ");";
	 
	 String createContactInfo = "CREATE TABLE " + TBL_CONTACT_INFO + " ( "
				+  C_ID + " INTEGER PRIMARY KEY," + C_CITYNAME
				+ " TEXT , " + C_CITYAREA + " TEXT ,"
				+ C_ADDRESS + " TEXT ," + C_STATE_NAME
				+ " TEXT ," + C_PHONE + " TEXT ," + C_EMAIL
				+ " TEXT , " + C_ISACTIVE + " TEXT ," + C_CREATE_DATE
				+ " TEXT ," + C_MODIFY_DATE + " TEXT ," + C_STATUS + " TEXT,"
				+  C_ORDERID + " TEXT ) ";
	 
	 String cityinfo = "CREATE TABLE " + TBL_CONTACT_CITY_INFO + " ( "
				+ CITY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + CITY_NAME
				+ " TEXT NOT NULL ) ";
	 
	
	
	// =====================================================================================
	// Database Class constructor
	// =====================================================================================
	public DBConnector(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	// =====================================================================================
	// On Create Method
	// =====================================================================================
	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(createCityMaster);
		db.execSQL(createRecentSearchMaster);
		db.execSQL(createContactInfo);
		db.execSQL(cityinfo);
		db.execSQL(createBannerInfo);
	}

	// =====================================================================================
	// On Upgrade Method
	// =====================================================================================
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS " + TBL_SOURCE_CITIES);
		db.execSQL("DROP TABLE IF EXISTS " + TBL_CONTACT_INFO);
		db.execSQL("DROP TABLE IF EXISTS " + TBL_RECENT_SEARCHBUS);
		db.execSQL("DROP TABLE IF EXISTS " + TBL_CONTACT_CITY_INFO);
		db.execSQL("DROP TABLE IF EXISTS " + TBL_BANNER_INFO);
		onCreate(db);
	}

	// =====================================================================================
	// Insert Into Source City Master Method
	// =====================================================================================
	
	public void addAllSourceCity(List<CityBean> listCity)
	{
		    try{
		    	String sql = "INSERT OR REPLACE INTO "
						+ TBL_SOURCE_CITIES
						+ " (scm_CityID,scm_CityName) VALUES (?,?)";
		    	SQLiteDatabase db = this.getWritableDatabase();

				db.beginTransaction();

				SQLiteStatement stmt = db.compileStatement(sql);
				
				for (int i = 0; i < listCity.size(); i++) {
					CityBean data=listCity.get(i);

					if (data.SCM_CITY_ID > 0) {
						
						stmt.bindLong(1,data.SCM_CITY_ID); // id
						if (data.SCM_CITY_NAME != null
								&& data.SCM_CITY_NAME.length() > 0) {
							stmt.bindString(2, data.SCM_CITY_NAME);
						} else {
							stmt.bindString(2, " ");
						}
					}
					stmt.execute();
					stmt.clearBindings();
				}
				db.setTransactionSuccessful();
				db.endTransaction();
				db.close();
		    }catch(Exception ex){ex.printStackTrace();}
		    
	}


	// =====================================================================================
	// Get All City List
	// =====================================================================================
	public String[] cities;

	public String[] getAllSourceCity() {
		String companySelect = "SELECT * FROM " + TBL_SOURCE_CITIES;
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(companySelect, null);
		cities = new String[cursor.getCount()];
		int i = 0;
		if (cursor.moveToFirst()) {
			do {
				cities[i] = cursor.getString(1).toString();
				i++;
			} while (cursor.moveToNext());
		}
		db.close();
		return cities;
	}

	// =====================================================================================
	// Get CityID Based on CityName
	// =====================================================================================
	public int GetSourceCityID(String sourceCityName) {
		String citySelect = "SELECT " + SCM_CITY_ID + " FROM "
				+ TBL_SOURCE_CITIES + " WHERE " + SCM_CITY_NAME + " = \""
				+ sourceCityName + "\"";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(citySelect, null);
		int cityid = 0;
		if (cursor.moveToFirst()) {
			do {
				cityid = cursor.getInt(0);
			} while (cursor.moveToNext());
		}
		db.close();
		return cityid;
	}
	// =====================================================================================
	// Delete all cities
	// =====================================================================================
	public void deleteAllSourceCity() {
		SQLiteDatabase db = this.getWritableDatabase();
		db.execSQL("Delete From " + TBL_SOURCE_CITIES);
		db.close();
	}
	// =====================================================================================
	// Insert Into Source City Master Method
	// =====================================================================================
	public int addRecentSearch(RecentSearchBean data) {

		int count = 0;
		SQLiteDatabase db = this.getWritableDatabase();
		try {
			Cursor cursor = db.query(TBL_RECENT_SEARCHBUS, new String[] { ID,
					FROM_ID, TO_ID, FROM_CITY, TO_CITY, DATE }, FROM_ID + "=?"
					+ " and " + TO_ID + "=?", new String[] { data.FROM_ID,
					data.TO_ID }, null, null, null);


			if (cursor.getCount() > 0) {
				cursor.moveToFirst();

				db.delete(TBL_RECENT_SEARCHBUS, ID + "=?",
						new String[] { cursor.getString(0) });
                ContentValues values = new ContentValues();
                values.put(FROM_ID, data.FROM_ID);
				values.put(TO_ID, data.TO_ID);
				values.put(FROM_CITY, data.FROM_CITY);
				values.put(TO_CITY, data.TO_CITY);
				values.put(DATE, data.DATE);
				db.insert(TBL_RECENT_SEARCHBUS, null, values);
				count++;
			} else {
				ContentValues values = new ContentValues();
				values.put(FROM_ID, data.FROM_ID);
				values.put(TO_ID, data.TO_ID);
				values.put(FROM_CITY, data.FROM_CITY);
				values.put(TO_CITY, data.TO_CITY);
				values.put(DATE, data.DATE);
				db.insert(TBL_RECENT_SEARCHBUS, null, values);
				count++;
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		db.close();
		return count;
	}

	// =====================================================================================
	// GET LIST FOR RECENT SEARCH BUS
	// =====================================================================================

	public List<RecentSearchBean> getRecentSearch() {
		SQLiteDatabase db = this.getWritableDatabase();
		List<RecentSearchBean> list = new ArrayList<RecentSearchBean>();
		Cursor cursor = db.rawQuery("SELECT * FROM " + TBL_RECENT_SEARCHBUS
				+ " ORDER BY ID DESC LIMIT 10 ", null);
		if (cursor.getCount() > 0) {
			cursor.moveToFirst();
			while (!cursor.isAfterLast()) {
				RecentSearchBean data = new RecentSearchBean();
				data.ID = cursor.getString(0);
				data.FROM_ID = cursor.getString(1);
				data.TO_ID = cursor.getString(2);
				data.FROM_CITY = cursor.getString(3);
				data.TO_CITY = cursor.getString(4);
				data.DATE = cursor.getString(5);
				list.add(data);
				cursor.moveToNext();
			}
		}
		db.close();
		return list;
	}

	// =====================================================================================
	// DELETE RECORD FROM RECENT SEARCH
	// =====================================================================================

	public int deleteRecentSearch() {
		int count = 0;
		SQLiteDatabase db = this.getWritableDatabase();
		try {
			Cursor cursor = db.rawQuery(
					"SELECT * FROM " + TBL_RECENT_SEARCHBUS, null);
			if (cursor.getCount() > 10) {
				Cursor cur = db.rawQuery(" SELECT " + " MIN(" + ID + ") FROM "
						+ TBL_RECENT_SEARCHBUS, null);
				if (cur.getCount() > 0) {
					cur.moveToFirst();
					db.delete(TBL_RECENT_SEARCHBUS, "ID=?",
							new String[] { cur.getString(0) });
					count++;
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		db.close();
		return count;
	}

	public int deleteAllRecentSearch() {
		int count = 0;
		try {
			SQLiteDatabase db = this.getWritableDatabase();
			db.execSQL("Delete From " + TBL_RECENT_SEARCHBUS);
			db.close();
			count++;
		} catch (Exception ex) {ex.printStackTrace();}
		return count;
	}
	
//=====================================================================================
// 					ContactUS All Details 
//=====================================================================================


		public boolean insertContact(List<ContactInfo> list) {

			SQLiteDatabase db = this.getWritableDatabase();
			try {
				
				String contactinsert = "INSERT OR REPLACE INTO "
						+ TBL_CONTACT_INFO
						+ " (ID,CityName,CityAreaName,Address,StateName,ContactNumber,EmailID,IsActive,CreateDate,ModifyDate,Status,OrderBy) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
				db.beginTransaction();

				SQLiteStatement stmt = db.compileStatement(contactinsert);

				for (int i = 0; i < list.size(); i++) {
					ContactInfo data = new ContactInfo();
					data = list.get(i);
					//Log.e("Insert Record List  table (dbconect)", data+"");
					if (data.C_ID != null && data.C_ID.length() > 0
							&& data.C_ISACTIVE.equals("1")) {
						if (data.C_ID != null && data.C_ID.length() > 0) {
							stmt.bindLong(1, Integer.parseInt(data.C_ID));
						//	Log.e("data.C_ID", ""+data.C_ID);
						} else {
						}
						if (data.C_CITYNAME != null && data.C_CITYNAME.length() > 0) {
							stmt.bindString(2, data.C_CITYNAME);

						} else {
							stmt.bindString(2, " ");
						}
						if (data.C_CITYAREA != null && data.C_CITYAREA.length() > 0) {
							stmt.bindString(3, data.C_CITYAREA);
						} else {
							stmt.bindString(3, " ");
						}
						if (data.C_ADDRESS != null && data.C_ADDRESS.length() > 0) {
							stmt.bindString(4, data.C_ADDRESS);
						} else {
							stmt.bindString(4, " ");
						}

						if (data.C_STATE_NAME != null
								&& data.C_STATE_NAME.length() > 0) {
							stmt.bindString(5, data.C_STATE_NAME);
						} else {
							stmt.bindString(5, " ");
						}
						if (data.C_PHONE != null && data.C_PHONE.length() > 0) {
							stmt.bindString(6, data.C_PHONE);
						} else {
							stmt.bindString(6, " ");
						}
						if (data.C_EMAIL != null && data.C_EMAIL.length() > 0) {
							stmt.bindString(7, data.C_EMAIL);
						} else {
							stmt.bindString(7, " ");
						}

						if (data.C_ISACTIVE != null && data.C_ISACTIVE.length() > 0) {
							stmt.bindString(8, data.C_ISACTIVE);
						} else {
							stmt.bindString(8, " ");
						}
						if (data.C_CREATE_DATE != null
								&& data.C_CREATE_DATE.length() > 0) {
							stmt.bindString(9, data.C_CREATE_DATE);
						} else {
							stmt.bindString(9, " ");
						}
						if (data.C_MODIFY_DATE != null
								&& data.C_MODIFY_DATE.length() > 0) {
							stmt.bindString(10, data.C_MODIFY_DATE);
						} else {
							stmt.bindString(10, " ");
						}
						if (data.C_STATUS != null
								&& data.C_STATUS.length() > 0) {
							stmt.bindString(11, data.C_STATUS);
						} else {
							stmt.bindString(11, " ");
						}
						if (data.C_ORDERID != null
								&& data.C_ORDERID.length() > 0) {
							stmt.bindString(12, data.C_ORDERID);
							//Log.e("data.C_ORDERID ", ""+data.C_ORDERID );
						} else {
							stmt.bindString(12, " ");
						}
						stmt.execute();
						stmt.clearBindings();
					}
				}

				db.setTransactionSuccessful();
				db.endTransaction();

				db.close();
				return true;
			} catch (Exception ex) {

			//	Log.e("DBHelper", "Error in database data inserted");
				ex.printStackTrace();
				db.close();
				return false;
			}
		}
	
		public List<ContactInfo> getContactInfoList() {
			SQLiteDatabase db = this.getWritableDatabase();
			List<ContactInfo> list = new ArrayList<ContactInfo>();
			// SimpleDateFormat formatter = new
			// SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");

			try {
				Cursor cur = db.rawQuery("SELECT * FROM ContactUsInfo ", null);

				if (cur.getCount() > 0) {
					cur.moveToFirst();
					while (!cur.isAfterLast()) {
						ContactInfo data = new ContactInfo();

						data.C_ID = cur.getString(0);
						data.C_CITYNAME = cur.getString(1);
						data.C_CITYAREA = cur.getString(2);
						data.C_ADDRESS = cur.getString(3);
						data.C_STATE_NAME = cur.getString(4);
						data.C_PHONE = cur.getString(5);
						data.C_EMAIL = cur.getString(6);
						data.C_STATUS = cur.getString(7);
						data.C_ORDERID = cur.getString(8);
						//Log.e("List C_STATUS", ""+data.C_STATUS);
					if(data.C_STATUS != null && data.C_STATUS.length() > 0)
					{
						if(data.C_STATUS.equalsIgnoreCase("1"))
						{
							list.add(data);
						}
					}
						cur.moveToNext();
					}
				}

			} catch (Exception ex) {
				ex.printStackTrace();
			}
			db.close();
			return list;
		}

		// ********** for contactUs city name

		public void InsertcityName() {

			SQLiteDatabase db = this.getWritableDatabase();
			Cursor c = db.rawQuery("SELECT CityName FROM ContactUsInfo", null);

			if (c != null) {
				if (c.moveToFirst()) {
					do {
						// Log.d(" insert city", "" +
						// c.getString(c.getColumnIndex("CityName")));
						String inct = c.getString(c.getColumnIndex("CityName"));

						ContentValues cvcity = new ContentValues();
						cvcity.put("City_Name", inct);
						String selectcity = getSinlgeEntry(inct);
						// Log.d(" select ct", selectcity);
						if (selectcity != null && selectcity.length() > 0) {
							if (selectcity.equals(inct)) {
								// Log.d(" select ct if", selectcity);
							}
						} else {
							db.insert("ContactUscity", null, cvcity);
						}
					} while (c.moveToNext());
				}
				db.close();
			}
		}

		public String getSinlgeEntry(String ctselect) {
			SQLiteDatabase db = this.getWritableDatabase();
			Cursor cursor = db.rawQuery(
					"SELECT * FROM ContactUscity  WHERE City_Name=? ",
					new String[] { ctselect });
			if (cursor.getCount() < 1) // UserName Not Exist
			{
				cursor.close();
				return null;
			} else {
				cursor.moveToFirst();
				String city = cursor.getString(cursor.getColumnIndex("City_Name"));
				cursor.close();
				return city;
			}
		}

		public Cursor getAllCity() {
			SQLiteDatabase db = this.getWritableDatabase();
			return db.query("ContactUscity", new String[] { "City_Name" }, null,
					null, null, null, null);
		}

		public void clearTabledata() {
			SQLiteDatabase db = this.getWritableDatabase();
			// String sql = "drop table ContactUsInfo";
			try {
				db.execSQL("DELETE FROM ContactUsInfo");
				db.close();
			} catch (SQLException e) {e.printStackTrace();
			}

		}
		
		public String modifydatesaveinsharepref(String date) {
			SQLiteDatabase db = this.getWritableDatabase();
			Cursor cursor = db.rawQuery(
					"SELECT ModifyDate FROM ContactUsInfo  WHERE CityName=?",
					new String[] { date });

			if (cursor.getCount() < 1) // UserName Not Exist
			{
				cursor.close();
				return null;
			} else {
				cursor.moveToFirst();

				String dt = cursor.getString(cursor.getColumnIndex("ModifyDate"));
				cursor.close();
				return dt;
			}
		}

		public List<ContactCityName> getContactCityList() {
			SQLiteDatabase db = this.getWritableDatabase();
			List<ContactCityName> list = new ArrayList<ContactCityName>();

			try {

				Cursor cur = db.rawQuery("SELECT City_Name FROM  ContactUscity",
						null);

				if (cur.getCount() > 0) {
					for (int i = 0; i <= cur.getCount(); i++) {
						ContactCityName data = new ContactCityName();
						data.CITY_NAME = cur.getString(cur
								.getColumnIndex("City_Name"));

						list.add(data);
					}
				} else {
					list = null;
				}

			} catch (Exception ex) {
				ex.printStackTrace();
			}
			db.close();
			return list;
		}
		
//=====================================================================================
//			Insert Banner 
//=====================================================================================
		public boolean insertBanner(List<BannerInfoBean> list) {

			SQLiteDatabase db = this.getWritableDatabase();
			try {
				String sql = "INSERT OR REPLACE INTO "
						+ TBL_BANNER_INFO
						+ " (IM_ID,IM_Type,IM_Message,IM_FromDate,IM_ToDate,IM_Duration) VALUES (?,?,?,?,?,?)";

				db.beginTransaction();

				SQLiteStatement stmt = db.compileStatement(sql);

				for (int i = 0; i < list.size(); i++) {
					BannerInfoBean data = new BannerInfoBean();

					data = list.get(i);

					if (data.IM_ID != null && data.IM_ID.length() > 0) {

						stmt.bindLong(1, Long.valueOf(data.IM_ID));

						if (data.IM_Type != null && data.IM_Type.length() > 0) {
							stmt.bindString(2, data.IM_Type);

						} else {
							stmt.bindString(2, " ");
						}
						if (data.IM_Message != null && data.IM_Message.length() > 0) {
							stmt.bindString(3, data.IM_Message);
						} else {
							stmt.bindString(3, " ");
						}

						if (data.IM_FromDate != null
								&& data.IM_FromDate.length() > 0) {
							stmt.bindString(4, data.IM_FromDate);
						} else {
							stmt.bindString(4, " ");
						}

						if (data.IM_ToDate != null && data.IM_ToDate.length() > 0) {
							stmt.bindString(5, data.IM_ToDate);
						} else {
							stmt.bindString(5, " ");
						}

						if (data.IM_Duration != null
								&& data.IM_Duration.length() > 0) {
							stmt.bindString(6, data.IM_Duration);
						} else {
							stmt.bindString(6, " ");
						}

						stmt.execute();
						stmt.clearBindings();
					}
				}

				db.setTransactionSuccessful();
				db.endTransaction();

				db.close();
				return true;
			} catch (Exception ex) {

				ex.printStackTrace();
				db.close();
				return false;
			}
		}
		
//=====================================================================================
//				Delete Contact Detail 
//=====================================================================================	
				
				public int deleteContact() {
					int count = 0;
					try {
						SQLiteDatabase db = this.getWritableDatabase();
						db.execSQL("Delete From " + TBL_CONTACT_CITY_INFO);
						db.execSQL("Delete From " + TBL_CONTACT_INFO);
						db.close();
						count++;
					} catch (Exception ex) {ex.printStackTrace();}
					return count;
				}

//=====================================================================================
//		Delete Banner 
//=====================================================================================	
		
		public int deleteBanner() {
			int count = 0;
			try {
				SQLiteDatabase db = this.getWritableDatabase();
				db.execSQL("Delete From " + TBL_BANNER_INFO);
				db.close();
				count++;
			} catch (Exception ex) {ex.printStackTrace();}
			return count;
		}

		public List<BannerInfoBean> getBannerImageData(){
			SQLiteDatabase db = this.getWritableDatabase();
			List<BannerInfoBean> list = new ArrayList<BannerInfoBean>();
			try{
				Cursor cur=db.rawQuery(
						"SELECT * FROM bannerInfo WHERE IM_Type = "
								+ 1, null);
				if (cur.getCount() > 0) {

					cur.moveToFirst();
					while (!cur.isAfterLast()) {
						BannerInfoBean data = new BannerInfoBean();
						data.IM_ID = cur.getString(0);
						data.IM_Type = cur.getString(1);
						data.IM_Message = cur.getString(2);
						data.IM_Duration = cur.getString(5);

						list.add(data);
						cur.moveToNext();
					}
				}
			}catch(Exception ex){ex.printStackTrace();}
			
			db.close();
			return list;
		}
		
		public List<BannerInfoBean> getBannerTextData(){
			SQLiteDatabase db = this.getWritableDatabase();
			List<BannerInfoBean> list = new ArrayList<BannerInfoBean>();
			try{
				Cursor cur=db.rawQuery(
						"SELECT * FROM bannerInfo WHERE IM_Type = "
								+ 2, null);
				if (cur.getCount() > 0) {

					cur.moveToFirst();
					while (!cur.isAfterLast()) {
						BannerInfoBean data = new BannerInfoBean();
						data.IM_ID = cur.getString(0);
						data.IM_Type = cur.getString(1);
						data.IM_Message = cur.getString(2);
						data.IM_Duration = cur.getString(5);

						list.add(data);
						cur.moveToNext();
					}
				}
			}catch(Exception ex){ex.printStackTrace();}
			db.close();
			return list;
		}
//=====================================================================================
//		Get Banner List
//=====================================================================================	
		public List<BannerInfoBean> getBannerInfoList() {
			SQLiteDatabase db = this.getWritableDatabase();
			List<BannerInfoBean> list = new ArrayList<BannerInfoBean>();
			try {
				Cursor cur = db.rawQuery(
						"SELECT * FROM "+ TBL_BANNER_INFO, null);

				if (cur.getCount() > 0) {

					cur.moveToFirst();
					while (!cur.isAfterLast()) {
						BannerInfoBean data = new BannerInfoBean();
						data.IM_ID = cur.getString(0);
						data.IM_Type = cur.getString(1);
						data.IM_Message = cur.getString(2);
						data.IM_Duration = cur.getString(5);

						list.add(data);
						cur.moveToNext();
					}
				}

			} catch (Exception ex) {
				ex.printStackTrace();
			}
			db.close();
			return list;
		}
}