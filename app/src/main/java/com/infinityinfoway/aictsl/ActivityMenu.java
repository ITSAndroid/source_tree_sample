package com.infinityinfoway.aictsl;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

public class ActivityMenu extends AppCompatActivity {
    private Toolbar toolbar;
    private Button buttoncityBusOperation, buttonIBus, buttonAtalCityBus, buttonSkyBus,
            buttonTeleRickshaw, buttonIBikePublicBicycles,
            buttonIRideRentalBikeService, buttonOnCallTaxiService;
    private LinearLayout llMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        llMenu = (LinearLayout) findViewById(R.id.llMenu);
        setSupportActionBar(toolbar);
        buttoncityBusOperation = (Button) findViewById(R.id.buttoncityBusOperation);
        ConnectionCheck connectionCheck = new ConnectionCheck();
        if (connectionCheck.isNetworkConnected(ActivityMenu.this)) {

            buttoncityBusOperation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    cityBusOperation();
                }
            });
        } else {
            Snackbar bar = Snackbar.make(llMenu, "No Internet Connection ", Snackbar.LENGTH_LONG);
            bar.show();
            // Toast.makeText(ActivityMenu.this, "No Internet Connection ", Toast.LENGTH_SHORT).show();
            return;
        }
    }

    private void cityBusOperation() {
        Intent intent = new Intent(ActivityMenu.this, ActivityCityBusOperation.class);
        startActivity(intent);

    }

    boolean doubleBackToExitPressedOnce = false;

    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() > 0) {
            getFragmentManager().popBackStack();
        } else if (!doubleBackToExitPressedOnce) {

            this.doubleBackToExitPressedOnce = true;

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
            Snackbar bar = Snackbar.make(llMenu, "tap again to exit", Snackbar.LENGTH_LONG);
            bar.show();
            //Toast.makeText(ActivityMenu.this, "tap again to exit", Toast.LENGTH_SHORT).show();
        } else {
            super.onBackPressed();
            return;
        }

    }
}
