package com.infinityinfoway.aictsl;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ListView;

import com.infinityinfoway.aictsl.bean.RecentSearchBean;
import com.infinityinfoway.aictsl.database.DBConnector;

import java.util.List;

public class ActivityRecentSearchList extends AppCompatActivity {
    ListView listView;
    RecentSearchListAdapter adapter;
    List<RecentSearchBean> list;
   // EasyTracker easyTracker;
    String TAG = "ActivityRecentSearchList";
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recent_search_list);
        // Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this, TAG));
       // easyTracker = EasyTracker.getInstance(ActivityRecentSearchList.this);


        listView = (ListView) findViewById(R.id.listView1);
        toolbar=(Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        DBConnector database = new DBConnector(ActivityRecentSearchList.this);

        database.deleteRecentSearch();
        list = database.getRecentSearch();
        database.close();

        if (list != null && list.size() > 0) {
            adapter = new RecentSearchListAdapter(list, ActivityRecentSearchList.this);
          //  Toast.makeText(ActivityRecentSearchList.this, "adapter==="+adapter.getCount(), Toast.LENGTH_SHORT).show();
            listView.setAdapter(adapter);
        } else {
            //CustomToast.showToastMessage(Activity_RecentSearchList.this,"Recent Search List Is Empty !!!");
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ActivityRecentSearchList.this);

            alertDialogBuilder
                    .setMessage("Recent Search List Is Empty !!!")
                    .setTitle("Alert!!..")
                    .setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }
//		txtClearAll.setOnClickListener(new OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//
//				exitDialog=new Dialog(Activity_RecentSearchList.this);
//				exitDialog.setTitle("Recent List Clear");
//				exitDialog.setContentView(R.layout.exitDialog);
//				exitDialog.setCancelable(false);
//				TextView txtTitle=(TextView)exitDialog.findViewById(R.id.txtTitle);
//				Button btnPositive=(Button)exitDialog.findViewById(R.id.btnPositive);
//				Button btnNegative=(Button)exitDialog.findViewById(R.id.btnNegative);
//				txtTitle.setText(getResources().getString(R.string.recentlListclear));
//				btnPositive.setOnClickListener(new OnClickListener() {
//
//					@Override
//					public void onClick(View arg0) {
//						exitDialog.cancel();
//						DBConnector database = new DBConnector(
//								Activity_RecentSearchList.this);
//						database.deleteAllRecentSearch();
//						database.close();
//
//						listView.setAdapter(null);
//
//						txtClearAll.setVisibility(View.GONE);
//
//						Toast.makeText(Activity_RecentSearchList.this,
//								"Successfully clear recent searches",
//								Toast.LENGTH_LONG).show();
//					}
//				});
//				btnNegative.setOnClickListener(new OnClickListener() {
//
//					@Override
//					public void onClick(View v) {
//						exitDialog.cancel();
//						return;
//					}
//				});
//				exitDialog.show();
//			}
//		});
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
            default:
        }
        return super.onOptionsItemSelected(item);

    }

    @Override
    public void onStart() {
        super.onStart();
       // EasyTracker.getInstance(this).activityStart(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        //EasyTracker.getInstance(this).activityStop(this);
    }
}
