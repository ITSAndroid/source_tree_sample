package com.infinityinfoway.aictsl.config;

import java.text.DecimalFormat;

public class Config {


	public static final String PACKAGE_NAME = "com.infinityinfoway.aictsl";
	public static final String APP_NAME = "AiCTSL";
	public static final String COMPANY_NAME = "aictsl";
	public static final String db_name = APP_NAME;
	public static final int db_version = 1;
	public static final String AEH_URL = "http://aeh.infinity-travel-solutions.com/app-api/service/common-api.php";
	public static final String AEH_ActURL = "http://aeh.infinity-travel-solutions.com/app-api/service/";
	public static final String api_url = "http://apibusops.itspl.net/";
	public static final String str_Key = "zw6Sv2A573wMxkZsvAF2ckFzuwTh9tT4dq5dK7j8DCgpR96j4mFc";
	public static final String str_URL = "http://apigpsaictsl.itspl.net/ITSGateway.asmx";
	public static final String str_SOAPActURL = "http://apibusops.itspl.net/";
	public static final boolean AEH_ENABLE = true;
		
	public static String AndroidId,AppVersionName,AppOsVer;
	public static int AppVersionCode;
		public static double roundTwoDecimals(double d)
	{
	    DecimalFormat twoDForm = new DecimalFormat("#.##");
	    return Double.valueOf(twoDForm.format(d));
	}

}
