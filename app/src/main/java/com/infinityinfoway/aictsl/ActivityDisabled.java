package com.infinityinfoway.aictsl;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

public class ActivityDisabled extends AppCompatActivity {
    //EasyTracker easyTracker = null;
    TextView tvTermsCondition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_disabled);
        //easyTracker = EasyTracker.getInstance(DisabledActivity.this);

    }
    public void onBackPressed()
    {
        finish();
    }
    @Override
    public void onStart()
    {
        super.onStart();
        //EasyTracker.getInstance(this).activityStart(this);
    }

    @Override
    public void onStop()
    {
        super.onStop();
        //EasyTracker.getInstance(this).activityStop(this);
    }
}
