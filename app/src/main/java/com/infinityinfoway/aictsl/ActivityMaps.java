package com.infinityinfoway.aictsl;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.infinityinfoway.aictsl.api.APIsConnectivity;
import com.infinityinfoway.aictsl.api.DataConnectivity;
import com.infinityinfoway.aictsl.database.DBConnector;
import com.infinityinfoway.aictsl.database.SharedPreference;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class ActivityMaps extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    SharedPreference getPref;
    int busid;
    int[] BUSID, LocationStatus;
    String[] BusNo, Location, LocationTime;
    Double[] Latitude, Longitude;
    // Double Latitude1, Longitude1;
    ProgressDialog progDialog;
    Bundle getSelData;
    DBConnector getSQLData;
    APIsConnectivity getAPI = new APIsConnectivity();
    DataConnectivity getData = new DataConnectivity();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        getPref = new SharedPreference(ActivityMaps.this);
        //easyTracker = EasyTracker.getInstance(ActivityGetBusLocation.this);
        getPref = new SharedPreference(ActivityMaps.this);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        getSelData = getIntent().getExtras();

        if (getSelData != null) {
            busid = getSelData.getInt("BusID");
            Log.e("busid", "" + busid);
            Log.v("busid", "" + busid);
        }
      //  Toast.makeText(this, "busid " + String.valueOf(busid), Toast.LENGTH_SHORT).show();
        new LazyDataConnection().execute("Get_BusLocation", getAPI.Get_BusLocation(busid));
        /*Latitude1 = Latitude[0];
        Longitude1 = Longitude[0];
       *//* Toast.makeText(this, "Latitude1 " + String.valueOf(Latitude1 + " " +Longitude1 ), Toast.LENGTH_SHORT).show();
        Log.e("Latitude1", "" + Latitude1 + " " +Longitude1 );
*/
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;


    }


    //========================================================================================
    //========================================================================================
    public void Get_BusLocation(String str_RespXML, String str_TagName) {
        Document doc = getData.XMLfromString(str_RespXML);
        NodeList nodes = doc.getElementsByTagName(str_TagName);

        BUSID = new int[nodes.getLength()];
        BusNo = new String[nodes.getLength()];
        Latitude = new Double[nodes.getLength()];
        Longitude = new Double[nodes.getLength()];
        Location = new String[nodes.getLength()];
        LocationTime = new String[nodes.getLength()];
        LocationStatus = new int[nodes.getLength()];

        if (nodes.getLength() > 0)
            for (int i = 0; i <= nodes.getLength(); i++) {
                Node e1 = nodes.item(i);
                Element el = (Element) e1;
                if (el != null) {

                    try {
                        BusNo[i] = el.getElementsByTagName("BusNo").item(0).getTextContent();
                       // Toast.makeText(this, "BusNo" + BusNo, Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                    }
                    try {
                        Latitude[i] = Double.parseDouble(el.getElementsByTagName("Latitude").item(0).getTextContent());
                       // Toast.makeText(this, "Latitude---" + Latitude[i], Toast.LENGTH_SHORT).show();

                    } catch (Exception e) {
                    }
                    try {
                        Longitude[i] = Double.parseDouble(el.getElementsByTagName("Longitude").item(0).getTextContent());
                       // Toast.makeText(this, "Longitude++++" + Longitude[i], Toast.LENGTH_SHORT).show();

                    } catch (Exception e) {
                    }
                    try {
                        Location[i] = el.getElementsByTagName("Location").item(0).getTextContent();
                       // Toast.makeText(this, "Location" + Location[i], Toast.LENGTH_SHORT).show();

                    } catch (Exception e) {
                    }
                    try {
                        LocationTime[i] = el.getElementsByTagName("LocationTime").item(0).getTextContent();
                       // Toast.makeText(this, "LocationTime" + LocationTime, Toast.LENGTH_SHORT).show();

                    } catch (Exception e) {
                    }

                    try {
                        BUSID[i] = Integer.parseInt(el.getElementsByTagName("BUSID").item(0).getTextContent());
                        //Toast.makeText(this, "BUSID" + String.valueOf(BUSID), Toast.LENGTH_SHORT).show();

                    } catch (Exception e) {
                    }
                    try {
                        LocationStatus[i] = Integer.parseInt(el.getElementsByTagName("LocationStatus").item(0).getTextContent());
                        //Toast.makeText(this, "LocationStatus" + String.valueOf(LocationStatus), Toast.LENGTH_SHORT).show();

                    } catch (Exception e) {
                    }

                }
            }
    }


    //=====================================================================================
// Async Task Class
//=====================================================================================
    public class LazyDataConnection extends AsyncTask<String, Void, String> {
        String method;

        @Override
        protected String doInBackground(String... arg0) {
            method = arg0[0];
            return getData.callWebService(arg0[0], arg0[1]);
        }

        protected void onPostExecute(String xmlResponse) {

            if (xmlResponse.equals("")) {
                try {
                    progDialog.dismiss();
                    if (progDialog != null && progDialog.isShowing()) {
                        progDialog.dismiss();
                    }
                } catch (Exception ex) {
                }
                Toast.makeText(ActivityMaps.this, "InternetErrorActivity", Toast.LENGTH_SHORT).show();
                return;
            } else {
                if (method.equals("Get_BusLocation")) {
                    Get_BusLocation(xmlResponse, "CurrentLocation");
                    try {
                        progDialog.dismiss();
                        if (progDialog != null && progDialog.isShowing()) {
                            progDialog.dismiss();
                        }
                    } catch (Exception ex) {
                    }

                    if (mMap != null) {
                        for (int i = 0; i < Latitude.length; i++) {
                            //LatLng buslatlog = new LatLng(22.3039, 70.8022);
                            LatLng buslatlog = new LatLng(Latitude[i], Longitude[i]);
                            // Log.e("Latitude[i]  Longitude[i]", " " + Latitude[i] + Longitude[i]);
                           // mMap.addMarker(new MarkerOptions().position(buslatlog).title(Location[i]));
                            Marker marker = mMap.addMarker(new MarkerOptions().position(buslatlog).title(Location[i]));
                            marker.showInfoWindow();
                            mMap.moveCamera(CameraUpdateFactory.newLatLng(buslatlog));
                            mMap.animateCamera( CameraUpdateFactory.zoomTo( 12.0f ) );

                        }
                    }

                }
            }
        }
    }
}
