/*
package com.infinityinfoway.aictsl;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.infinityinfoway.aictsl.api.APIsConnectivity;
import com.infinityinfoway.aictsl.api.DataConnectivity;
import com.infinityinfoway.aictsl.database.DBConnector;
import com.infinityinfoway.aictsl.database.SharedPreference;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class ActivityGetBusLocation extends
        FragmentActivity implements //OnMapReadyCallback {
    SharedPreference getPref;
    int busid;
    int[] BUSID;
    String[] BusNo, Latitude, Longitude, Location, LocationTime, LocationStatus;
    ProgressDialog progDialog;
    Bundle getSelData;
    DBConnector getSQLData;
    APIsConnectivity getAPI = new APIsConnectivity();
    DataConnectivity getData = new DataConnectivity();
    Toolbar toolbar;
    //EasyTracker easyTracker = null;
    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_bus_location);
        getPref = new SharedPreference(ActivityGetBusLocation.this);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        */
/*setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
     *//*
   //easyTracker = EasyTracker.getInstance(ActivityGetBusLocation.this);
        getPref = new SharedPreference(ActivityGetBusLocation.this);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        getSelData = getIntent().getExtras();

        if (getSelData != null) {
            busid = getSelData.getInt("BusID");
        }

        new LazyDataConnection().execute("Get_BusLocation", getAPI.Get_BusLocation(busid));
    }
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
    }

     */
/* @Override
      public void onMapReady(GoogleMap map) {
  //DO WHATEVER YOU WANT WITH GOOGLEMAP
          map.setMapType(GoogleMap.MAP_TYPE_HYBRID);
          if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
              // TODO: Consider calling
              //    ActivityCompat#requestPermissions
              // here to request the missing permissions, and then overriding
              //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
              //                                          int[] grantResults)
              // to handle the case where the user grants the permission. See the documentation
              // for ActivityCompat#requestPermissions for more details.
              return;
          }
          map.setMyLocationEnabled(true);
          map.setTrafficEnabled(true);
          map.setIndoorEnabled(true);
          map.setBuildingsEnabled(true);
          map.getUiSettings().setZoomControlsEnabled(true);
      }
  *//*

    //=====================================================================================
// Source City List
//=====================================================================================
    public void Get_BusLocation(String str_RespXML, String str_TagName) {
        Document doc = getData.XMLfromString(str_RespXML);
        NodeList nodes = doc.getElementsByTagName(str_TagName);

        BUSID = new int[nodes.getLength()];
        BusNo = new String[nodes.getLength()];
        Latitude = new String[nodes.getLength()];
        Longitude = new String[nodes.getLength()];
        Location = new String[nodes.getLength()];
        LocationTime = new String[nodes.getLength()];
        LocationStatus = new String[nodes.getLength()];

        if (nodes.getLength() > 0)
            for (int i = 0; i <= nodes.getLength(); i++) {
                Node e1 = nodes.item(i);
                Element el = (Element) e1;
                if (el != null) {

                    try {
                        BusNo[i] = el.getElementsByTagName("BusNo").item(0).getTextContent();
                        Toast.makeText(this, "BusNo" + BusNo, Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                    }
                    try {
                        Latitude[i] = el.getElementsByTagName("Latitude").item(0).getTextContent();
                        Toast.makeText(this, "Latitude" + Latitude, Toast.LENGTH_SHORT).show();

                    } catch (Exception e) {
                    }
                    try {
                        Longitude[i] = el.getElementsByTagName("Longitude").item(0).getTextContent();
                        Toast.makeText(this, "Longitude" + Longitude, Toast.LENGTH_SHORT).show();

                    } catch (Exception e) {
                    }
                    try {
                        Location[i] = el.getElementsByTagName("Location").item(0).getTextContent();
                        Toast.makeText(this, "Location" + Location, Toast.LENGTH_SHORT).show();

                    } catch (Exception e) {
                    }
                    try {
                        LocationTime[i] = el.getElementsByTagName("LocationTime").item(0).getTextContent();
                        Toast.makeText(this, "LocationTime" + LocationTime, Toast.LENGTH_SHORT).show();

                    } catch (Exception e) {
                    }
                    try {
                        LocationStatus[i] = el.getElementsByTagName("LocationStatus").item(0).getTextContent();
                        Toast.makeText(this, "LocationStatus" + LocationStatus, Toast.LENGTH_SHORT).show();

                    } catch (Exception e) {
                    }

                    try {
                        BUSID[i] = Integer.parseInt(el.getElementsByTagName("BUSID").item(0).getTextContent());
                        Toast.makeText(this, "BUSID" + BUSID, Toast.LENGTH_SHORT).show();

                    } catch (Exception e) {
                    }

                }
            }
    }



    //=====================================================================================
// Async Task Class
//=====================================================================================
    public class LazyDataConnection extends AsyncTask<String, Void, String> {
        String method;

        @Override
        protected String doInBackground(String... arg0) {
            method = arg0[0];
            return getData.callWebService(arg0[0], arg0[1]);
        }

        protected void onPostExecute(String xmlResponse) {

            if (xmlResponse.equals("")) {
                try {
                    progDialog.dismiss();
                    if (progDialog != null && progDialog.isShowing()) {
                        progDialog.dismiss();
                    }
                } catch (Exception ex) {
                }
                Toast.makeText(ActivityGetBusLocation.this, "InternetErrorActivity", Toast.LENGTH_SHORT).show();
                return;
            } else {
                if (method.equals("Get_BusLocation")) {
                    Get_BusLocation(xmlResponse, "CurrentLocation");
                    try {
                        progDialog.dismiss();
                        if (progDialog != null && progDialog.isShowing()) {
                            progDialog.dismiss();
                        }
                    } catch (Exception ex) {
                    }

                }
            }
        }
    }


    */
/*@Override
    public boolean onOptionsItemSelected(MenuItem item) {

       *//*
*/
/* switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
            default:
        }
        return super.onOptionsItemSelected(item);*//*
*/
/*

    }*//*



    @Override
    public void onStart() {
        super.onStart();
        //EasyTracker.getInstance(this).activityStart(this);
    }

    @Override
    public void onStop() {
        super.onStop();
       // EasyTracker.getInstance(this).activityStop(this);
    }
}
*/
